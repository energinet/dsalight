# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.0-only

import os
import sys
import xlrd
import time
import psutil
import hashlib
import zipfile
import datetime
import platform
import matplotlib
import subprocess
import win32com.client
import xml.etree.ElementTree as ET

from packaging.version import Version
from PyQt5 import QtWidgets, QtGui, QtCore, uic

from DPS import DPS
from RMSReport import RMSReport

from SetupBalances import SetupBalances
from TASP import TASP

''' MPO 2018-01-16
SetupBalances "hard-code" usage of a very specific PowerFactory version by:
    sys.path.append(r"C:\\Program Files\\DIgSILENT\\PowerFactory 2017 SP3\\Python\\3.5\\")
which will make it "unpredictable" when importing PowerFactory library into Python:
    import powerfactory as pf
In order to be able to use the main code of python different versions of PowerFactory,
this part of the code will "clean" the "sys.path" to be ale to include only the
 proper version of PowerFactory library
'''
for idx in range(3):
    for line in sys.path:
        if 'PowerFactory' in line or 'ELvis' in line:
            sys.path.remove(line)


class DSAlight(QtWidgets.QMainWindow):

    __author__ = 'Jeppe Meldgaard Røge'
    __author_email__ = 'dsalight@energinet.dk'
    __copyright__ = '© Copyright 2016-2024, Energinet'
    __version__ = '4.6.0'
    __name__ = 'DSAlight v{0} © Energinet 2024'.format(__version__)

    def __init__(self):
        super().__init__()

        self.CONFIGURATION_FILE = os.path.join(os.getcwd(), 'DSAlight.ini')

        self.mainW = uic.loadUi(r'ui\DSAlight.ui')
        self.mainW.setWindowTitle(self.__name__)

        self.pfUserInfoForm = uic.loadUi(r'ui\UserInfoForm.ui')
        self.pfUserInfoForm.btnBoxUser.accepted.connect(self.startPf)

        self.ContingencyForm = uic.loadUi(r'ui\ContingencyConfiguration.ui')
        self.ContingencyForm.setWindowTitle('{0} (Contingency Configuration) {1}'.format(*self.mainW.windowTitle().split(' ', 1)))
        self.ContingencyForm.buttonBox.accepted.connect(lambda: self.contFinalize(True))
        self.ContingencyForm.buttonBox.rejected.connect(lambda: self.contFinalize(False))
        self.ContingencyForm.pb_Busbars.clicked.connect(lambda: self.contShow(self.ContingencyElmTerm, 1))
        self.ContingencyForm.pb_Transformers.clicked.connect(lambda: self.contShow(self.ContingencyElmTr, 1))
        self.ContingencyForm.pb_Shunts.clicked.connect(lambda: self.contShow(self.ContingencyElmShnt, 1))
        self.ContingencyForm.pb_Lines.clicked.connect(lambda: self.contShow(self.ContingencyElmBranch, 1))
        self.ContingencyForm.pb_HVDC.clicked.connect(lambda: self.contShow(self.ContingencyElmHVDC, 0))
        self.ContingencyForm.pb_Generators.clicked.connect(lambda: self.contShow(self.ContingencyElmGen, 0))
        self.ContingencyForm.pb_PV_Generators.clicked.connect(lambda: self.contShow(self.ContingencyElmPV, 1))
        self.ContingencyForm.pb_Contingency_Set.clicked.connect(lambda: self.contShow(self.selectedContingency, 0))
        self.ContingencyForm.pb_SIPS.clicked.connect(lambda: self.contShow(self.ContingencySIPS, 1))

        SP_DialogOpenButton = self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_DialogOpenButton'))
        for element in (self.ContingencyForm.pb_Busbars, self.ContingencyForm.pb_Transformers,
                        self.ContingencyForm.pb_Shunts, self.ContingencyForm.pb_Lines,
                        self.ContingencyForm.pb_HVDC, self.ContingencyForm.pb_Generators,
                        self.ContingencyForm.pb_PV_Generators, self.ContingencyForm.pb_Contingency_Set,
                        self.ContingencyForm.pb_SIPS):
            element.setIcon(SP_DialogOpenButton)
        for element in (self.ContingencyForm.cb_Busbars, self.ContingencyForm.cb_Transformers,
                        self.ContingencyForm.cb_Shunts, self.ContingencyForm.cb_Lines,
                        self.ContingencyForm.cb_HVDC, self.ContingencyForm.cb_Generators,
                        self.ContingencyForm.cb_PV_Generators, self.ContingencyForm.cb_SIPS):
            element.clicked.connect(self.getSelectedContingency)
        for element in (self.ContingencyForm.sb_Busbars, self.ContingencyForm.sb_Transformers,
                        self.ContingencyForm.sb_Shunts, self.ContingencyForm.sb_Lines,
                        self.ContingencyForm.sb_Generators):
            element.valueChanged.connect(self.getSelectedContingency)

        self.DelfinForm = uic.loadUi(r'ui\DelfinImport.ui')
        self.DelfinForm.setWindowTitle('{0} (Delfin Import) {1}'.format(*self.mainW.windowTitle().split(' ', 1)))
        self.DelfinForm.buttonBox.accepted.connect(lambda: self.delfinFinalize(True))
        self.DelfinForm.buttonBox.rejected.connect(lambda: self.delfinFinalize(False))
        self.DelfinForm.pb_Add.clicked.connect(self.delfinAdd)
        self.DelfinForm.pb_Remove.clicked.connect(self.delfinRemove)
        self.DelfinForm.pb_Delete.clicked.connect(lambda: self.delfinRemove(True))
        for element in (self.DelfinForm.cb_ITC, self.DelfinForm.cb_MultipleTP, self.DelfinForm.cb_MultipleNetsys):
            element.clicked.connect(self.getSelectedDelfin)
        self.DelfinForm.pb_Add.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_ArrowRight')))
        self.DelfinForm.pb_Remove.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_ArrowLeft')))
        self.DelfinForm.pb_Delete.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_TrashIcon')))
        for element in (self.DelfinForm.pb_Add, self.DelfinForm.pb_Remove, self.DelfinForm.pb_Delete):
            element.setText(None)

        self.mainW.progressbar = QtWidgets.QProgressBar()
        self.mainW.progressbar.setFixedWidth(150)
        self.mainW.progressbar.setMinimum(0)
        self.mainW.progressbar.setMaximum(100)
        self.mainW.statusbar.addWidget(self.mainW.progressbar)

        self.outputTab = {value: idx for idx, value in enumerate(('Import', 'Load Flow', 'Contingency', 'Short-Circuit', 'RMS'))}

        self.SMTNETScheduleTimer = QtCore.QTimer()
        self.SMTNETScheduleTimer.timeout.connect(self.SMTNETScheduleScan)

        self.mainW.btnClearOutput.clicked.connect(lambda: self.getOutputWidget().clear())
        self.mainW.btnCopyOutput.clicked.connect(self.copyToClipboard)
        self.mainW.btnFont.clicked.connect(self.setFont)
        self.mainW.btnStartPf.clicked.connect(self.getPfCredentials)
        self.mainW.btnGetDpsData.clicked.connect(lambda: self.getDPSData())
        self.mainW.btnGetGekkoData.clicked.connect(lambda: self.getGekkoData())
        self.mainW.btnGetDelfinData.clicked.connect(self.delfinPrepare)
        self.mainW.btnGetRevisionPlanBalances.clicked.connect(self.getPlanlaegningsbalancerData)
        self.mainW.btnGetStateChanges.clicked.connect(self.getStateChangesData)
        self.mainW.btnCreateStandAloneCases.clicked.connect(lambda: self.createStandAloneCases(self.outputTab['Import']))
        self.mainW.btnUncheckAllDPS.clicked.connect(lambda: self.checkAllItems(self.mainW.listDPS, False))
        self.mainW.btnCheckAllDPS.clicked.connect(lambda: self.checkAllItems(self.mainW.listDPS, True))
        self.mainW.btnUncheckAllRMS.clicked.connect(lambda: self.checkAllItems(self.mainW.listRMS, False))
        self.mainW.btnCheckAllRMS.clicked.connect(lambda: self.checkAllItems(self.mainW.listRMS, True))
        self.mainW.btnExpandAll.clicked.connect(lambda: self.mainW.treeGekko.expandAll())
        self.mainW.btnCollapseAll.clicked.connect(lambda: self.mainW.treeGekko.collapseAll())
        self.mainW.btnRunLdf.clicked.connect(self.runLdf)
        self.mainW.btnDefCont.clicked.connect(self.contPrepare)
        self.mainW.btnRunCont.clicked.connect(self.runCont)
        self.mainW.btnRunRMS.clicked.connect(self.runRMS)
        self.mainW.btnSaveScreenshot.clicked.connect(self.saveScreenshot)
        self.mainW.btnSaveOutputWindow.clicked.connect(self.saveOutputWindow)
        self.mainW.btnRMSReport.clicked.connect(self.RMS_Report)
        self.mainW.btnContReport.clicked.connect(self.contReport)
        self.mainW.btnEnerginet.clicked.connect(lambda: os.startfile('https://energinet.dk'))
        self.mainW.cbSMTNETScheduleScan.stateChanged.connect(self.SMTNETScheduleControl)
        self.mainW.actSettings.triggered.connect(self.settingsDSA)
        self.mainW.actShowPf.triggered.connect(self.showPf)
        self.mainW.actHidePf.triggered.connect(self.hidePf)
        self.mainW.actSupport_DSAlight.triggered.connect(self.supportRequest)
        self.mainW.actSupport_DB_ELvis.triggered.connect(self.supportRequest)
        self.mainW.actUserGuide.triggered.connect(lambda: os.startfile(os.path.join(os.getcwd(), r'help\DSAlight User Guide.pdf')))
        self.mainW.actNaming_convention.triggered.connect(self.namingConvention)
        self.mainW.actChangelog.triggered.connect(lambda: os.startfile(r'ChangeLog.txt'))
        self.mainW.actDSAlight_Website.triggered.connect(lambda: os.startfile('https://gitlab.com/energinet/dsalight'))
        self.mainW.actAbout_Qt.triggered.connect(lambda: QtWidgets.QApplication.aboutQt())
        self.mainW.actAbout_DSAlight.triggered.connect(self.aboutDSAlight)
        self.mainW.actSPD_DSA.triggered.connect(self.SPD_DSA)
        self.mainW.listDPS.clicked.connect(self.enableCalculations)
        self.mainW.listRMS.clicked.connect(self.enableCalculations)
        self.mainW.rbDK1.toggled.connect(self.activateStudyCase)

        for element in (self.mainW.btnGetDpsData, self.mainW.btnGetGekkoData, self.mainW.btnGetDelfinData,
                        self.mainW.gB_AdvancedOptions, self.mainW.gB_Calculations,
                        self.mainW.gB_DPSList, self.mainW.gB_GekkoList, self.mainW.gb_RMSList,
                        self.mainW.btnContReport):
            element.setEnabled(False)

        self.mainW.show()

        self.readConfigurationFile()
        if self.checkForUpdates.lower() == 'true':
            self.checkForNewVersion()

        self.SMTNETSchedulePath = os.path.join(self.SMTNETSchedule, *time.strftime('%Y,%#m,%#d', time.gmtime()).split(','))

        self.PowerFactoryErrorCode = {
            1000: 'Application initialisation failed',
            1001: 'Cannot load DLL XXX',
            1002: 'DLL file XXX appears not to be valid',
            2000: 'Configuration contains errors',
            2001: 'Configuration file XYZ is missing',
            2002: 'Windows has to be configured with ”XYZ” as the ”Language for Non-Unicode Programs” for the selected application language ”ABC”',
            3000: 'No PowerFactory licence found or the selected licence ”X@Y” is invalid',
            3001: 'Licence is invalid',
            3002: 'Your licence requires online access to verify its validity',
            3003: 'This floating licence was not generated by PowerFactory tools',
            3004: 'You are about to access a hot-standby licence. Please use a full licence instead',
            3005: 'An internal licence error occurred',
            3006: 'Failed to update the activation file',
            3007: 'The currently used licence is not included in the licence activation file of your PowerFactory installation',
            3008: 'You are using an engine licence',
            3009: 'Function multi-user database not included in licence',
            3010: 'Function Scripting and Automation not included in licence',
            3011: 'Error during initialization of licence thread',
            3012: 'Connection to licence module has been lost',
            3013: 'Windows Service CodeMeter.exe is not running',
            3014: 'Licence system runtime not installed',
            4000: 'Required database initialisation was rejected',
            4001: 'Database migration failed',
            4002: 'Another PowerFactory instance already accesses this single-user database',
            4003: 'Required database repairing due to records of a future date was rejected',
            4004: 'The database contains records of a future date, and can’t be used',
            4005: 'Database cannot be read with PowerFactory AAA. Please use PowerFactory BBB or higher to access it',
            4006: 'The database cannot be read with this version, and database migration was rejected',
            4007: 'Database is in an invalid migration state',
            4008: 'Migration failed due to incorrect Administrator password',
            4009: 'Starting Offline session failed',
            4010: 'Internal database error: ...',
            4011: 'No more objects can be created in Offline mode because the Id contingent is exhausted',
            4012: 'Oracle client library cannot be loaded. Please ensure that the Oracle client runtime is installed correctly, and that PowerFactory is configured accordingly',
            4013: 'Error in local database: ...',
            5000: 'User couldn’t be logged on',
            5001: 'Logon is currently restricted to a limited user group',
            5002: 'No Profile found',
            5003: 'Workspace export failed',
            5004: 'User did not change password at log on',
            7000: 'PowerFactory cannot be started again in the same process',
            7001: 'Function XYZ called with invalid argument',
                }

        try:
            self.OUTLOOK = win32com.client.Dispatch('Outlook.application')
        except Exception:
            self.updateGUI('WARN: Microsoft Outlook has not been detected - support request features will not be available', None)
            for element in (self.mainW.actSupport_DSAlight, self.mainW.actSupport_DB_ELvis):
                element.setEnabled(False)

    def settingsDSA(self):
        with open(self.CONFIGURATION_FILE, 'rb') as fid:
            md5_org = hashlib.md5(fid.read()).hexdigest()

        subprocess.call(['cmd.exe', '/c', self.CONFIGURATION_FILE], shell=True)

        with open(self.CONFIGURATION_FILE, 'rb') as fid:
            md5_new = hashlib.md5(fid.read()).hexdigest()

        if md5_org != md5_new:
            QtWidgets.QMessageBox.information(self.mainW, self.mainW.windowTitle(), 'You have modified configuration file - DSAlight will restart now')
            os.execl(sys.executable, sys.executable, *sys.argv)

    def readConfigurationFile(self):
        if os.path.isfile(self.CONFIGURATION_FILE):
            with open(self.CONFIGURATION_FILE, encoding='utf-8') as fid:
                lines = fid.read().split('\n')

            for line in (element for element in lines
                         if element and '=' in element):
                parameter, value = line.split('=', 1)
                value = value.strip()
                if '\%' in value:
                    value = os.path.expandvars(value)
                elif not value:
                    value = None
                    self.updateGUI('WARN: INI File - parameter "{0}" does not have any value'.format(parameter), None)
                setattr(self, parameter.strip(), value)

            if self.pfIniFile:
                if not os.path.isfile(self.pfIniFile):
                    self.pfIniFile = None
                    self.updateGUI('ERROR: Specified PowerFactory INI File does not exist', None)
                elif not os.stat(self.pfIniFile).st_size:
                    self.pfIniFile = None
                    self.updateGUI('ERROR: Specified PowerFactory INI File is empty', None)

            PF_PYTHON = os.path.join(self.pfPath, 'Python', '{0}.{1}'.format(sys.version_info[0], sys.version_info[1]))
            sys.path.append(PF_PYTHON)
            try:
                import powerfactory
            except Exception:
                self.updateGUI('ERROR: DSAlight has not been able to import the PowerFactory module from: "{0}"'.format(PF_PYTHON), 100)
                self.mainW.gB_ImportData.setEnabled(False)
            else:
                self.powerfactory = powerfactory
                self.isUserGiven = bool(self.username)

                self.ENABLE = {key: True for key in ('Da2Prog', 'Da2Prog_MappingFile', 'Da2Prog_OutputFolder', 'RMS')}

                if self.D2P_Application is None or not os.path.isfile(self.D2P_Application):
                    self.updateGUI('ERROR: Da2Prog not found - DPS, Delfin & SMTNETScheduleScan modules will not be available to use', 100)
                    self.ENABLE['Da2Prog'] = False

                if self.mapping_file is None or not os.path.isfile(self.mapping_file):
                    self.updateGUI('ERROR: Da2Prog mapping file not found - DPS, Delfin & SMTNETScheduleScan modules will not be available to use', 100)
                    self.ENABLE['Da2Prog'] = False

                if self.output_folder is None or not os.path.isdir(self.output_folder):
                    self.updateGUI('ERROR: Da2Prog output folder does not exist - DPS, Delfin & SMTNETScheduleScan modules will not be available to use', 100)
                    self.updateGUI('WARN: SaveScreenshot & SaveOutputWindow will not be available to use', 100)
                    for element in (self.mainW.btnSaveScreenshot, self.mainW.btnSaveOutputWindow):
                        element.setEnabled(False)
                    self.ENABLE['Da2Prog'] = False

                if self.rmsOutputFilePath is None or not os.path.isdir(self.rmsOutputFilePath):
                    self.updateGUI('ERROR: RMS output folder does not exist - RMS calculation module will not be available to use', 100)
                    self.ENABLE['RMS'] = False

        else:
            self.updateGUI('ERROR: DSAlight has not been able to import the "DSAlight.ini" file', 100)
            for element in (self.mainW.btnStartPf, self.mainW.rbDK1, self.mainW.rbDK2, self.mainW.cbITCStudyCase, self.mainW.cbCreateDerivedProject, self.mainW.actSettings):
                element.setEnabled(False)

    def updateGUI(self, message, progress):
        if message is not None:
            item = QtWidgets.QListWidgetItem(message)
            if 'ERROR' in message:
                item.setForeground(QtCore.Qt.red)
            elif 'WARN' in message:
                item.setForeground(QtCore.Qt.darkYellow)
            self.getOutputWidget().addItem(item)
            self.getOutputWidget().scrollToBottom()
        if progress is not None:
            self.mainW.progressbar.setValue(progress)
        QtWidgets.QApplication.processEvents()

    def checkForNewVersion(self):
        try:
            VERSIONS = [Version(os.path.split(filename)[1].split('DSAlight_v')[1].split('.exe')[0])
                        for filename in os.scandir(self.installationFolder) if filename.is_file() and filename.name.endswith('.exe')]
        except Exception:
            pass
        else:
            DSA_VERSION = Version(self.__version__)
            for update in sorted(VERSIONS, reverse=True):
                if DSA_VERSION < update and \
                   (DSA_VERSION.is_prerelease or self.checkForDevelopmentBuilds.lower() == 'true' or (not DSA_VERSION.is_prerelease and not update.is_prerelease)):
                    QtWidgets.QMessageBox.warning(self.mainW, self.mainW.windowTitle(), 'You are using DSAlight version {0} - Please update to version {1}'.format(self.__version__, update.public))
                    subprocess.Popen(r'explorer /select, "{0}\DSAlight_v{1}.exe"'.format(self.installationFolder, update.public))
                    QtCore.QTimer().singleShot(300000, self.checkForNewVersion)
                    break

    def aboutDSAlight(self):
        OS = '{0} {1} {2}'.format(platform.system(), platform.release(), platform.architecture()[0])
        MEMORY = psutil.virtual_memory()[0]/1024**3

        CONTENT = ('<b>DSAlight v{0} © <a href="https://energinet.dk">Energinet</a></b> 2024<br><br>'
                   'DSAlight is graphical user interface developed by <a href="https://energinet.dk">Energinet</a> to interact with '
                   '<a href="https://www.digsilent.de/en/powerfactory.html">DIgSILENT PowerFactory</a> in order to perform a dynamic stability assessment of the transmission system '
                   'in accordance with "<a href="http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2017.220.01.0001.01.ENG&toc=OJ:L:2017:220:TOC#d1e3962-1-1">Article 38 - Dynamic stability monitoring and assessment</a>" of the '
                   '<a href="https://electricity.network-codes.eu/network_codes/sys-ops/">System Operation Guideline</a><br>'
                   '<br>DSAlight  uses free and open source language: <a href="https://www.python.org">Python</a> and <a href="https://www.qt.io">Qt</a>'
                   '<br>---------------------------------------------------------------------------<br>'
                   '<br>System overview:<br><br>User: {1}<br>PC: {2}<br>OS: {3}<br>Python version: {4}<br>Qt version: {5}<br>Matplotlib version: {6}<br>CPU Cores: {7}<br>Memory: {8:.1f} [GB]<br><br>'
                   '<i>"By optimizing as many routines as possible without losing user friendliness, DSAlight is trying to reduce the world carbon dioxide emissions. '
                   'When using less CPU power, the PC can throttle down and reduce power consumption, resulting in a greener environment."<i>'
                   .format(self.__version__, os.environ['USERNAME'], platform.node(), OS, platform.python_version(), QtCore.qVersion(), matplotlib.__version__, os.cpu_count(), MEMORY))
        QtWidgets.QMessageBox.information(self.mainW, 'About DSAlight', CONTENT)

    def namingConvention(self):
        NAMING_CONVENTION = ('StudyCase = EB_SBE_NET\n\n'
                             'where:\tEB    = ID for Power Balance\n'
                             '\tSBE  = ID for Energinet''s own system supporting units\n'
                             '\tNET = ID for grid configuration\n\n'
                             'It is not allowed to use " " or "-" while defining naming for StudyCase !\n\n'
                             'Available SBE''s for DK1 = [FGDG1, VHAG1, TJEG1, CO1, SK4]\n'
                             'Available SBE''s for DK2 = [BJSG1, HKSG1, RADSVC]\n\n'
                             'Example for DK1:\n\n'
                             'EB2_10001_000 --> Second EB, FGDG1 and SK4 in operation, intact net\n'
                             'EB5_01000_001 --> Fifth EB, VHAG1 in operation, outage of first element\n\n'
                             '---------------------------------------------------------------------------\n\n'
                             'ShortCircuit = Any name without " " and "-"\n\n'
                             'This normally comes with PowerFactory and does not need user interference. '
                             'Typically, the name convention for ShortCircuit consist of "ID_NAME", e.g. "01_KAS_400".\n\n'
                             '---------------------------------------------------------------------------\n\n'
                             'Example of the final LogFile with proper naming convention:\n\n'
                             'EB2_10001_000-01_KAS_400')
        QtWidgets.QMessageBox.information(self.mainW, 'Naming convention for RMS analysis with DSAlight', NAMING_CONVENTION)

    def saveOutputWindow(self):
        OUTPUTWINDOW = os.path.join(self.output_folder, '{0} - DSAlight Output Window.txt'.format(time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime())))
        self.updateGUI('Output window saved under: {0}'.format(OUTPUTWINDOW), None)
        outputWindow = self.getOutputWidget()
        with open(OUTPUTWINDOW, 'w', encoding='utf-8') as fid:
            fid.write('\n'.join(outputWindow.item(idx).text() for idx in range(outputWindow.count())))
        return OUTPUTWINDOW

    def saveScreenshot(self):
        SCREENSHOT = os.path.abspath(os.path.join(self.output_folder, '{0} - DSAlight Screenshot.png'.format(time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime()))))
        GEOMETRY = self.mainW.frameGeometry()
        QtWidgets.QApplication.primaryScreen().grabWindow(0, GEOMETRY.x(), GEOMETRY.y(), GEOMETRY.width(), GEOMETRY.height()).save(SCREENSHOT)
        self.updateGUI('Screenshot saved under: {0}'.format(SCREENSHOT), None)
        return SCREENSHOT

    def copyToClipboard(self):
        outputWindow = self.getOutputWidget()
        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.clear()
        clipboard.setText('\n'.join(outputWindow.item(idx).text() for idx in range(outputWindow.count())), mode=clipboard.Clipboard)

    def supportRequest(self):
        mail = self.OUTLOOK.CreateItem(0)
        mail.Subject = 'DSAlight v{0} support'.format(self.__version__)
        if self.mainW.btnSaveScreenshot.isEnabled():
            mail.Attachments.Add(self.saveScreenshot())
        if self.mainW.btnSaveOutputWindow.isEnabled():
            mail.Attachments.Add(self.saveOutputWindow())
        mail.Display()
        if self.sender() == self.mainW.actSupport_DSAlight:
            mail.To = self.emailTo
            mail.Cc = self.emailCc
            mail.HTMLBody = '<body>Dear DSAlight developers,<br><br>WRITE YOURS REQUEST HERE</body>'
        else:
            mail.To = self.emailDb
            mail.Cc = '{0};{1}'.format(self.emailTo, self.emailCc)
            mail.HTMLBody = '<body>Dear DB-ELvis,<br><br>WRITE YOURS REQUEST HERE</body>'

    def SPD_DSA(self):
        os.startfile('https://docstore.entsoe.eu/Documents/SOC%20documents/Regional_Groups_Continental_Europe/2017/DSA_REPORT_Public.pdf')

    def getOutputWidget(self):
        idx = self.mainW.tabOutput.currentIndex()
        if idx == 0:
            widget = self.mainW.listOutImport
        elif idx == 1:
            widget = self.mainW.listOutLdf
        elif idx == 2:
            widget = self.mainW.listOutCont
        elif idx == 3:
            widget = self.mainW.listOutShortCircuit
        elif idx == 4:
            widget = self.mainW.listOutRMS
        else:
            widget = None
        return widget

    def setFont(self):
        font, valid = QtWidgets.QFontDialog.getFont(self.getOutputWidget().font())
        if valid:
            self.getOutputWidget().setFont(font)

    def checkAllItems(self, list_widget, state):
        for item in list_widget.findItems('*', QtCore.Qt.MatchWildcard):
            if state:
                item.setCheckState(QtCore.Qt.Checked)
            else:
                item.setCheckState(QtCore.Qt.Unchecked)
        self.enableCalculations()

    def enableCalculations(self):
        if self.getSelectedDPS():
            self.mainW.btnRunLdf.setEnabled(True)
            self.mainW.btnRunShc.setEnabled(True)
            self.mainW.btnDefCont.setEnabled(True)
            self.mainW.btnRunCont.setEnabled(self.getSelectedContingency())
            self.mainW.btnCreateStandAloneCases.setEnabled(self.getSelectedRMS())
            self.mainW.cbAppendStandAloneCases.setEnabled(self.getDSALightCases())
            self.mainW.btnRunRMS.setEnabled(self.getSelectedRMS() and self.ENABLE['RMS'])

            if hasattr(self, 'GekkoModel'):
                for idx1 in range(self.GekkoModel.rowCount()):
                    iCounter = 0
                    for idx2 in range(self.GekkoModel.item(idx1).rowCount()):
                        starttime = int(time.mktime(time.strptime(self.GekkoModel.item(idx1).child(idx2, 1).text(), '%Y-%m-%d %H:%M:%S')))
                        endtime = int(time.mktime(time.strptime(self.GekkoModel.item(idx1).child(idx2, 2).text(), '%Y-%m-%d %H:%M:%S')))
                        for PIT in self.selectedDPS:
                            if starttime <= PIT <= endtime:
                                iCounter += 1
                                self.GekkoModel.item(idx1).child(idx2, 0).setCheckState(QtCore.Qt.Checked)
                                self.mainW.treeGekko.setRowHidden(idx1, self.GekkoModel.item(idx1).index().parent(), False)
                                self.mainW.treeGekko.setRowHidden(idx2, self.GekkoModel.item(idx1).index(), False)
                                break
                            else:
                                self.GekkoModel.item(idx1).child(idx2, 0).setCheckState(QtCore.Qt.Unchecked)
                                self.mainW.treeGekko.setRowHidden(idx2, self.GekkoModel.item(idx1).index(), True)
                    if iCounter == 0:
                        self.mainW.treeGekko.setRowHidden(idx1, self.GekkoModel.item(idx1).index().parent(), True)
                    else:
                        self.GekkoModel.item(idx1).setText('{0} ({1} out of {2})'.format(self.GekkoModel.item(idx1).text().split(' (')[0], iCounter, self.GekkoModel.item(idx1).rowCount()))
                self.mainW.treeGekko.expandAll()
                self.mainW.treeGekko.resizeColumnToContents(0)
        else:
            if hasattr(self, 'GekkoModel'):
                for idx1 in range(self.GekkoModel.rowCount()):
                    self.GekkoModel.item(idx1).setText('{0} ({1})'.format(self.GekkoModel.item(idx1).text().split(' (')[0], self.GekkoModel.item(idx1).rowCount()))
                    self.mainW.treeGekko.setRowHidden(idx1, self.GekkoModel.item(idx1).index().parent(), False)
                    for idx2 in range(self.GekkoModel.item(idx1).rowCount()):
                        self.GekkoModel.item(idx1).child(idx2, 0).setCheckState(QtCore.Qt.Unchecked)
                        self.mainW.treeGekko.setRowHidden(idx2, self.GekkoModel.item(idx1).index(), False)
                self.mainW.treeGekko.resizeColumnToContents(0)
            self.mainW.treeGekko.collapseAll()
            for element in (self.mainW.btnRunLdf, self.mainW.btnDefCont,
                            self.mainW.btnRunCont, self.mainW.btnRunShc,
                            self.mainW.btnRunRMS,
                            self.mainW.btnCreateStandAloneCases,
                            self.mainW.cbAppendStandAloneCases):
                element.setEnabled(False)

    def showPf(self):
        self.app.Show()
        self.mainW.actShowPf.setEnabled(False)
        self.mainW.actHidePf.setEnabled(True)

    def hidePf(self):
        self.app.Hide()
        self.mainW.actShowPf.setEnabled(True)
        self.mainW.actHidePf.setEnabled(False)

    def getPfCredentials(self):
        if self.isUserGiven:
            self.startPf()
        else:
            USER = os.environ['USERNAME'].split('-')[0]
            self.pfUserInfoForm.edtUsername.setText(USER.upper())
            self.pfUserInfoForm.edtPassword.setText(USER.lower())
            self.pfUserInfoForm.show()

    def startPf(self):
        self.mainW.tabOutput.setCurrentIndex(0)
        if not self.isUserGiven:
            self.username = self.pfUserInfoForm.edtUsername.text()
            self.password = self.pfUserInfoForm.edtPassword.text()
            if self.pfUserInfoForm.cBApplySettings.isChecked():
                with open(self.CONFIGURATION_FILE, 'r+', encoding='utf-8') as fid:
                    content = fid.read().split('\n')
                    for idx, line in enumerate(content):
                        if line.startswith('username'):
                            content[idx] = 'username = {0}'.format(self.username)
                        elif line.startswith('password'):
                            content[idx] = 'password = {0}'.format(self.password)
                    fid.seek(0)
                    fid.write('\n'.join(content))

        self.updateGUI('Starting PowerFactory', 0)
        tic = time.time()
        try:
            if self.pfIniFile is None:
                self.app = self.powerfactory.GetApplicationExt(self.username, self.password)
            else:
                self.app = self.powerfactory.GetApplicationExt(self.username, self.password, '/ini "{0}"'.format(self.pfIniFile))
        except self.powerfactory.ExitError as error:
            self.updateGUI('ERROR: PowerFactory was not able to start. {0} !'.format(self.PowerFactoryErrorCode[error.code]), 100)
        else:
            self.PF_VERSION = Version(self.app.__version__)
            self.updateGUI('PowerFactory v{0} started in {1:.1f} [s], PowerFactory user: {2}'.format(self.PF_VERSION.public, time.time() - tic, self.app.GetCurrentUser().GetAttribute('e:loc_name')), 25)
            self.mainW.actShowPf.setEnabled(True)
            self.mainW.btnStartPf.setEnabled(False)
            self.mainW.cbITCStudyCase.setEnabled(False)
            self.mainW.cbCreateDerivedProject.setEnabled(False)
            if not self.app.GetCurrentUser().GetAttribute('e:stab'):
                self.updateGUI('ERROR: RMS license has not been assigned - RMS calculation module will not be available to use', None)
                self.ENABLE['RMS'] = False
            self.activateProject()

    def activateProject(self):
        if self.mainW.cbCreateDerivedProject.isChecked():
            self.app.SetShowAllUsers(1)
            oIntUser = next(oIntUser for oIntUser in self.app.GetAllUsers() if oIntUser.GetAttribute('e:loc_name') == 'ELvis')
            self.app.SetShowAllUsers(0)
            oIntVersion = oIntUser.GetContents('ENDK*.IntPrj')[0].GetLatestVersion()
            oIntPrj = oIntVersion.CreateDerivedProject('DSAlight_{0}'.format(time.strftime('%Y-%m-%d-%H_%M_%S', time.localtime())),self.app.GetCurrentUser())
            self.updateGUI('Created derived project: "{0}" from version: "{1}"'.format(oIntPrj.GetAttribute('e:loc_name'), oIntVersion.GetAttribute('e:loc_name')), None)
            with open(self.CONFIGURATION_FILE, 'r+', encoding='utf-8') as fid:
                content = fid.read().split('\n')
                for idx, line in enumerate(content):
                    if line.startswith('projectName'):
                        content[idx] = 'projectName = {0}'.format(oIntPrj.GetAttribute('e:loc_name'))
                        break
                fid.seek(0)
                fid.write('\n'.join(content))

        else:
            oIntPrj = self.app.GetCurrentUser().SearchObject(self.projectName)

        if oIntPrj is not None:
            self.updateGUI('Activating project', 50)
            tic = time.time()
            if oIntPrj.Activate() == 0:
                self.oIntPrj = oIntPrj
                self.updateGUI('Project "{0}" activated in {1:.1f} [s]'.format(oIntPrj.GetAttribute('e:loc_name'), time.time() - tic), 75)
                if oIntPrj.GetAttribute('e:der_baseversion2'):
                    self.updateGUI('WARN: Newer version for "{0}" exists: "{1}". Please consider updating'.format(oIntPrj.GetAttribute('e:loc_name'), oIntPrj.GetAttribute('r:der_baseversion2:e:loc_name')), None)
                if not self.app.GetProjectFolder('ras'):
                    self.app.GetProjectFolder('oplib').CreateObject('IntPrjFolder', 'Remedial Action Schemes').SetAttribute('e:iopt_typ', 'ras')

                self.ProjectFolder = {'Variations': self.app.GetProjectFolder('scheme'),
                                      'Study Cases': self.app.GetProjectFolder('study'),
                                      'DPL-koder': self.app.GetProjectFolder('script'),
                                      'Faults': self.app.GetProjectFolder('fault'),
                                      'RAS': self.app.GetProjectFolder('ras'),
                                      'Variation 07 DSAlight': self.app.SearchObjectByForeignKey('485bbf9e7d0cd0f22efc'),
                                      '001 DELFIN DK': self.app.SearchObjectByForeignKey('43bba3c5152775399d06'),
                                      '001 GEKKO DK': self.app.SearchObjectByForeignKey('47a3b42506076fb4dc99'),
                                      '000_Standard Bruger': self.app.SearchObjectByForeignKey('42f9a8affabdf95fc1a5'),
                                      'HVDC_Adjust': self.app.GetProjectFolder('script').GetContents('005 Dynamiske Beregninger')[0].GetContents('HVDC_*Adjust*.ComDpl', 1)[0],
                                      'RMS_Adjust': self.app.GetProjectFolder('script').GetContents('005 Dynamiske Beregninger')[0].GetContents('Klargør_RMS_beregninger*.ComDpl', 1)[0]}

                for oComDpl in self.ProjectFolder['HVDC_Adjust'].GetContents('*.ComDpl'):
                    iErr, _ = oComDpl.GetInputParameterInt('iInput')
                    if not iErr:
                        oComDpl.SetInputParameterInt('iInput', 0)

                if self.mainW.cbITCStudyCase.isChecked():
                    self.ProjectFolder['StudyCase_DK1'] = self.ProjectFolder['Study Cases'].GetContents('DK1 ITC.IntCase', 1)[0]
                    self.ProjectFolder['StudyCase_DK2'] = self.ProjectFolder['Study Cases'].GetContents('DK2 ITC.IntCase', 1)[0]
                else:
                    if self.studyCaseDk1.startswith('##'):
                        self.ProjectFolder['StudyCase_DK1'] = self.app.SearchObjectByForeignKey(self.studyCaseDk1[2:])
                    else:
                        self.ProjectFolder['StudyCase_DK1'] = self.ProjectFolder['Study Cases'].SearchObject(self.studyCaseDk1)

                    if self.studyCaseDk2.startswith('##'):
                        self.ProjectFolder['StudyCase_DK2'] = self.app.SearchObjectByForeignKey(self.studyCaseDk2[2:])
                    else:
                        self.ProjectFolder['StudyCase_DK2'] = self.ProjectFolder['Study Cases'].SearchObject(self.studyCaseDk2)

                self.activateStudyCase()
            else:
                self.updateGUI('ERROR: Project "{0}" could not be activated !'.format(oIntPrj.GetAttribute('e:loc_name')), 100)
        else:
            self.updateGUI('ERROR: Project "{0}" not found !'.format(self.projectName), 100)

    def activateStudyCase(self):
        if hasattr(self, 'app') and hasattr(self, 'oIntPrj'):
            self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
            if self.mainW.rbDK1.isChecked():
                oStudyCase = self.ProjectFolder['StudyCase_DK1']
                aStudyCase = self.studyCaseDk1
                self.GRID = 'DK1'
            else:
                oStudyCase = self.ProjectFolder['StudyCase_DK2']
                aStudyCase = self.studyCaseDk2
                self.GRID = 'DK2'

            if oStudyCase == self.app.GetActiveStudyCase():
                self.updateGUI('Study Case "{0}" is already activated !'.format(oStudyCase.GetAttribute('e:loc_name')), 100)
                iErr = 0
            elif oStudyCase is None:
                self.updateGUI('ERROR: Study Case "{0}" not found!'.format(aStudyCase), 100)
                iErr = 1
            else:
                self.updateGUI('Activating Study Case', 75)
                tic = time.time()
                iErr = oStudyCase.Activate()
                if iErr == 0:
                    self.updateGUI('Study Case "{0}" activated in {1:.1f} [s]'.format(oStudyCase.GetAttribute('e:loc_name'), time.time() - tic), 100)
                else:
                    self.updateGUI('ERROR: Study Case "{0}" could not be activated!'.format(oStudyCase.GetAttribute('e:loc_name')), 100)

            if iErr == 0:
                self.oStudyCase = oStudyCase
                self.updateDPSList()
                self.updateRMSList()
                self.updateGekkoList()

            for element in (self.mainW.btnGetGekkoData,
                            self.mainW.gB_AdvancedOptions, self.mainW.gB_Calculations,
                            self.mainW.gB_DPSList, self.mainW.gB_GekkoList, self.mainW.gb_RMSList,
                            self.mainW.btnContReport):
                element.setEnabled(iErr == 0)

            for element in (self.mainW.btnGetDpsData, self.mainW.btnGetDelfinData, self.mainW.cbSMTNETScheduleScan):
                element.setEnabled(self.ENABLE['Da2Prog'])

    def SMTNETScheduleControl(self):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
        if self.mainW.cbSMTNETScheduleScan.isChecked():
            self.SMTNETScheduleTIC = time.time()
            self.SMTNETScheduleTimer.start(5000)
            self.updateGUI('SMTNET Schedule Scan has been started - waiting for DPS jobs', 0)
        else:
            self.SMTNETScheduleTimer.stop()
            self.updateGUI('SMTNET Schedule Scan has been stoped', 100)

    def SMTNETScheduleScan(self):
        self.SMTNETSchedulePath = os.path.join(self.SMTNETSchedule, *time.strftime('%Y,%#m,%#d', time.gmtime()).split(','))
        DPS_SCHEDULES = []
        for entry1 in (entry1 for entry1 in os.scandir(self.SMTNETSchedulePath) if entry1.is_dir()):
            for entry2 in os.scandir(entry1):
                if entry2.is_file() and entry2.name.endswith('.zip') and entry2.stat()[-1] > self.SMTNETScheduleTIC:
                    DPS_SCHEDULES.append(entry2.path)

        if not DPS_SCHEDULES:
            return

        DPS_SCHEDULES.sort(key=lambda x: os.path.getctime(x))
        self.SMTNETScheduleTIC = os.path.getctime(DPS_SCHEDULES[0])
        with zipfile.ZipFile(DPS_SCHEDULES[0]) as zf:
            CFGTP = zf.read('CFGTP.csv').decode().split('\n')[-1]
            ControlXMLfile = zf.read(next(file for file in zf.namelist() if file.startswith('CONTROL_'))).decode()
        ControlXMLelement = ET.fromstring(ControlXMLfile)
        SCHEDULED = time.strftime('%d-%m-%Y %H:%M:%S', time.localtime(self.SMTNETScheduleTIC))
        USER = ControlXMLelement.find('TITLE').text.split('_')[-1]
        MODE = ControlXMLelement.find('MODE').text
        CONGRP = ControlXMLelement.find('CONGRP').text
        MONGRP = ControlXMLelement.find('MONGRP').text

        STARTDATE = CFGTP.split(',')[1].split(',')[0]

        self.updateGUI('SMTNET job detected: USER = {0}, SCHEDULED = {1}, STARTDATE = {2}, MODE = {3}, CONGRP = {4}, MONGRP = {5}'.format(USER, SCHEDULED, STARTDATE, MODE, CONGRP, MONGRP), None)
        if self.username == USER:
            self.mainW.cbSMTNETScheduleScan.setChecked(False)
            self.updateGUI('Welcome VHI', None)
            if 'DK1' in MONGRP:
                self.mainW.rbDK1.setChecked(True)
            else:
                self.mainW.rbDK2.setChecked(True)

            for entry in os.scandir(os.path.dirname(DPS_SCHEDULES[0])):
                if entry.is_file() and entry.name.startswith('DPS Message') and entry.name.endswith('.xml'):
                    self.getDPSData(entry.path)
                    break

    def createStandAloneCases(self, output_tab):
        self.mainW.tabOutput.setCurrentIndex(output_tab)
        self.updateGUI('Prepare "Stand Alone Cases"', 0)
        tic = time.time()

        iStudyCase = self.oStudyCase.GetAttribute('e:iStudyTime')
        metadata = ['DSAlight v{0}\nUser: {1}\nCreated: {2}'.format(self.__version__, os.environ['USERNAME'], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()))]

        if not self.mainW.cbAppendStandAloneCases.isEnabled() or not self.mainW.cbAppendStandAloneCases.isChecked():

            self.updateGUI('Delete already existing Stand Alone Cases', 50)
            self.deleteFolder(self.ProjectFolder['Study Cases'].GetContents('DSALightCases*.IntFolder') +
                              self.ProjectFolder['Variation 07 DSAlight'].GetContents('*'))
            oDSAFolder = self.ProjectFolder['Study Cases'].CreateObject('IntFolder', 'DSALightCases')
            oDSAFolder.SetAttribute('e:desc', metadata)

            self.oStudyCase.SetStudyTime(self.selectedDPS[0])

            sInSstage = self.ProjectFolder['000_Standard Bruger'].GetContents('*.IntSstage', 1)
            self.app.SetWriteCacheEnabled(1)
            for idx, iSelctedTime in enumerate(self.selectedDPS, start=1):
                for oIntSstage in sInSstage:
                    if oIntSstage.GetAttribute('e:tAcTime') == iSelctedTime:
                        oIntScheme = oIntSstage.GetVariation()
                        oIntScheme.Deactivate()
                        oIntSstage.SetAttribute('e:tAcTime', iSelctedTime - 1)
                        oIntScheme.Activate()
                        break
                oNewStudyCase = oDSAFolder.AddCopy(self.oStudyCase, '{0}_EB{1:02d}'.format(self.GRID, idx))
                oNewStudyCase.SetAttribute('e:desc', ['{0}\nDPS Time: {1}'.format(metadata[0], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(iSelctedTime)))])
                oNewStudyCase.SetStudyTime(iSelctedTime)
                self.updateGUI('Create seperate StudyCase: {0}'.format(oNewStudyCase.GetAttribute('e:loc_name')), None)

            self.app.WriteChangesToDb()
            self.app.SetWriteCacheEnabled(0)
            self.oStudyCase.SetStudyTime(iStudyCase)

        else:
            oDSAFolder = self.ProjectFolder['Study Cases'].GetContents('DSALightCases*.IntFolder')[0]

            for idx, iSelctedTime in enumerate(self.selectedDPS, start=len(self.DSALightCases)+1):
                oNewStudyCase = oDSAFolder.AddCopy(self.oStudyCase, '{0}_EB{1:02d}'.format(self.GRID, idx))
                oNewStudyCase.SetAttribute('e:desc', ['{0}\nDPS Time: {1}'.format(metadata[0], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(iSelctedTime)))])
                oNewStudyCase.SetStudyTime(iSelctedTime)
                self.updateGUI('Append seperate StudyCase: {0}'.format(oNewStudyCase.GetAttribute('e:loc_name')), None)

        self.updateGUI('Delete not selected RMS Short Circuit Definitions', 75)
        for oIntEvt in (oIntEvt for oIntEvt in oDSAFolder.GetContents('*.IntEvt', 1)
                        if oIntEvt.GetAttribute('e:loc_name') not in self.selectedRMS):
            oIntEvt.Delete()

        self.updateGUI('Prepare variations for each study case - execute DPL scripts: "{0}" & "{1}"'.
                       format(self.ProjectFolder['HVDC_Adjust'].GetAttribute('e:loc_name'),
                              self.ProjectFolder['RMS_Adjust'].GetAttribute('e:loc_name')), 95)
        self.ProjectFolder['RMS_Adjust'].SetInputParameterString('aIntScheme', '01 SetUp RMS')

        for idx, oNewStudyCase in enumerate(oDSAFolder.GetContents('*.IntCase'), start=1):
            if oNewStudyCase not in self.DSALightCases:
                oNewStudyCase.Activate()
                self.checkAvailableDelfinData()
                oVariationFolder = self.ProjectFolder['Variation 07 DSAlight'].CreateObject('IntFolder', '{0:03d} {1}'.format(idx, oNewStudyCase.GetAttribute('e:loc_name')))
                oVariationFolder.SetAttribute('e:desc', metadata)

                sIntScheme = [oIntScheme for oIntScheme in self.app.GetActiveNetworkVariations() if oIntScheme.GetAttribute('e:loc_name').startswith('Scenarier_20')]
                self.deactivateIntScheme(sIntScheme)
                self.ProjectFolder['RMS_Adjust'].SetExternalObject('oIntFolder_ext', oVariationFolder)
                self.ProjectFolder['RMS_Adjust'].Execute()
                self.activateIntScheme(sIntScheme)

                oIntScheme = oVariationFolder.CreateObject('IntScheme', '02 Adjust RMS')
                oIntScheme.NewStage('01 Adjust HVDC_RPC', oNewStudyCase.GetAttribute('e:iStudyTime') + 61, 1)
                self.ProjectFolder['HVDC_Adjust'].Execute()
                oIntScheme.NewStage('02 Adjust LDF', oNewStudyCase.GetAttribute('e:iStudyTime') + 1, 0)
                oIntScheme.NewStage('99 Run RMS', oNewStudyCase.GetAttribute('e:iStudyTime') + 58, 1)
        self.oStudyCase.Activate()
        self.updateGUI('Finished StandAloneCases in {0}'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)

    def cleanAllData(self):
        self.deactivateIntScheme(self.ProjectFolder['001 DELFIN DK'].GetContents('*.IntScheme') +
                                 self.ProjectFolder['Variations'].GetContents('DPS_Variation*.IntScheme'), True)

        self.deactivateIntScheme(self.ProjectFolder['Variations'].GetContents('Scenarier_2*.IntScheme', 1))
        self.updateDPSList()

    def checkAvailableDelfinData(self, iStudyCase=None):
        if iStudyCase is None:
            iStudyCase = self.app.GetActiveStudyCase().GetAttribute('e:iStudyTime')

        for oIntScheme in self.ProjectFolder['001 DELFIN DK'].GetContents('*.IntScheme'):
            if oIntScheme.GetAttribute('e:tFromAc') <= iStudyCase <= oIntScheme.GetAttribute('e:tToAc'):
                oIntScheme.Activate()
                self.updateGUI('Activate Delfin data: {0}'.format(oIntScheme.GetAttribute('e:loc_name')), None)
            else:
                oIntScheme.Deactivate()

    def getDPSData(self, DPSXMLfile=None):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
        self.updateGUI('Import DPS data', 0)
        if DPSXMLfile is None:
            DPSXMLfile = QtWidgets.QFileDialog.getOpenFileName(self.mainW, 'Select DPS xml file', self.SMTNETSchedulePath, 'Extensible Markup Language file (*.xml)')[0]
        if os.path.isfile(DPSXMLfile):
            DPSXMLfile = os.path.abspath(DPSXMLfile)
            for entry in os.scandir(os.path.dirname(DPSXMLfile)):
                if entry.is_file() and entry.name.endswith('.zip'):
                    with zipfile.ZipFile(entry) as zf:
                        ControlXMLfile = zf.read(next(file for file in zf.namelist() if file.startswith('CONTROL_'))).decode()
                    ControlXMLelement = ET.fromstring(ControlXMLfile)

                    if self.GRID not in ControlXMLelement.find('MONGRP').text:
                        ret = QtWidgets.QMessageBox.warning(self.mainW, self.mainW.windowTitle(),
                                                            'Selected DPS data "{0}" does not contain data for selected area "{1}". Do You want to continue ?'.format(DPSXMLfile, self.GRID),
                                                            QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                        if ret == QtWidgets.QMessageBox.No:
                            self.updateGUI('WARN: DPS import aborted due to inconsistent data !', 100)
                            return
                        else:
                            self.updateGUI('WARN: Continue DPS import despite inconsistent data !', 100)
                    break

            tic = time.time()
            self.cleanAllData()
            DPS_Import = DPS(self.app, self.D2P_Application, DPSXMLfile, self.mapping_file, self.output_folder, self.mainW.listOutImport, self.mainW.progressbar)
            self.updateGUI('Da2Prog needed {0}'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)
            self.updateDPSList(CFGTP=DPS_Import.CFGTP)

            if DPS_Import.GekkoXMLfile is not None and os.path.isfile(DPS_Import.GekkoXMLfile):
                self.getGekkoData(DPS_Import.GekkoXMLfile)
                try:
                    os.remove(DPS_Import.GekkoXMLfile)
                except OSError:
                    pass
            else:
                self.updateGUI('WARN: None Gekko input file has been found inside DPS!', 100)
        else:
            self.updateGUI('ERROR: None DPS xml file has been selected!', 100)

    def getGekkoData(self, GekkoXMLfile=None):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
        self.updateGUI('Import Gekko data', 0)
        if GekkoXMLfile is None:
            GekkoXMLfile = QtWidgets.QFileDialog.getOpenFileName(self.mainW, 'Select Gekko xml file', '', 'Extensible Markup Language file (*.xml)')[0]
        if os.path.isfile(GekkoXMLfile):
            GekkoXMLfile = os.path.abspath(GekkoXMLfile)
            tic = time.time()
            Gekko_Outages = ET.parse(GekkoXMLfile).getroot().findall('Outage')
            Gekko_Amount = len(Gekko_Outages)
            if Gekko_Amount:
                iCounter = 0
                self.deactivateIntScheme(self.ProjectFolder['001 GEKKO DK'].GetContents('*.IntScheme'), True)
                sElmSite = {oElm.GetAttribute('e:loc_name') for oElm in self.app.GetCalcRelevantObjects('*.ElmSite')}
                PF_CIMID = {oElm.GetAttribute('e:cimRdfId:0'): oElm for oElm in self.app.GetCalcRelevantObjects('*.ElmBranch,*.ElmSvs,*.ElmShnt,*.ElmTr*,*.ElmSym,*.ElmGenstat,*.ElmAsm,*.ElmCoup')}
                self.updateGUI('Create PowerFactory outages from Gekko xml file: {0}'.format(GekkoXMLfile), 0)
                self.app.SetWriteCacheEnabled(1)
                for idx, outage in enumerate(Gekko_Outages, start=1):
                    self.updateGUI(None, idx/Gekko_Amount*100)
                    EquipmentMRId = '_{0}'.format(outage.find('EquipmentMRId').text)
                    EquipmentId = outage.find('EquipmentId').text
                    FromStation = outage.find('FromStation').text
                    if EquipmentMRId in PF_CIMID:
                        WorkStartUTC = int(time.mktime(time.strptime(outage.find('WorkStart').text, '%Y-%m-%dT%H:%M:%SZ')))
                        WorkEndUTC = int(time.mktime(time.strptime(outage.find('WorkEnd').text, '%Y-%m-%dT%H:%M:%SZ')))
                        try:
                            Description = outage.find('ShortDescription').text
                        except Exception:
                            Description = 'Has not been able to set description for "{0} / {1}" with MRId "{2}" --> Check input XML Gekko file: {3}'.format(FromStation, EquipmentId, EquipmentMRId, GekkoXMLfile)
                            self.updateGUI('WARN: {0}'.format(Description), None)

                        oIntScheme = self.ProjectFolder['001 GEKKO DK'].CreateObject('IntScheme', PF_CIMID[EquipmentMRId].GetAttribute('e:loc_name'))
                        oIntScheme.SetAttribute('e:iStartTime', WorkStartUTC)
                        oIntScheme.SetAttribute('e:iEndTime', WorkEndUTC + 1)
                        oIntScheme.SetAttribute('e:iValPeriod', 1)
                        oIntScheme.SetAttribute('e:desc', [Description])

                        oIntSstage = oIntScheme.CreateObject('IntSstage', PF_CIMID[EquipmentMRId].GetAttribute('e:loc_name'))
                        oIntSstage.SetAttribute('e:tAcTime', WorkStartUTC)
                        oIntSstage.SetAttribute('e:desc', [Description])
                        oIntSstage.CreateStageObject(1, PF_CIMID[EquipmentMRId])
                        oIntScheme.Activate()

                        iCounter += 1
                    else:
                        if FromStation in sElmSite:
                            self.updateGUI('WARN: MRId "{0}" for "{1} / {2}" has not been found in "{3}"'.format(EquipmentMRId, FromStation, EquipmentId, self.oStudyCase.GetAttribute('e:loc_name')), None)
                self.app.WriteChangesToDb()
                self.app.SetWriteCacheEnabled(0)
                self.updateGUI('Imported {0} out of {1} Gekko outages in {2}'.format(iCounter, Gekko_Amount, (time.strftime('%H:%M:%S', time.gmtime(time.time() - tic)))), 100)
                self.updateGekkoList()
            else:
                self.updateGUI('ERROR: Invalid Gekko input file - no instances of class "Outage" is found!', 100)
        else:
            self.updateGUI('ERROR: None Gekko input file has been selected!', 100)

    def getDelfinData(self):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
        self.updateGUI('Import Delfin data', 0)
        tic = time.time()
        self.cleanAllData()
        oComDpl = self.ProjectFolder['DPL-koder'].GetContents('A04 Generelle værktøjer')[0].GetContents('Netsysrapport*.ComDpl')[0]
        oComDpl.SetInputParameterInt('iMode', 3)
        ITC = self.DelfinForm.cb_ITC.isChecked()
        MULTIPLE_NETSYS = self.DelfinForm.cb_MultipleNetsys.isEnabled() and self.DelfinForm.cb_MultipleNetsys.isChecked()
        DELFIN = {key: {'DGS': None, 'PowerFactory': False} for key in (int(time.mktime(time.strptime(self.DelfinForm.listWidget.item(ind).text(), '%Y-%m-%d %H:%M')))
                                                                        for ind in range(self.DelfinForm.listWidget.count()))}
        for idx, his_epoch in enumerate(DELFIN, start=1):
            if idx == 1 or MULTIPLE_NETSYS:
                if ITC:
                    netsysraport = os.path.join(self.output_folder, 'Netsysrapport_{0}_ITC.txt'.format(time.strftime('%Y-%m-%d', time.localtime(his_epoch))))
                    aStudyCase = self.oStudyCase.GetAttribute('e:loc_name')
                    if not aStudyCase.endswith('ITC'):
                        ret = QtWidgets.QMessageBox.warning(self.mainW, self.mainW.windowTitle(),
                                                            'Selected StudyCase "{0}" is not ITC compatible. Do You want to continue ?'.format(aStudyCase),
                                                            QtWidgets.QMessageBox.Reset | QtWidgets.QMessageBox.Ignore | QtWidgets.QMessageBox.Abort)
                        if ret == QtWidgets.QMessageBox.Ignore:
                            self.updateGUI('WARN: Continue Delfin import despite incorrect StudyCase !', None)
                        elif ret == QtWidgets.QMessageBox.Abort:
                            self.updateGUI('WARN: Delfin import aborted due to incorrect StudyCase !', 100)
                            return
                        else:
                            self.updateGUI('WARN: Delfin import aborted due to incorrect StudyCase ! Please correct StudyCase inside INI file', 100)
                            self.settingsDSA()
                            return
                else:
                    netsysraport = os.path.join(self.output_folder, 'Netsysrapport_{0}.txt'.format(time.strftime('%Y-%m-%d_%H-%M', time.localtime(his_epoch))))
                oComDpl.SetInputParameterString('aFile', netsysraport)
                self.updateGUI('Saving Netsysrapport to: {0}'.format(netsysraport), 15)
                self.oStudyCase.SetStudyTime(his_epoch - 1)
                if oComDpl.Execute() != 0:
                    self.updateGUI('ERROR: Netsysrapport failed!', None)
                    continue
            if ITC:
                path = os.path.join(self.output_folder, 'Delfin2PowerFactory {0} ITC'.format(time.strftime('%Y-%m', time.localtime(his_epoch))))
            else:
                path = os.path.join(self.output_folder, 'Delfin2PowerFactory {0}'.format(time.strftime('%Y-%m-%d %H-%M-%S', time.localtime(his_epoch))))
            arg = '"{0}" delfin "{1}" "{2}" "{3}" "{4}" "{5}"'.format(self.D2P_Application, netsysraport, self.mapping_file, his_epoch, ITC, path)
            CREATE_NO_WINDOW = 0x08000000
            self.updateGUI('Calling Da2Prog in order to import Delfin data', 50)
            try:
                subprocess.check_output(arg, creationflags=CREATE_NO_WINDOW)
            except subprocess.CalledProcessError as e:
                if e.returncode != 0:
                    self.updateGUI('WARN: Please check the Da2Prog log file located: {0}'.format(path), None)
            if ITC:
                dgs = os.path.join(path, 'RunITC.dgs')
            else:
                dgs = os.path.join(path, 'Run.dgs')
            if os.path.isfile(dgs):
                DELFIN[his_epoch]['DGS'] = dgs
            if ITC:
                break

        oComImport = self.app.GetFromStudyCase('ComImport')
        oComImport.SetAttribute('e:dgsFormatIdx', 0)
        oComImport.SetAttribute('e:iopt_prj', 1)
        oComImport.SetAttribute('e:pAddPrj', self.oIntPrj)

        for his_epoch in (his_epoch for his_epoch in DELFIN if DELFIN[his_epoch]['DGS'] is not None):
            self.updateGUI('Importing DGS file into PowerFactory: {0}'.format(DELFIN[his_epoch]['DGS']), 75)
            oComImport.SetAttribute('e:fFile', DELFIN[his_epoch]['DGS'])
            if oComImport.Execute() == 0:
                DELFIN[his_epoch]['PowerFactory'] = True
            else:
                self.updateGUI('ERROR: PowerFactory failed to import data from file: {0}'.format(DELFIN[his_epoch]['DGS']), None)

        oComDpl = self.ProjectFolder['DPL-koder'].GetContents('A04 Generelle værktøjer')[0].GetContents('OutOfService*.ComDpl')[0]
        oComDpl.SetInputParameterInt('iClearScreen', 0)
        oComDpl.SetInputParameterInt('iHigh', 3)
        oComDpl.SetInputParameterDouble('fLow', -10)

        self.updateGUI('Execute DPL script: "{0}"'.format(oComDpl.GetAttribute('e:loc_name')), 95)

        sIntScheme = self.ProjectFolder['001 DELFIN DK'].GetContents('*.IntScheme')
        self.deactivateIntScheme(sIntScheme)

        for oIntScheme in sIntScheme:
            sIntSstage = oIntScheme.GetContents('*.IntSstage')
            if len(sIntSstage) == 1:
                oIntScheme.NewStage('01 OutOfService', sIntSstage[0].GetAttribute('e:tAcTime')+1, 1)
                oComDpl.Execute()
                oIntScheme.Deactivate()

        IMPORTED_DGS = [his_epoch+1 for his_epoch in DELFIN if DELFIN[his_epoch]['PowerFactory']]
        if IMPORTED_DGS:
            if ITC:
                self.updateDPSList(epoch=[his_epoch+1 for his_epoch in DELFIN])
                self.updateGUI('Imported ITC Delfin data for {0} in {1}'.format(time.strftime('%Y-%m', time.localtime([*DELFIN][0])), time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)
            else:
                self.updateDPSList(epoch=IMPORTED_DGS)
                if len(IMPORTED_DGS) == 1:
                    self.updateGUI('Imported Delfin timepoint in {0}'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)
                else:
                    self.updateGUI('Imported {0} Delfin timepoints in {1}'.format(len(IMPORTED_DGS), time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)
        else:
            self.updateGUI('ERROR: None of selected Delfin timepoints have been imported', 100)

    def getPlanlaegningsbalancerData(self):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
        self.updateGUI('Importing Revision Plan Balances', 0)
        tic = time.time()
        self.cleanAllData()
        try:
            SetupBalances.Main(self.app, 'DSAlight')
        except Exception:
            self.updateGUI('ERROR: Faild to import Revision Plan Balancess', 100)
        else:
            self.updateGUI('Imported Revision Plan Balancess in {0}'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)
            xl = win32com.client.Dispatch('EXCEL.Application')
            _, YEAR, GRID = xl.ActiveSheet.Name.split(' ')
            try:
                t_act = int(time.mktime(time.strptime(str(xl.ActiveSheet.Cells(1, 1).Value).split('+')[0], '%Y-%m-%d %H:%M:%S')))
            except Exception:
                t_act = int(time.mktime(time.strptime('{0} 1'.format(YEAR), '%Y %H')))
            self.updateDPSList(epoch=[t_act + idx*3600 for idx, elm in enumerate(xl.ActiveSheet.Range('E2:AZ2').Value[0]) if isinstance(elm, (float, str))])
            xl.Quit
            self.oStudyCase.Activate()
            self.deactivateIntScheme(self.ProjectFolder['Variations'].GetContents('Scenarier_2*.IntScheme', 1))
            oIntScheme = self.ProjectFolder['Variations'].GetContents('001 Scenarier {0}.IntFolder'.format(GRID), 1)[0].GetContents('Scenarier_{0}.IntScheme'.format(YEAR))[0]
            oIntScheme.Activate()
            self.enableCalculations()

    def getStateChangesData(self):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
        self.updateGUI('Import State Changes data', 0)
        filename = QtWidgets.QFileDialog.getOpenFileName(self.mainW, 'Select State Changes file', '', 'Excel files (*.xls *.xlsx)')[0]
        if os.path.isfile(filename):
            filename = os.path.abspath(filename)
            tic = time.time()
            self.updateGUI('Importing State Changes from file: {0}'.format(filename), 25)
            self.deleteFolder(self.ProjectFolder['Variation 07 DSAlight'].GetContents('StateChanges*.IntFolder'))
            oVariationFolder = self.ProjectFolder['Variation 07 DSAlight'].CreateObject('IntFolder', 'StateChanges')
            oVariationFolder.SetAttribute('e:desc', ['DSAlight v{0}\nUser: {1}\nImport date: {2}\nInput file: {3}'.format(
                    self.__version__, os.environ['USERNAME'], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()), filename)])
            workbook = xlrd.open_workbook(filename)
            worksheet = workbook.sheet_by_index(0)
            for iCol in range(2, worksheet.ncols):
                oElm = self.app.SearchObjectByForeignKey(worksheet.cell_value(9, iCol))
                valid = int(worksheet.cell_value(6, iCol))
                self.updateGUI(None, iCol/(worksheet.ncols)*100)
                if oElm and valid == 1:
                    self.updateGUI('Creating State Change with name "{0}" for "{1}" '.format(worksheet.cell_value(5, iCol), oElm.GetAttribute('e:loc_name')), None)
                    oIntScheme = oVariationFolder.CreateObject('IntScheme', worksheet.cell_value(5, iCol).replace('=', '-').replace(',', '_'))
                    oIntScheme.SetAttribute('e:iStartTime', int(time.mktime(time.strptime(str(xlrd.xldate_as_tuple(worksheet.cell_value(10, 1), workbook.datemode)), '(%Y, %m, %d, %H, %M, %S)'))))
                    oIntScheme.SetAttribute('e:iEndTime', 1 + int(time.mktime(time.strptime(str(xlrd.xldate_as_tuple(worksheet.cell_value(worksheet.nrows-1, 1), workbook.datemode)), '(%Y, %m, %d, %H, %M, %S)'))))
                    oIntScheme.SetAttribute('e:iValPeriod', 1)
                    oIntScheme.Activate()
                    value_type = worksheet.cell_value(7, iCol)
                    for iRow in range(10, worksheet.nrows):
                        ValidFrom = xlrd.xldate_as_tuple(worksheet.cell_value(iRow, 1), workbook.datemode)
                        oIntSstage = oIntScheme.CreateObject('IntSstage', '{:04d}-{:02d}-{:02d} {:02d}-{:02d}-{:02d}'.format(*ValidFrom))
                        oIntSstage.SetAttribute('e:tAcTime', int(time.mktime(time.strptime(str(ValidFrom), '(%Y, %m, %d, %H, %M, %S)'))))
                        if value_type in ('0|1', 'int'):
                            value = int(worksheet.cell_value(iRow, iCol))
                        else:
                            value = worksheet.cell_value(iRow, iCol)
                        oDelta = oIntSstage.CreateStageObject(2, oElm)
                        exec('oDelta.{0} = value'.format(worksheet.cell_value(8, iCol).replace('e:', '')))
                elif oElm and valid != 1:
                    self.updateGUI('WARN: State Change "{0}" for "{1}" is not valid'.format(worksheet.cell_value(5, iCol), oElm.GetAttribute('e:loc_name')), None)
                else:
                    self.updateGUI('WARN: State Change "{0}" with Foreign Key "{1}" not found'.format(worksheet.cell_value(5, iCol), worksheet.cell_value(9, iCol)), None)
            self.updateGUI('Imported State Changes in {0}'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)
        else:
            self.updateGUI('ERROR: None State Changes file selected', 100)

    def getDSALightCases(self):
        self.DSALightCases = self.ProjectFolder['Study Cases'].GetContents('DSALightCases*.IntFolder')[0].GetContents('*.IntCase') if self.ProjectFolder['Study Cases'].GetContents('DSALightCases*.IntFolder') else []
        return len(self.DSALightCases) > 0

    def updateRMSList(self):
        self.mainW.listRMS.clear()
        self.mainW.btnUncheckAllRMS.setEnabled(False)
        self.mainW.btnCheckAllRMS.setEnabled(False)
        idx = 0
        for idx, oIntEvt in enumerate(self.oStudyCase.GetContents('*.IntEvt', 1), start=1):
            item = QtWidgets.QListWidgetItem(oIntEvt.GetAttribute('e:loc_name'))
            item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
            item.setCheckState(QtCore.Qt.Checked)
            tshortcircuit = []
            for oEvtShc in oIntEvt.GetContents('*.EvtShc'):
                tshortcircuit.append(oEvtShc.GetAttribute('e:time'))
            if not tshortcircuit:
                item.setToolTip('FlatRun')
            else:
                tshortcircuit.sort()
                item.setToolTip('Start: {} [s]\nStop: {} [s] \n\nShort-Circuit: {}'.format(tshortcircuit[0], tshortcircuit[1], oEvtShc.GetAttribute('r:p_target:e:loc_name')))
            self.mainW.listRMS.addItem(item)
        self.updateGUI('Found {0} RMS Short Circuit definitions in "{1}" study case'.format(idx, self.oStudyCase.GetAttribute('e:loc_name')), 100)
        if idx > 0:
            self.mainW.btnUncheckAllRMS.setEnabled(True)
            self.mainW.btnCheckAllRMS.setEnabled(True)
            self.mainW.listRMS.scrollToTop()
        self.enableCalculations()

    def updateDPSList(self, CFGTP=None, epoch=None):
        self.mainW.listDPS.clear()
        self.mainW.btnUncheckAllDPS.setEnabled(False)
        self.mainW.btnCheckAllDPS.setEnabled(False)
        if self.getSelectedContingency():
            self.contFinalize(False)
        if CFGTP is not None:
            if CFGTP:
                split = CFGTP.split(',')
                epoch = []
                if len(split) == 6:
                    try:
                        shortstep = int(float(split[2]))
                        longstep = int(float(split[4]))
                        startTimeStamp = int(time.mktime(time.strptime(split[1], '%d-%m-%Y %H:%M:%S')) + 60*60)
                        for long in range(int(float(split[5]))):
                            for short in range(int(float(split[3])) + 1):
                                epoch.append(startTimeStamp + long*longstep*60 + short*shortstep*60)
                    except Exception:
                        self.updateGUI('ERROR while parsing CFGTP string', 100)
                else:
                    self.updateGUI('CFGTP string has no valid format. 6 values are expected, {0} are given'.format(len(split)), 100)
            else:
                self.updateGUI('No time valid time information available', 100)
        if epoch is not None and any(epoch):
            for value in epoch:
                item = QtWidgets.QListWidgetItem(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(value)))
                item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                item.setCheckState(QtCore.Qt.Checked)
                self.mainW.listDPS.addItem(item)
            self.mainW.btnUncheckAllDPS.setEnabled(True)
            self.mainW.btnCheckAllDPS.setEnabled(True)
        self.enableCalculations()

    def updateGekkoList(self):
        sIntScheme = self.ProjectFolder['001 GEKKO DK'].GetContents('*.IntScheme')
        if not sIntScheme:
            if hasattr(self, 'GekkoModel'):
                self.mainW.treeGekko.setModel(None)
                del self.GekkoModel

            self.mainW.btnCollapseAll.setEnabled(False)
            self.mainW.btnExpandAll.setEnabled(False)
            return

        self.GekkoModel = QtGui.QStandardItemModel()
        self.GekkoModel.setHorizontalHeaderLabels(['Station / Element', 'ValidFrom', 'ValidTo', 'Description'])

        sIntScheme.sort(key=lambda x: x.GetContents('*')[0].GetAttribute('e:loc_name'))
        iIntScheme = len(sIntScheme)

        for idx, oIntScheme in enumerate(sIntScheme, start=1):

            oElement = oIntScheme.GetContents('*')[0].GetContents('*')[0].GetAttribute('e:root_id')
            station = oElement.GetParent().GetAttribute('e:loc_name')

            if idx == 1:
                parent = QtGui.QStandardItem('{0} / {1}'.format(station, oElement.GetAttribute('e:loc_name')))
            else:
                if sIntScheme[idx-2].GetContents('*')[0].GetContents('*')[0].GetAttribute('e:root_id') != oElement:
                    parent.setText('{0} ({1})'.format(parent.text(), parent.rowCount()))
                    self.GekkoModel.appendRow(parent)
                    parent = QtGui.QStandardItem('{0} / {1}'.format(station, oElement.GetAttribute('e:loc_name')))

            CheckBox = QtGui.QStandardItem('')
            CheckBox.setCheckable(True)
            CheckBox.setFlags(CheckBox.flags() ^ QtCore.Qt.ItemIsUserCheckable)

            ValidFrom = QtGui.QStandardItem(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(oIntScheme.GetAttribute('e:iStartTime'))))
            ValidTo = QtGui.QStandardItem(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(oIntScheme.GetAttribute('e:iEndTime')-1)))
            Description = QtGui.QStandardItem(' '.join(oIntScheme.GetAttribute('e:desc')))

            parent.appendRow([CheckBox, ValidFrom, ValidTo, Description])

            for item in (parent, CheckBox, ValidFrom, ValidTo, Description):
                item.setEditable(False)

            if idx == iIntScheme:
                parent.setText('{0} ({1})'.format(parent.text(), parent.rowCount()))
                self.GekkoModel.appendRow(parent)

        self.updateGUI('Found {0} Gekko outage definitions for {1} components in "{2}" study case'.format(idx, self.GekkoModel.rowCount(), self.oStudyCase.GetAttribute('e:loc_name')), 100)
        self.mainW.treeGekko.setModel(self.GekkoModel)
        self.mainW.treeGekko.expandAll()
        for idx in range(5):
            self.mainW.treeGekko.resizeColumnToContents(idx)
        self.mainW.treeGekko.collapseAll()
        self.mainW.treeGekko.setSortingEnabled(True)
        for idx in range(2):
            self.mainW.treeGekko.sortByColumn(idx, QtCore.Qt.SortOrder(0))
        self.mainW.btnCollapseAll.setEnabled(True)
        self.mainW.btnExpandAll.setEnabled(True)
        self.enableCalculations()

    def getSelectedRMS(self):
        self.selectedRMS = [self.mainW.listRMS.item(idx).text() for idx in range(self.mainW.listRMS.count()) if self.mainW.listRMS.item(idx).checkState() == QtCore.Qt.Checked]
        return len(self.selectedRMS) > 0

    def getSelectedDPS(self):
        self.selectedDPS = [int(time.mktime(time.strptime(self.mainW.listDPS.item(idx).text(), '%Y-%m-%d %H:%M:%S'))) for idx in range(self.mainW.listDPS.count()) if self.mainW.listDPS.item(idx).checkState() == QtCore.Qt.Checked]
        return len(self.selectedDPS) > 0

    def getSelectedContingency(self):
        self.ContingencyElmTerm = []
        self.ContingencyElmTr = []
        self.ContingencyElmShnt = []
        self.ContingencyElmBranch = []
        self.ContingencyElmHVDC = []
        self.ContingencyElmGen = []
        self.ContingencyElmPV = []
        self.ContingencySIPS = []
        self.selectedContingency = []

        for element in (self.ContingencyForm.pb_Busbars, self.ContingencyForm.pb_Transformers,
                        self.ContingencyForm.pb_Shunts, self.ContingencyForm.pb_Lines, self.ContingencyForm.pb_HVDC,
                        self.ContingencyForm.pb_Generators, self.ContingencyForm.pb_PV_Generators,
                        self.ContingencyForm.pb_Contingency_Set, self.ContingencyForm.pb_SIPS, self.ContingencyForm.cb_SIPS,
                        self.ContingencyForm.buttonBox.button(QtWidgets.QDialogButtonBox.Ok)):
            element.setEnabled(False)

        for element in (self.ContingencyForm.la_Busbars, self.ContingencyForm.la_Transformers,
                        self.ContingencyForm.la_Shunts, self.ContingencyForm.la_Lines, self.ContingencyForm.la_HVDC,
                        self.ContingencyForm.la_Generators, self.ContingencyForm.la_PV_Generators,
                        self.ContingencyForm.la_Contingency_Set, self.ContingencyForm.la_SIPS):
            element.setText('Selected: N/A')

        if self.ContingencyForm.cb_Busbars.isChecked():
            self.ContingencyElmTerm = [oElm for oElm in self.sElmTerm
                                       if 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('e:uknom') >= self.ContingencyForm.sb_Busbars.value()]
            self.ContingencyForm.la_Busbars.setText('Selected: {0:>5}'.format(len(self.ContingencyElmTerm)))
            if self.ContingencyElmTerm:
                self.ContingencyForm.pb_Busbars.setEnabled(True)

        if self.ContingencyForm.cb_Transformers.isChecked():
            self.ContingencyElmTr = [oElm for oElm in self.sElmTr
                                     if 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('r:bushv:r:cBusBar:e:uknom') >= self.ContingencyForm.sb_Transformers.value()]
            self.ContingencyForm.la_Transformers.setText('Selected: {0:>5}'.format(len(self.ContingencyElmTr)))
            if self.ContingencyElmTr:
                self.ContingencyForm.pb_Transformers.setEnabled(True)

        if self.ContingencyForm.cb_Shunts.isChecked():
            self.ContingencyElmShnt = [oElm for oElm in self.sElmShnt
                                       if 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('e:ushnm') >= self.ContingencyForm.sb_Shunts.value()]
            self.ContingencyForm.la_Shunts.setText('Selected: {0:>5}'.format(len(self.ContingencyElmShnt)))
            if self.ContingencyElmShnt:
                self.ContingencyForm.pb_Shunts.setEnabled(True)

        if self.ContingencyForm.cb_Lines.isChecked():
            self.ContingencyElmBranch = [oElm for oElm in self.sElmBranch
                                         if 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('r:p_conn0:r:bus1:r:cBusBar:e:uknom') >= self.ContingencyForm.sb_Lines.value()]
            self.ContingencyForm.la_Lines.setText('Selected: {0:>5}'.format(len(self.ContingencyElmBranch)))
            if self.ContingencyElmBranch:
                self.ContingencyForm.pb_Lines.setEnabled(True)

        if self.ContingencyForm.cb_HVDC.isChecked():
            self.ContingencyElmHVDC = list(set(oElm.GetParent() for oElm in self.sElmHVDC))
            self.ContingencyForm.la_HVDC.setText('Selected: {0:>5}'.format(len(self.ContingencyElmHVDC)))
            if self.ContingencyElmHVDC:
                self.ContingencyForm.pb_HVDC.setEnabled(True)

        if self.ContingencyForm.cb_Generators.isChecked():
            self.ContingencyElmGen = [oElm for oElm in self.sElmGen
                                      if 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('e:pgini:act') >= self.ContingencyForm.sb_Generators.value()]
            self.ContingencyForm.la_Generators.setText('Selected: {0:>5}'.format(len(self.ContingencyElmGen)))
            if self.ContingencyElmGen:
                self.ContingencyForm.pb_Generators.setEnabled(True)

        if self.ContingencyForm.cb_PV_Generators.isChecked():
            self.ContingencyElmPV = [oElm for oElm in self.sElmPV
                                     if 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80]
            self.ContingencyForm.la_PV_Generators.setText('Selected: {0:>5}'.format(len(self.ContingencyElmPV)))
            if self.ContingencyElmPV:
                self.ContingencyForm.pb_PV_Generators.setEnabled(True)

        self.selectedContingency = self.ContingencyElmTerm + self.ContingencyElmTr + self.ContingencyElmShnt + self.ContingencyElmBranch + \
            self.ContingencyElmHVDC + list(set(self.ContingencyElmGen + self.ContingencyElmPV))

        if self.selectedContingency:
            self.ContingencyForm.la_Contingency_Set.setText('Selected: {0:>5}'.format(len(self.selectedContingency)))
            self.ContingencyForm.pb_Contingency_Set.setEnabled(True)
            self.ContingencyForm.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(True)
            if self.sIntRas:
                self.ContingencyForm.cb_SIPS.setEnabled(True)
                if self.ContingencyForm.cb_SIPS.isChecked():
                    self.ContingencySIPS = self.sIntRas
                    self.ContingencyForm.pb_SIPS.setEnabled(True)
                    self.ContingencyForm.la_SIPS.setText('Selected: {0:>5}'.format(len(self.ContingencySIPS)))

        return len(self.selectedContingency) > 0

    def getSelectedDelfin(self):
        if (self.sender() == self.DelfinForm.cb_MultipleTP and not self.DelfinForm.cb_MultipleTP.isChecked()) or \
           (self.sender() == self.DelfinForm.cb_ITC and (self.DelfinForm.cb_ITC.isChecked() or
                                                         (not self.DelfinForm.cb_ITC.isChecked() and not self.DelfinForm.cb_MultipleTP.isChecked()))):
            self.DelfinForm.listWidget.clear()

        TIME_POINTS = self.DelfinForm.listWidget.count()

        for element in (self.DelfinForm.timeEdit, self.DelfinForm.cb_MultipleTP):
            element.setEnabled(not self.DelfinForm.cb_ITC.isChecked())

        self.DelfinForm.cb_MultipleNetsys.setEnabled(not self.DelfinForm.cb_ITC.isChecked() and self.DelfinForm.cb_MultipleTP.isChecked() and TIME_POINTS > 1)

        if TIME_POINTS:
            self.DelfinForm.la_Selected.setText('Selected: {0}'.format(TIME_POINTS))
            for element in (self.DelfinForm.buttonBox.button(QtWidgets.QDialogButtonBox.Ok),
                            self.DelfinForm.pb_Remove, self.DelfinForm.pb_Delete):
                element.setEnabled(True)
        else:
            self.DelfinForm.la_Selected.setText('Selected: N/A')
            for element in (self.DelfinForm.buttonBox.button(QtWidgets.QDialogButtonBox.Ok),
                            self.DelfinForm.pb_Remove, self.DelfinForm.pb_Delete):
                element.setEnabled(False)
        return TIME_POINTS

    def runLdf(self):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Load Flow'])
        self.updateGUI('Executing Load Flow calculations', 0)
        tic = time.time()

        iStudyTimes = len(self.selectedDPS)

        iStudyCase = self.oStudyCase.GetAttribute('e:iStudyTime')
        oComLdf = self.app.GetFromStudyCase('ComLdf')

        Converged = 0
        InNotConverged = 0
        OutNotConverged = 0

        self.deactivateIntScheme(self.ProjectFolder['Variation 07 DSAlight'].GetContents('Adjust HVDC_RPC*.IntScheme'), True)

        oIntScheme = self.ProjectFolder['Variation 07 DSAlight'].CreateObject('IntScheme', 'Adjust HVDC_RPC')
        oIntSstage = oIntScheme.CreateObject('IntSstage', '01 Adjust HVDC_RPC')

        for idx, StudyTime in enumerate(self.selectedDPS, start=1):
            self.checkAvailableDelfinData(StudyTime)
            aDPSTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(StudyTime))
            oIntScheme.Deactivate()
            oIntSstage.SetAttribute('e:tAcTime', StudyTime)
            oIntScheme.Activate()
            oIntSstage.Activate(1)
            self.ProjectFolder['HVDC_Adjust'].Execute()

            iErr = oComLdf.Execute()
            if iErr == 0:
                Converged += 1
                voltage_low = len([1 for oElm in self.app.GetCalcRelevantObjects('*.ElmTerm', 0)
                                   if oElm.GetAttribute('e:iUsage') == 0 and oElm.GetAttribute('e:uknom') > 100 and 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('m:u') < 0.95])
                voltage_high = len([1 for oElm in self.app.GetCalcRelevantObjects('*.ElmTerm', 0)
                                    if oElm.GetAttribute('e:iUsage') == 0 and oElm.GetAttribute('e:uknom') > 100 and 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('m:u') > 1.05])
                loading = len([1 for oElm in self.app.GetCalcRelevantObjects('*.ElmBranch,*.ElmTr*', 0)
                               if oElm.GetUnom() > 100 and 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.GetAttribute('c:loading') > 80])
                self.updateGUI('Load flow "{0}" succeed.  |  '
                               'Loading violations (>80%): {1:>3}  |  '
                               'Voltage violations: U<0.95pu = {2:>3}  &  U>1.05pu = {3:>3}  |'.format(aDPSTime, loading, voltage_low, voltage_high), idx/iStudyTimes*100)
            elif iErr == 1:
                InNotConverged += 1
                self.updateGUI('WARN: Load flow "{0}" failed (inner loops)'.format(aDPSTime), idx/iStudyTimes*100)
            elif iErr == 2:
                OutNotConverged += 1
                self.updateGUI('WARN: Load flow "{0}" failed (outer loops)'.format(aDPSTime), idx/iStudyTimes*100)
        self.deactivateIntScheme([oIntScheme], True)
        self.oStudyCase.SetStudyTime(iStudyCase)
        self.updateGUI('Number of succeed Load Flow calculations: {0} out of: {1} in {2}'.format(Converged, idx, time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)

    def runCont(self):

        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Contingency'])
        self.updateGUI('Executing Contingency calculations', 0)
        tic = time.time()

        iStudyCase = self.oStudyCase.GetAttribute('e:iStudyTime')

        oComSimoutage = self.app.GetFromStudyCase('ComSimoutage')

        Converged = 0

        self.deactivateIntScheme(self.ProjectFolder['Variation 07 DSAlight'].GetContents('Adjust HVDC_RPC*.IntScheme'), True)

        oIntScheme = self.ProjectFolder['Variation 07 DSAlight'].CreateObject('IntScheme', 'Adjust HVDC_RPC')
        oIntSstage = oIntScheme.CreateObject('IntSstage', '01 Adjust HVDC_RPC')

        iStudyTimes = len(self.selectedDPS)

        oIntFolder = self.ProjectFolder['Faults'].GetContents('ContingencyDSAlight.IntFltcases')[0]

        for idx, StudyTime in enumerate(self.selectedDPS, start=1):
            self.checkAvailableDelfinData(StudyTime)
            aDPSTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(StudyTime))
            oIntScheme.Deactivate()
            oIntSstage.SetAttribute('e:tAcTime', StudyTime)
            oIntScheme.Activate()
            oIntSstage.Activate(1)
            self.ProjectFolder['HVDC_Adjust'].Execute()

            oElmRes = self.app.GetFromStudyCase('Contingency_{0}.ElmRes'.format(time.strftime('%Y-%m-%d %H-%M-%S', time.localtime(StudyTime))))
            oElmRes.SetAttribute('e:calTp', 13)
            oComSimoutage.SetAttribute('e:p_rescnt', oElmRes)

            oComSimoutage.RemoveContingencies()
            if oComSimoutage.Execute():
                self.updateGUI('ERROR: Contingency analysis "{0}" could not be initialised'.format(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(StudyTime))), idx/iStudyTimes*100)
                continue

            for iComOutage, oElm in enumerate(oIntFolder.GetContents('*.IntEvt'), start=1):
                oComOutage = oComSimoutage.CreateObject('ComOutage', oElm.GetAttribute('e:loc_name'))
                oComOutage.SetAttribute('e:number', iComOutage)
                oComOutage.SetAttribute('e:cpCase', oElm)

            if oComSimoutage.Execute() == 0:
                Converged += 1
                voltage_low = len([1 for oElm in self.app.GetCalcRelevantObjects('*.ElmTerm', 0)
                                   if oElm.GetAttribute('e:iUsage') == 0 and oElm.GetAttribute('e:uknom') > 100 and 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.HasAttribute('m:min_v') and oElm.GetAttribute('m:min_v') < 0.95])
                voltage_high = len([1 for oElm in self.app.GetCalcRelevantObjects('*.ElmTerm', 0)
                                    if oElm.GetAttribute('e:iUsage') == 0 and oElm.GetAttribute('e:uknom') > 100 and 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.HasAttribute('m:max_v') and oElm.GetAttribute('m:max_v') > 1.05])
                loading = len([1 for oElm in self.app.GetCalcRelevantObjects('*.ElmBranch,*.ElmTr*', 0)
                               if oElm.GetUnom() > 100 and 70 <= int(oElm.GetAttribute('r:cpGrid:e:chr_name') or 0) <= 80 and oElm.HasAttribute('c:maxLoading') and oElm.GetAttribute('c:maxLoading') > 80])
                self.updateGUI('Contingency calculation "{0}" with {1} N-1 succeed.  |  '
                               'Loading violations (>80%): {2:>3}  |  '
                               'Voltage violations: U<0.95pu = {3:>3}  &  U>1.05pu = {4:>3}  |'.format(aDPSTime, iComOutage, loading, voltage_low, voltage_high), idx/iStudyTimes*100)
            else:
                self.updateGUI('WARN: Contingency calculation "{0}" with {1} N-1 failed'.format(aDPSTime, iComOutage), idx/iStudyTimes*100)
        self.deactivateIntScheme([oIntScheme], True)
        self.oStudyCase.SetStudyTime(iStudyCase)
        self.updateGUI('Number of succeed Contingency calculations: {0} out of: {1} in {2}'.format(Converged, idx, time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)

    def runRMS(self):
        tic = time.time()
        self.createStandAloneCases(self.outputTab['RMS'])

        self.ProjectFolder['Study Cases'].GetContents('DSALightCases.IntFolder')[0].GetContents('*.IntCase')[0].Activate()

        oComDpl = self.ProjectFolder['DPL-koder'].GetContents('005 Dynamiske Beregninger')[0].GetContents('SetUpTaskAutomation*.ComDpl', 1)[0]
        self.updateGUI('Configuration of TaskAutomation - execute DPL script: "{0}"'.format(oComDpl.GetAttribute('e:loc_name')), 25)

        oComDpl.SetInputParameterString('aDirectory', self.rmsOutputFilePath)
        oComDpl.SetInputParameterDouble('fSimStop', float(self.runtime))
        oComDpl.SetInputParameterInt('iSstart', 1)
        oComDpl.SetInputParameterInt('iSstop', -1)
        oComDpl.SetInputParameterInt('iCstart', 1)
        oComDpl.SetInputParameterInt('iCstop', -1)
        oComDpl.SetInputParameterInt('iSavePlotsLayout', 0)

        self.updateGUI('Executing TaskAutomation', 50)

        oComDpl.Execute()

        oComPython = oComDpl.GetContents('TASP.ComPython')[0]
        _, oComTasks = oComPython.GetExternalObject('oComTasks')
        TASP(self.app, oComTasks, self.rmsOutputFilePath)

        self.oStudyCase.Activate()
        self.updateGUI('Finished {0} RMS calculations in {1}'.format(len(self.selectedDPS)*len(self.selectedRMS), time.strftime('%H:%M:%S', time.gmtime(time.time() - tic))), 100)

    def contPrepare(self):

        # TODO GetMinDistance()

        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Contingency'])
        self.updateGUI('Contingency configuration started', 15)
        for element in (self.mainW.btnDefCont, self.mainW.btnRunCont):
            element.setEnabled(False)
        iStudyCase = self.oStudyCase.GetAttribute('e:iStudyTime')
        self.oStudyCase.SetStudyTime(self.selectedDPS[0])

        self.sElmTerm = [oElm for oElm in self.app.GetCalcRelevantObjects('*.ElmTerm', 0) if oElm.GetAttribute('e:iUsage') == 0]
        self.sElmTr = self.app.GetCalcRelevantObjects('*.ElmTr*', 0)
        self.sElmShnt = self.app.GetCalcRelevantObjects('*.ElmShnt', 0)
        self.sElmBranch = [oElm for oElm in self.app.GetCalcRelevantObjects('*.ElmBranch', 0) if oElm.GetAttribute('e:cIsInfoOk')]
        self.sElmHVDC = [oElm for oElm in self.app.GetCalcRelevantObjects('dc_*.ElmGenstat,*.ElmVsc,*.ElmRec', 0)
                         if oElm.GetClassName() == 'ElmGenstat' or oElm.GetAttribute('e:c_pmod') and 'ComModel DK' in oElm.GetAttribute('r:c_pmod:r:cpGrid:e:loc_name')]
        self.sElmGen = self.app.GetCalcRelevantObjects('*.ElmSym,i_*.ElmGenstat,*.ElmAsm', 0)
        self.sElmPV = [oElm for oElm in self.app.GetCalcRelevantObjects('*.ElmSym', 0) if oElm.GetAttribute('e:av_mode') == 'constv']
        self.sIntRas = self.ProjectFolder['RAS'].GetContents('*.IntRas', 1)

        self.oStudyCase.SetStudyTime(iStudyCase)
        self.ContingencyForm.show()

    def contShow(self, components, mode):
        self.showPf()
        self.app.ShowModelessBrowser(components, mode)

    def contCreate(self):

        self.deleteFolder(self.ProjectFolder['Faults'].GetContents('ContingencyDSAlight*.IntFltcases'))
        oIntFolder = self.ProjectFolder['Faults'].CreateObject('IntFltcases', 'ContingencyDSAlight')

        manual = self.ContingencyElmHVDC + self.ContingencyElmBranch
        auto = [oElm for oElm in self.selectedContingency if oElm not in manual]

        if auto:
            self.app.GetFromStudyCase('ComSimoutage').CreateFaultCase(auto, 1, 0, oIntFolder)

        if manual:
            ZF = {'KO1': ('BJS_400-Z1', 'BJS_400-Z3'),
                  'KS1': ('VHA_400-ZF4', 'VHA_400-ZF5', 'VHA_400-ZF6'),
                  'KS2': ('VHA_400-ZF1', 'VHA_400-ZF2', 'VHA_400-ZF3'),
                  'SB1': ('FGD_400-ZF1', 'FGD_400-ZF2', 'FGD_400-ZF3', 'FGD_400-ZF4',
                          'HKS_400-ZF1', 'HKS_400-ZF2', 'HKS_400-ZF3', 'HKS_400-ZF4'),
                  'SK1': ('TJE_150-ZA1', 'TJE_150-ZA2', 'TJE_150-ZA3'),
                  'SK2': ('TJE_150-ZA1', 'TJE_150-ZA2', 'TJE_150-ZA3'),
                  'SK3': ('TJE_400-ZF1', 'TJE_400-ZF2', 'TJE_400-ZF3', 'TJE_400-ZF4')}

            self.app.SetWriteCacheEnabled(1)
            for oElm in manual:
                oIntEvt = oIntFolder.CreateObject('IntEvt', oElm.GetAttribute('e:loc_name'))
                oIntEvt.SetAttribute('e:mod_cnt', 1)

                if oElm.GetClassName() == 'ElmBranch':
                    sElm = oElm.GetContents()
                else:
                    sElm = oElm.GetContents('dc_*.ElmGenstat,*.ElmVsc,*.ElmRec')
                    if oElm.GetAttribute('e:loc_name') in ZF:
                        sElm += self.app.GetCalcRelevantObjects('{0}.ElmShnt'.format('.ElmShnt,'.join(ZF[oElm.GetAttribute('e:loc_name')])))

                for oMinor in sElm:
                    oEvtShc = oIntEvt.CreateObject('EvtShc', oMinor.GetAttribute('e:loc_name'))
                    oEvtShc.SetAttribute('e:p_target', oMinor)
            self.app.WriteChangesToDb()
            self.app.SetWriteCacheEnabled(0)

    def contFinalize(self, flag):
        if flag:
            self.contCreate()

            oComSimoutage = self.app.GetFromStudyCase('ComSimoutage')

            oComSimoutage.RemoveContingencies()

            oComSimoutage.SetAttribute('e:iEnableParal', int(not self.ContingencyForm.cb_DisableParallelComputation.isChecked()))
            oComSimoutage.SetAttribute('e:iEnableParalDC', int(not self.ContingencyForm.cb_DisableParallelComputation.isChecked()))
            oComSimoutage.SetAttribute('e:minCntcyAC', os.cpu_count() * 10)
            oComSimoutage.SetAttribute('e:minCntcyDC', os.cpu_count() * 10)

            if self.ContingencyForm.rb_ACLoadFlow.isChecked():
                aLoadFlow = 'AC Load Flow Calculation'
                oComSimoutage.SetAttribute('e:iopt_Linear', 0)
            else:
                aLoadFlow = 'DC Load Flow Calculation'
                oComSimoutage.SetAttribute('e:iopt_Linear', 1)

            if self.ContingencySIPS:
                oComSimoutage.SetAttribute('e:rasActive', 1)
                oComSimoutage.SetAttribute('e:rasOutput', 1)

                oComSimoutage.RemoveAllRas()

                for oIntRas in self.ContingencySIPS:
                    oComSimoutage.AddRas(oIntRas)
            else:
                oComSimoutage.SetAttribute('e:rasActive', 0)
                oComSimoutage.SetAttribute('e:rasOutput', 0)

            self.updateGUI('Contingency configuration successfully finished. Created N-1 cases: {0}, selected SIPS: {1}, {2}'.format(len(self.selectedContingency), len(self.ContingencySIPS), aLoadFlow), 100)
        else:
            self.updateGUI('WARN: Contingency configuration has been reset', 100)
            for element in (self.ContingencyForm.cb_Busbars, self.ContingencyForm.cb_Transformers,
                            self.ContingencyForm.cb_Shunts, self.ContingencyForm.cb_Lines,
                            self.ContingencyForm.cb_HVDC, self.ContingencyForm.cb_Generators,
                            self.ContingencyForm.cb_PV_Generators, self.ContingencyForm.cb_SIPS,
                            self.ContingencyForm.cb_DisableParallelComputation):
                element.setChecked(False)
            self.ContingencyForm.rb_ACLoadFlow.setChecked(True)
        self.enableCalculations()

    def contReport(self):
        oComCntreport = self.app.GetFromStudyCase('ComCntreport')
        self.showPf()
        if oComCntreport.ShowEditDialog() == 0:
            oComCntreport.Execute()
        else:
            self.hidePf()

    def delfinPrepare(self):
        self.mainW.tabOutput.setCurrentIndex(self.outputTab['Import'])
        self.updateGUI('Delfin configuration started', 15)
        self.DelfinForm.calendarWidget.setMaximumDate(QtCore.QDate.currentDate())
        self.DelfinForm.calendarWidget.setSelectedDate(QtCore.QDate.currentDate())
        self.getSelectedDelfin()
        self.DelfinForm.show()

    def delfinAdd(self):
        if self.DelfinForm.cb_ITC.isChecked():
            self.DelfinForm.listWidget.clear()
            YEAR = self.DelfinForm.calendarWidget.selectedDate().year()
            MONTH = self.DelfinForm.calendarWidget.selectedDate().month()
            for date in (datetime.datetime(YEAR, MONTH, day) for day in range(15, 22)):
                if date.isoweekday() == 3:
                    for DATE in (date, date - datetime.timedelta(days=3)):
                        for TIME in (datetime.time(*hour) for hour in ([3, 30], [11, 30], [19, 30])):
                            DATE_TIME = [datetime.datetime.combine(DATE, TIME)]
                            DATE_TIME.append(DATE_TIME[0].strftime('%Y-%m-%d %H:%M'))
                            if DATE_TIME[0] < (datetime.datetime.now() - datetime.timedelta(minutes=15)):
                                self.DelfinForm.listWidget.addItem(DATE_TIME[1])
                            else:
                                self.updateGUI('WARN: Timepoint "{0}" not yet available in Delfin database'.format(DATE_TIME[1]), None)
                    break
        else:
            if not self.DelfinForm.cb_MultipleTP.isChecked():
                self.DelfinForm.listWidget.clear()
            DATE_TIME = [datetime.datetime.combine(self.DelfinForm.calendarWidget.selectedDate().toPyDate(), self.DelfinForm.timeEdit.time().toPyTime())]
            DATE_TIME.append(DATE_TIME[0].strftime('%Y-%m-%d %H:%M'))
            if not self.DelfinForm.listWidget.findItems(DATE_TIME[1], QtCore.Qt.MatchExactly):
                if DATE_TIME[0] < (datetime.datetime.now() - datetime.timedelta(minutes=15)):
                    self.DelfinForm.listWidget.addItem(DATE_TIME[1])
                else:
                    self.updateGUI('WARN: Timepoint "{0}" not yet available in Delfin database'.format(DATE_TIME[1]), None)
            else:
                self.updateGUI('WARN: Timepoint "{0}" already selected'.format(DATE_TIME[1]), None)
        self.getSelectedDelfin()

    def delfinRemove(self, remove_all=False):
        if remove_all:
            self.DelfinForm.listWidget.clear()
        else:
            for item in self.DelfinForm.listWidget.selectedItems():
                self.DelfinForm.listWidget.takeItem(self.DelfinForm.listWidget.row(item))
        self.getSelectedDelfin()

    def delfinFinalize(self, flag):
        if flag:
            self.updateGUI('Delfin configuration successfully finished. Number of selected timepoints: {0}'.format(self.getSelectedDelfin()), 100)
            self.getDelfinData()
        else:
            self.updateGUI('WARN: Delfin import has been aborted', 100)

    def deleteFolder(self, sFolder):
        for oFolder in sFolder:
            self.deactivateIntScheme(oFolder.GetContents('*.IntScheme', 1))
            oFolder.Delete()

    def activateIntScheme(self, sIntScheme):
        for oIntScheme in sIntScheme:
            oIntScheme.Activate()

    def deactivateIntScheme(self, sIntScheme, delete=False):
        for oIntScheme in sIntScheme:
            oIntScheme.Deactivate()
            if delete:
                oIntScheme.Delete()

    def RMS_Report(self):
        RMSReport(self.rmsOutputFilePath)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    DSAlight()
    sys.exit(app.exec_())
