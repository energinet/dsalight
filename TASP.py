# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.0-only

'''
© Copyright 2016-2022, Energinet
Author: Jeppe Meldgaard Røge jeg@energinet.dk

Version 4.1.0 [2022-01-18]  (JEG)
---------------------------------
  - CHG: Proper handling of new graphics introduced in 2021 (GrpPage & PltLinebarplt).

Version 4.0 [2019-10-04]
------------------------
  - CHG: Export only layout files (DSAlight v4.x)


Version 3.4 [2019-08-05]
------------------------
  - ADD: Export the nominal value so the values of the variable can be normalised in DSAlight


Version 3.3 [2019-06-03]
------------------------
  - CHG: Proper handling of "Do nothing" for VoltageProfile (KDL)
  - CHG: Extended check for detection of ElmRes (NAQ)
  - CHG: Now export seperate "Layout" for each StudyCase (NAQ)
  - CHG: Improvement in code in regards of speed by reading the graphic board
  - CHG: Use local time instead of UTC


Version 3.2 [2019-03-05]
------------------------
  - ADD: "Fragmentation" option to split the tasks to 4 small sequential jobs (VLA)
  - ADD: Delete all temporary files after exporting plot files


Version 3.1 [2018-11-13]
------------------------
  - ADD: PowerFactory 2018 option to transfer only the results to master process


Version 3.0 [2018-10-12]
------------------------
  - CHG: New approach for saving the plots. From now on only the numerical data
         will be saved as CSV files - compability with DSAlight v3.0


Version 2.2 [2018-07-17]
------------------------
  - CHG: Usage of "ComWr" object for saving plots:
             PowerFactory 2017 --> WMF file format
             PowerFactory 2018 --> PNG file format


Version 2.1 [2018-07-12]
------------------------
  - ADD: Detection and processing of XY plots (JEG)
  - ADD: Check for proper content of ElmRes


Version 2.0 [2018-07-04]
------------------------
  - INF: Initial "Python" release

'''

__author__ = 'Jeppe Meldgaard Røge'
__author_email__ = 'jeg@energinet.dk'
__copyright__ = '© Copyright 2016-2022, Energinet'
__version__ = '4.1.0'

import os
import json


def TASP(app, oComTasks, directory_output):

    # Get all relevant Study Cases
    print(oComTasks.GetClassName())
    if oComTasks.GetClassName() == 'ComTasks':
        sIntCase = oComTasks.GetAttribute('e:vecCases')
    if oComTasks.GetClassName() == 'IntCase':
        sIntCase = [oComTasks]
    else:
        sIntCase = oComTasks.GetContents('*.IntCase', 1)

    # Check if has any results to export
    if not sIntCase:
        app.PrintWarn('Nothing to export...')
        return

    for oIntCase in sIntCase:

        # Create folder for saving the results
        aFolderPath = os.path.abspath(os.path.join(directory_output, oIntCase.GetAttribute('e:loc_name')))
        if not os.path.exists(aFolderPath):
            os.makedirs(aFolderPath)

        # Create dictionary with GraphicBoard layout
        layout = {}
        if int(aPF_Version[:2]) <21:
            for oSetVipage in (oSetVipage for oSetVipage in oIntCase.GetContents('*.SetVipage', 1) if oSetVipage.GetContents('*.VisPlot')):
                layout[oSetVipage.GetAttribute('e:loc_name')] = {}
                sVisPlot = oSetVipage.GetContents('*.VisPlot')
                sVisPlot.sort(key=lambda x: x.GetAttribute('e:Position:0') + 1/x.GetAttribute('e:Position:1'))
                for subplot_index, oVisPlot in enumerate(sVisPlot, start=1):
                    VisXvalue = [(oVisXvalue.GetAttribute('e:lab_text'), oVisXvalue.GetAttribute('e:cConst'), oVisXvalue.GetAttribute('e:value'), int(oVisXvalue.GetAttribute('e:color')))
                                for oVisXvalue in oVisPlot.GetContents('*.VisXvalue')]
                    layout[oSetVipage.GetAttribute('e:loc_name')]['Subplot {0}'.format(subplot_index)] = [
                            [(oElm.GetAttribute('e:loc_name'), aVariable, int(fColor), bool(bIsNom), fValNom)
                            for oElm, aVariable, fColor, bIsNom, fValNom in zip(oVisPlot.GetAttribute('e:pObjs'), oVisPlot.GetAttribute('e:Variable'),
                            oVisPlot.GetAttribute('e:Color'), oVisPlot.GetAttribute('e:dIsNom'), oVisPlot.GetAttribute('e:dValNom')) if oElm],
                            VisXvalue]
        else:
            for oGrpPage in (oGrpPage for oGrpPage in oIntCase.GetContents('*.GrpPage',1) if oGrpPage.GetContents('*.PltLinebarplot')):
                layout[oGrpPage.GetAttribute('e:loc_name')] = {}
                app.GetOutputWindow().Print(oGrpPage)
                (sPltLinebarplot,sPltDataseries)  = list(zip(*(sorted(zip(oGrpPage.GetContents('*.PltLinebarplot'),oGrpPage.GetContents('*.PltDataseries',1)),key=lambda x: x[0].GetAttribute('e:plotAreaLeft') + 1/x[0].GetAttribute('e:plotAreaBottom')))))
                for subplot_index, (oPltDataseries,oPltLinebarplot) in enumerate(zip(sPltDataseries,sPltLinebarplot), start=1):
                    VisXvalue = [(oVisXvalue.GetAttribute('e:lab_text'), oVisXvalue.GetAttribute('e:cConst'), oVisXvalue.GetAttribute('e:value'), int(oVisXvalue.GetAttribute('e:color')))
                                for oVisXvalue in oPltLinebarplot.GetContents('*.VisXvalue')]
                    layout[oGrpPage.GetAttribute('e:loc_name')]['Subplot {0}'.format(subplot_index)] = [
                            [(oElm.GetAttribute('e:loc_name'), aVariable, int(fColor), bool(bIsNom), fValNom)
                            for oElm, aVariable, fColor, bIsNom, fValNom in zip(oPltDataseries.GetAttribute('e:curveTableElement'), oPltDataseries.GetAttribute('e:curveTableVariable'),
                            oPltDataseries.GetAttribute('e:curveTableColor'), oPltDataseries.GetAttribute('e:curveTableNormalise'), oPltDataseries.GetAttribute('e:curveTableNormValue')) if oElm],
                            VisXvalue]          


        # Save JSON files with layout data
        jsonFile = os.path.join(aFolderPath, 'Layout.json')
        if not os.path.isfile(jsonFile) or not os.stat(jsonFile).st_size:
            with open(jsonFile, 'w', encoding='utf-8') as fid:
                json.dump(layout, fid, indent=4)


if __name__ == '__main__':

    import powerfactory

    # Get "PowerFactory Application class"
    app = powerfactory.GetApplication()

    # Get PowerFactory version
    aPF_Version = powerfactory.__version__  

    # Input parameters
    directory_output = app.GetCurrentScript().DIRECTORY_OUTPUT
    oComTasks = app.GetCurrentScript().oComTasks

    if oComTasks is not None:
        TASP(app, oComTasks, directory_output)
    else:
        app.PrintError('No ComTasks object has been selected - nothing to do...')
