# -*- mode: python ; coding: utf-8 -*-

import glob
import os
import sys
import shutil
import fnmatch
from PyInstaller.utils.hooks import collect_submodules
hiddenimports_win32timezone = collect_submodules('win32timezone')
hiddenimports_py2_warn = collect_submodules('pkg_resources.py2_warn')

main_folder='DSAlight_v4.6.0'

block_cipher = None
added_files =  [('DSAlight.ini', '.'),
                ('ChangeLog.txt', '.'),
                ('StateChange.xlsx', '.'),
                ('Help', 'Help'),
                ('ui', 'ui'),
                ('icons', 'icons')]
				
main_path_files = [ 'DSAlight.exe',
					'RMSReport.exe',
					'kiwisolver',
					'lib',
					'matplotlib',
					'numpy',
					'pandas',
					'PIL',
					'psutil',
					'PyQt5',
					'base_library.zip',
					'libopenblas*.dll'
					'python3.dll',
					'python310.dll']




dsalight_a = Analysis(
    ['DSAlight.py'],
	pathex=['C:\\WPy64-31050\\scripts'],
	binaries=[],
	datas=added_files,
	hiddenimports = hiddenimports_win32timezone + hiddenimports_py2_warn,
	hookspath=['.'],
	runtime_hooks=['add_lib.py'],
	excludes=['altgraph','setuptools'],
	win_no_prefer_redirects=False,
	win_private_assemblies=False,
	cipher=block_cipher
)
rmsreport_a = Analysis(
    ['RMSReport.py'],
	pathex=['C:\\WPy64-31050\\scripts'],
	binaries=[],
	datas=added_files,
	hiddenimports = hiddenimports_win32timezone + hiddenimports_py2_warn,
	hookspath=['.'],
	runtime_hooks=['add_lib.py'],
	excludes=['altgraph','setuptools'],
	win_no_prefer_redirects=False,
	win_private_assemblies=False,
	cipher=block_cipher
)

MERGE((dsalight_a, 'DSAlight', 'DSAlight'), (rmsreport_a, 'RMSReport', 'RMSReport'))

dsalight_pyz = PYZ(dsalight_a.pure, dsalight_a.zipped_data, cipher=block_cipher)
rmsreport_pyz = PYZ(rmsreport_a.pure, rmsreport_a.zipped_data, cipher=block_cipher)

dsalight_exe = EXE(
    dsalight_pyz,
	dsalight_a.scripts,
    exclude_binaries=True,
    name='DSAlight',
    debug=False,
    strip=False,
    upx=True,
    console=False
)

rmsreport_exe = EXE(
    rmsreport_pyz,
    rmsreport_a.scripts,
	exclude_binaries=True,
    name='RMSReport',
    debug=False,
    strip=False,
    upx=True,
    console=False
)

coll = COLLECT(dsalight_exe,
               dsalight_a.binaries,
               dsalight_a.zipfiles,
               dsalight_a.datas,
			   rmsreport_exe,
               rmsreport_a.binaries,
               rmsreport_a.zipfiles,
               rmsreport_a.datas,
			   strip=False,
               upx=True,
               name=main_folder
)



main_path = os.path.join(DISTPATH,main_folder)
lib_path = os.path.join(main_path,'lib')

#Creating lib_path:
try:
    os.makedirs(lib_path,exist_ok=True)
    print('Created: ',lib_path)
except OSError as error:
    print(error) 


#Making a list of files in the main_path and a list of files which must stay in the main_path:
files = [os.path.basename(item) for item in glob.glob(main_path+'\\*', recursive=False)]
essential_files = [*[item[0] for item in added_files],*main_path_files]

#Removing the files which must stay in the main_path from the list of files:
for file in files[:]:
	for file_to_keep in essential_files:
		if(file.startswith(file_to_keep.split('*')[0])):
			if file.split('.')[-1]==file_to_keep.split('.')[-1]:
				files.remove(file)
  
#Moving files to the lib_path:
for file in files:
    print('Moving:', file)
    shutil.move(os.path.join(main_path,file), os.path.join(lib_path,file))