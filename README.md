# DSAlight
DSAlight is graphical user interface developed by [Energinet](https://energinet.dk) to interact with [DIgSILENT PowerFactory](https://www.digsilent.de/en/powerfactory.html) in order to perform a dynamic stability assessment of the transmission system in accordance with [Article 38 - Dynamic stability monitoring and assessment](http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2017.220.01.0001.01.ENG&toc=OJ:L:2017:220:TOC#d1e3962-1-1) of the [System Operation Guideline](https://electricity.network-codes.eu/network_codes/sys-ops/). 

DSAlight uses free and open source language [Python](https://www.python.org) (v3.10) and [Qt](https://www.qt.io) (v5.15). The application utilize the existing [Energinet's](https://energinet.dk) dynamic environment and provides two user friendly  modules: 
- Module 1: Front-End for PowerFactory (specific to [Energinet's](https://energinet.dk) RMS environment, but can be used as template for adapting to different RMS set-up's):
    - Data import
    - Case configuration
    - Execution of simulations (LoadFlow, Contingency, RMS,…)
- Module 2: Result assessment & evaluation (independent from [PowerFactory](https://www.digsilent.de/en/powerfactory.html) application)

## Contributing
We kindly encourage to contact us per email or submitting pull requests with regards to the further development and new features proposal.

## Usage
DSAlight is started by executing the main program "DSAlight.py". However, the preferred way to run the DSAlight is to distribute the compiled stand-alone version to end-users.

Please read "help\DSAlight User Guide.pdf" for more details.

### Compilation to stand-alone executable file
DSAlight can be compiled to stand-alone executable file under Windows using e.g. [PyInstaller](https://www.pyinstaller.org). Use the package manager [pip](https://pip.pypa.io/en/stable/) to install PyInstaller:
```python
pip install pyinstaller
```
Then, use the dedicated spec file "DSAlight.spec" as input for PyInstaller:
```sh
pyinstaller DSAlight.spec
```
or run the compilation directly from Python as described in:  https://pyinstaller.readthedocs.io/en/stable/usage.html#running-pyinstaller-from-python-code
```python
PyInstaller.__main__.run(['DSAlight.spec'])
```

### Sample RMS result files
You can find syntetic output files set up to demonstrate the capabilities of DSAlight Module 2 under "RMS_demo_files"