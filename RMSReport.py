# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.0-only

import os
import re
import sys
import glob
import time
import json
import xlsxwriter
import subprocess
import win32com.client
import numpy as np
import pandas as pd
import chardet

from PyQt5 import QtWidgets, QtCore, QtGui, uic

import matplotlib as mpl
mpl.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
import matplotlib.pyplot as plt


class RMSReport(QtWidgets.QMainWindow):

    __author__ = 'Jeppe Meldgaard Røge'
    __author_email__ = 'dsalight@energinet.dk'
    __copyright__ = '© Copyright 2016-2024, Energinet'
    __version__ = '4.6.0'
    __name__ = 'DSAlight (RMS Report) v{0} © Energinet 2024'.format(__version__)

    def __init__(self, rms_output_path):
        super().__init__()

        self.CONFIGURATION_FILE = os.path.join(os.getcwd(), 'DSAlight.ini')

        self.root = rms_output_path
        self.mainW = uic.loadUi(r'ui\RMSreport.ui')
        self.mainW.setWindowTitle(self.__name__)

        self.AboutTrafficLight = uic.loadUi(r'ui\AboutTrafficLight.ui')
        self.AboutTrafficLight.setWindowTitle(self.__name__)

        self.mainW.cbDate.currentIndexChanged.connect(self.updateCbDate)
        self.mainW.cbFault.currentIndexChanged.connect(self.updateCbFault)
        self.mainW.cbPlot.currentIndexChanged.connect(self.updateCbPlot)
        self.mainW.cbDateMaster.currentIndexChanged.connect(self.updateCbDateMaster)
        self.mainW.cbFaultMaster.currentIndexChanged.connect(self.updateCbFaultMaster)
        self.mainW.cbPlotMaster.currentIndexChanged.connect(self.updateCbPlotMaster)
        self.mainW.cbDateSlave.currentIndexChanged.connect(self.updateCbDateSlave)
        self.mainW.cbFaultSlave.currentIndexChanged.connect(self.updateCbFaultSlave)
        self.mainW.cbLineStyle.currentIndexChanged.connect(self.updateCbFaultSlave)
        self.mainW.cbTimeStamp.stateChanged.connect(self.showTimestamp)
        self.mainW.cbLineWrap.stateChanged.connect(self.lineWrap)
        self.mainW.cbResultsTransposition.stateChanged.connect(self.fillInOverview)
        self.mainW.cbSynchronizeDate.stateChanged.connect(self.updateCbDateSlave)
        self.mainW.cbSynchronizeFault.stateChanged.connect(self.updateCbDateSlave)
        self.mainW.cbRemoveLegend.stateChanged.connect(lambda: self.showLegend(self.fig))
        self.mainW.cbRemoveLegendCompare.stateChanged.connect(lambda: self.showLegend(self.figCompare))
        self.mainW.cbFreezeYDetails.stateChanged.connect(lambda: self.freezeYAxis(self.fig))
        self.mainW.cbFreezeYCompare.stateChanged.connect(lambda: self.freezeYAxis(self.figCompare))
        self.mainW.cbLimitX.stateChanged.connect(lambda: self.limitXAxis(self.fig, self.mainW.cbLimitX, self.mainW.dSPXmin, self.mainW.dSPXmax))
        self.mainW.cbLimitXCompare.stateChanged.connect(lambda: self.limitXAxis(self.figCompare, self.mainW.cbLimitXCompare, self.mainW.dSPXminCompare, self.mainW.dSPXmaxCompare))
        self.mainW.dSPXmin.valueChanged.connect(lambda: self.limitXAxis(self.fig, self.mainW.cbLimitX, self.mainW.dSPXmin, self.mainW.dSPXmax))
        self.mainW.dSPXmax.valueChanged.connect(lambda: self.limitXAxis(self.fig, self.mainW.cbLimitX, self.mainW.dSPXmin, self.mainW.dSPXmax))
        self.mainW.dSPXminCompare.valueChanged.connect(lambda: self.limitXAxis(self.figCompare, self.mainW.cbLimitXCompare, self.mainW.dSPXminCompare, self.mainW.dSPXmaxCompare))
        self.mainW.dSPXmaxCompare.valueChanged.connect(lambda: self.limitXAxis(self.figCompare, self.mainW.cbLimitXCompare, self.mainW.dSPXminCompare, self.mainW.dSPXmaxCompare))
        self.mainW.btnLogToClip.clicked.connect(self.copyToClipboard)
        self.mainW.btnOpenLog.clicked.connect(lambda: os.startfile(self.LogFile))
        self.mainW.btnUpdate_Overview.clicked.connect(self.screenLogFiles)
        self.mainW.btnUpdate_Detailed.clicked.connect(self.screenLogFiles)
        self.mainW.btnFolder_Overview.clicked.connect(self.selectFolder)
        self.mainW.btnFolder_Detailed.clicked.connect(self.selectFolder)
        self.mainW.btnFont.clicked.connect(self.setFont)
        self.mainW.btnNextDate.clicked.connect(lambda: self.increaseIndex(self.mainW.cbDate, 1))
        self.mainW.btnPrevDate.clicked.connect(lambda: self.increaseIndex(self.mainW.cbDate, -1))
        self.mainW.btnNextFault.clicked.connect(lambda: self.increaseIndex(self.mainW.cbFault, 1))
        self.mainW.btnPrevFault.clicked.connect(lambda: self.increaseIndex(self.mainW.cbFault, -1))
        self.mainW.btnNextPlot.clicked.connect(lambda: self.increaseIndex(self.mainW.cbPlot, 1))
        self.mainW.btnPrevPlot.clicked.connect(lambda: self.increaseIndex(self.mainW.cbPlot, -1))
        self.mainW.btnNextDateMaster.clicked.connect(lambda: self.increaseIndex(self.mainW.cbDateMaster, 1))
        self.mainW.btnPrevDateMaster.clicked.connect(lambda: self.increaseIndex(self.mainW.cbDateMaster, -1))
        self.mainW.btnNextFaultMaster.clicked.connect(lambda: self.increaseIndex(self.mainW.cbFaultMaster, 1))
        self.mainW.btnPrevFaultMaster.clicked.connect(lambda: self.increaseIndex(self.mainW.cbFaultMaster, -1))
        self.mainW.btnNextPlotMaster.clicked.connect(lambda: self.increaseIndex(self.mainW.cbPlotMaster, 1))
        self.mainW.btnPrevPlotMaster.clicked.connect(lambda: self.increaseIndex(self.mainW.cbPlotMaster, -1))
        self.mainW.btnNextDateSlave.clicked.connect(lambda: self.increaseIndex(self.mainW.cbDateSlave, 1))
        self.mainW.btnPrevDateSlave.clicked.connect(lambda: self.increaseIndex(self.mainW.cbDateSlave, -1))
        self.mainW.btnNextFaultSlave.clicked.connect(lambda: self.increaseIndex(self.mainW.cbFaultSlave, 1))
        self.mainW.btnPrevFaultSlave.clicked.connect(lambda: self.increaseIndex(self.mainW.cbFaultSlave, -1))
        for element in (self.mainW.btnSaveScreenshot_Overview,
                        self.mainW.btnSaveScreenshot_Detailed,
                        self.mainW.btnSaveScreenshot_Compare):
            element.clicked.connect(self.saveScreenshot)
        self.mainW.btnRedLight.clicked.connect(lambda: self.searchInLogFile('redLightButton'))
        self.mainW.btnOrangeLight.clicked.connect(lambda: self.searchInLogFile('orangeLightButton'))
        self.mainW.btnGreenLight.clicked.connect(lambda: self.searchInLogFile(None))
        self.mainW.btnSimulationDuration.clicked.connect(lambda: self.searchInLogFile(['.*Duration \[[H]{1,2}:MM:SS\]:']))
        self.mainW.btnSimulationStopTime.clicked.connect(lambda: self.searchInLogFile(['Event: Stop Simulation']))
        self.mainW.btnExpectedSimulationStop.clicked.connect(lambda: self.searchInLogFile(['Expected stop:  Run Simulation has been:']))
        self.mainW.btnTripOfCHP.clicked.connect(lambda: self.searchInLogFile(['Decentral produktion']))
        self.mainW.btnTripOfWindOld.clicked.connect(lambda: self.searchInLogFile(['Landvind Gammel']))
        self.mainW.btnACExchange.clicked.connect(lambda: self.searchInLogFile(['Udveksling til']))
        self.mainW.btnCommutationFailure.clicked.connect(lambda: self.searchInLogFile(['Commutation failure']))
        self.mainW.btnBlockingMode.clicked.connect(lambda: self.searchInLogFile(['Changed to blocking mode']))
        self.mainW.btnNormalOperation.clicked.connect(lambda: self.searchInLogFile(['Back to normal operation']))
        self.mainW.btnSimulationScan.clicked.connect(lambda: self.searchInLogFile(['Simulation Scan']))
        self.mainW.btnMaxVoltage.clicked.connect(lambda: self.searchInLogFile(['maximum voltage limit violated', 'FRT limit.*>=']))
        self.mainW.btnMinVoltage.clicked.connect(lambda: self.searchInLogFile(['minimum voltage limit violated', 'FRT limit.*<=']))
        self.mainW.btnPoleSlip.clicked.connect(lambda: self.searchInLogFile(['Generator out of step (pole slip)']))
        self.mainW.btnShuntOver.clicked.connect(lambda: self.searchInLogFile(['ShuntRelay.*U>']))
        self.mainW.btnShuntUnder.clicked.connect(lambda: self.searchInLogFile(['ShuntRelay.*U<']))
        self.mainW.btnInnerLoops.clicked.connect(lambda: self.searchInLogFile(['Maximum number of inner-loop iterations']))
        self.mainW.btnOuterLoops.clicked.connect(lambda: self.searchInLogFile(['Maximum number of outer-loop iterations']))
        self.mainW.btnEventLoops.clicked.connect(lambda: self.searchInLogFile(['Maximum number of event loops']))
        self.mainW.btnNumericalIssues.clicked.connect(lambda: self.searchInLogFile(['System-Matrix Inversion failed',
                                                                                    'quation system could not be solved', 'Math. error in DSL']))
        self.mainW.btnNotCorrectlyInitialised.clicked.connect(lambda: self.searchInLogFile('NotCorrectlyInitialised'))
        self.mainW.btnAVC.clicked.connect(lambda: self.searchInLogFile(['AVC triggered:']))
        self.mainW.btnRPC.clicked.connect(lambda: self.searchInLogFile(['RPC triggered:']))
        self.mainW.btnMIO.clicked.connect(lambda: self.searchInLogFile(['MIO triggered:']))
        self.mainW.btnSIPS.clicked.connect(lambda: self.searchInLogFile(['SIPS triggered:']))
        self.mainW.btnPowerLimitExceeded.clicked.connect(lambda: self.searchInLogFile(['Power Limit exceeded', 'power limit exceeded']))
        self.mainW.btnShowPlot.clicked.connect(lambda: subprocess.Popen(r'explorer /select, "{0}"'.format(self.ExportFolder)))
        self.mainW.btnExportPlots.clicked.connect(self.exportPlots)
        self.mainW.btnShowLog.clicked.connect(lambda: subprocess.Popen(r'explorer /select, "{0}"'.format(self.LogFile)))
        self.mainW.btnTrafficLightsOverview.clicked.connect(lambda: self.AboutTrafficLight.show())
        self.mainW.btnTrafficLightsDetailed.clicked.connect(lambda: self.AboutTrafficLight.show())
        self.mainW.btnSupportRequest.clicked.connect(self.supportRequest)
        self.mainW.btnSaveOverview.clicked.connect(self.saveOverview)
        self.mainW.btnComputationStats.clicked.connect(self.computationStats)
        self.mainW.btnUserDefined.clicked.connect(self.searchUserDefined)
        self.mainW.btnOpenMasterLogFile.clicked.connect(lambda: os.startfile(self.MasterLogFile))
        self.mainW.tableWidget.cellClicked.connect(self.detailedView)

        for element in (self.mainW.btnEnerginet1, self.mainW.btnEnerginet2, self.mainW.btnEnerginet3):
            element.clicked.connect(lambda: os.startfile('https://energinet.dk'))

        self.overviewButtons = (self.mainW.rb_TrafficLight,
                                self.mainW.rb_SimulationFinishTime,
                                self.mainW.rb_SimulationScan,
                                self.mainW.rb_SimulationDuration,
                                self.mainW.rb_PoleSlip,
                                self.mainW.rb_MaxVoltage, self.mainW.rb_MinVoltage,
                                self.mainW.rb_CommutationFailure,
                                self.mainW.rb_ShuntOver, self.mainW.rb_ShuntUnder,
                                self.mainW.rb_PowerLimitExceeded,
                                self.mainW.rb_AVC_RPC_MIO,
                                self.mainW.rb_SIPS,
                                self.mainW.rb_NotCorrectlyInitialised,
                                self.mainW.rb_FaultImpedance,
                                self.mainW.rb_IntegrationStep,
                                self.mainW.rb_NumericalIssues,
                                self.mainW.rb_BoundaryMW, self.mainW.rb_BoundaryMVAr,
                                self.mainW.rb_UserDefined)

        for element in (*self.overviewButtons, self.mainW.cBHideStableCases, self.mainW.cbHighlightNumericalIssues):
            element.clicked.connect(self.evaluateLogFiles)
        self.mainW.cbBoundary.currentIndexChanged.connect(self.evaluateLogFiles)

        SP_DialogOpenButton = self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_DialogOpenButton'))
        for element in (self.mainW.btnFolder_Overview, self.mainW.btnFolder_Detailed):
            element.setIcon(SP_DialogOpenButton)

        SP_BrowserReload = self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_BrowserReload'))
        for element in (self.mainW.btnUpdate_Overview, self.mainW.btnUpdate_Detailed):
            element.setIcon(SP_BrowserReload)

        SP_FileDialogInfoView = self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_FileDialogInfoView'))
        for element in (self.mainW.btnTrafficLightsOverview, self.mainW.btnTrafficLightsDetailed):
            element.setIcon(SP_FileDialogInfoView)

        SP_MediaSkipBackward = self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_MediaSkipBackward'))
        for element in (self.mainW.btnPrevDate, self.mainW.btnPrevFault, self.mainW.btnPrevPlot,
                        self.mainW.btnPrevDateMaster, self.mainW.btnPrevFaultMaster,
                        self.mainW.btnPrevDateSlave, self.mainW.btnPrevFaultSlave, self.mainW.btnPrevPlotMaster):
            element.setIcon(SP_MediaSkipBackward)

        SP_MediaSkipForward = self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_MediaSkipForward'))
        for element in (self.mainW.btnNextDate, self.mainW.btnNextFault, self.mainW.btnNextPlot,
                        self.mainW.btnNextDateMaster, self.mainW.btnNextFaultMaster,
                        self.mainW.btnNextDateSlave, self.mainW.btnNextFaultSlave, self.mainW.btnNextPlotMaster):
            element.setIcon(SP_MediaSkipForward)

        self.mainW.btnOpenMasterLogFile.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_FileDialogDetailedView')))
        self.mainW.btnSaveOverview.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_DialogSaveButton')))
        self.mainW.btnUserDefined.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_FileDialogContentsView')))
        self.mainW.btnComputationStats.setIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_FileDialogListView')))

        try:
            self.OUTLOOK = win32com.client.Dispatch('Outlook.application')
        except Exception:
            self.mainW.btnSupportRequest.setEnabled(False)
            self.mainW.btnSupportRequest.setToolTip('Microsoft Outlook has not been detected - support request feature is not available...')

        self.OUTPUT_FOLDER = None
        for output_folder in (os.path.join('D:', os.sep, os.environ['USERNAME'].split('-')[0].upper()), r'C:\Temp'):
            if os.path.isdir(output_folder):
                self.OUTPUT_FOLDER = output_folder
                break

        if not self.OUTPUT_FOLDER:
            for item in (self.mainW.btnSaveScreenshot_Overview, self.mainW.btnSaveScreenshot_Detailed):
                item.setEnabled(False)
                item.setToolTip(r'Temporary directory does not exist...')

        self.fig = plt.figure(tight_layout=True)
        self.canvas = FigureCanvasQTAgg(self.fig)
        self.mainW.matplot.addWidget(self.canvas)
        self.mainW.matplot.addWidget(NavigationToolbar2QT(self.canvas, self.mainW))

        self.figCompare = plt.figure(tight_layout=True)
        self.canvasCompare = FigureCanvasQTAgg(self.figCompare)
        self.mainW.matplotCompare.addWidget(self.canvasCompare)
        self.mainW.matplotCompare.addWidget(NavigationToolbar2QT(self.canvasCompare, self.mainW))

        self.drawEmptyMatplot(self.fig)
        self.drawEmptyMatplot(self.figCompare)

        self.MCOLORS = {idx: color for idx, color in enumerate(
                        ('white', 'black', 'red', 'green', 'blue',
                         'sienna', 'cyan', 'magenta', 'gray', 'silver'))}

        self.GraphicsDisplayResolutions = {'4K UHD': [3840, 2160],
                                           'FHD': [1920, 1080],
                                           'HD': [1280, 720],
                                           'XGA': [1024, 768],
                                           'SVGA': [800, 600],
                                           'VGA': [640, 480]}

        self.YLIM = {}
        self.BufferSizeLimit = 10
        self.LogFileSizeLimit = 25

        self.font = QtGui.QFont()
        self.font.setBold(True)

        self.progress = QtWidgets.QProgressDialog('Searching for LogFiles...', 'Cancel', 0, 100, self.mainW)
        self.progress.setWindowTitle(self.__name__)
        self.progress.setModal(True)
        self.progress.setMinimumWidth(500)
        self.progress.setMinimumHeight(110)

        for element in (self.mainW.widget_matplot, self.mainW.btnShowPlot, self.mainW.btnExportPlots, self.mainW.cb_GraphicsDisplayResolution, self.mainW.cbPlotExtension,
                        self.mainW.cbAllTimes, self.mainW.cbAllLocations, self.mainW.cbAllPlots,
                        self.mainW.cbLimitX, self.mainW.dSPXmin, self.mainW.dSPXmax, self.mainW.btnOpenMasterLogFile):
            element.setEnabled(False)

        self.mainW.show()
        self.readConfigurationFile()
        self.screenLogFiles()

    def readConfigurationFile(self):
        if os.path.isfile(self.CONFIGURATION_FILE):
            with open(self.CONFIGURATION_FILE, encoding='utf-8') as fid:
                lines = fid.read().split('\n')

            for line in (element for element in lines
                         if element and '[' not in element):
                lineSet = line.split(';', 1)[0].split('=', 1)
                parameter = lineSet[0].strip()
                if len(lineSet) == 2 and lineSet[1] != '':
                    value = lineSet[1].strip()
                else:
                    value = None
                setattr(self, parameter, value)

    def saveScreenshot(self):
        SCREENSHOT = os.path.abspath(os.path.join(self.OUTPUT_FOLDER, '{0} - DSAlight RMS Report Screenshot.png'.format(time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime()))))
        GEOMETRY = self.mainW.frameGeometry()
        QtWidgets.QApplication.primaryScreen().grabWindow(0, GEOMETRY.x(), GEOMETRY.y(), GEOMETRY.width(), GEOMETRY.height()).save(SCREENSHOT)
        return SCREENSHOT

    def lineWrap(self):
        self.mainW.logViewer.setLineWrapMode(self.mainW.cbLineWrap.isChecked())

    def setFont(self):
        font, valid = QtWidgets.QFontDialog.getFont(self.mainW.logViewer.font())
        if valid:
            self.mainW.logViewer.setFont(font)

    def supportRequest(self):
        mail = self.OUTLOOK.CreateItem(0)
        mail.To = self.emailTo
        mail.Cc = self.emailCc
        mail.Subject = '{0} support'.format(self.mainW.windowTitle().split(' ©')[0])
        if self.mainW.btnSaveScreenshot_Overview.isEnabled():
            mail.Attachments.Add(self.saveScreenshot())
        if self.PlotFile is not None:
            mail.Attachments.Add(self.PlotFile)
        if self.LogFile is not None:
            mail.Attachments.Add(self.LogFile)
        mail.Display()
        mail.HTMLBody = '<body>Dear DynamikGruppen,<br><br>WRITE YOURS REQUEST HERE<br><br>Path to folder with results: <a href="file:///{0}">{0}</a></body>{1}'.format(self.root, mail.HTMLBody)

    def copyToClipboard(self):
        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.clear()
        clipboard.setText(self.LogFile, mode=clipboard.Clipboard)

    def selectFolder(self):
        folder = QtWidgets.QFileDialog.getExistingDirectory(self.mainW, 'Choose Directory', self.root)
        if os.path.isdir(folder):
            self.root = os.path.abspath(folder)
            self.screenLogFiles()

    def screenLogFiles(self):
        for element in (self.mainW.groupBox_1, self.mainW.groupBox_2,
                        self.mainW.tableWidget, self.mainW.Details,
                        self.mainW.Compare, self.mainW.btnSaveOverview, self.mainW.cbSaveActualView):
            element.setEnabled(False)
        self.mainW.tabWidget.setCurrentIndex(0)
        self.mainW.WorkingDirectory.setText(self.root)
        
        if not self.root:
            self.mainW.btnOpenMasterLogFile.setEnabled(False)
            self.progress.setValue(100)
            return
        
        elif not os.path.isdir(self.root):
            QtWidgets.QMessageBox.critical(self.mainW, self.mainW.windowTitle(), 'Selected folder "{0}" does not exist...'.format(self.root))
            self.mainW.btnOpenMasterLogFile.setEnabled(False)
            self.progress.setValue(100)
            return

        MasterLogFiles = glob.glob(os.path.join(self.root, '*TARMS.out'))
        if MasterLogFiles:
            self.MasterLogFile = MasterLogFiles[0]
            self.mainW.btnOpenMasterLogFile.setEnabled(True)
        else:
            self.mainW.btnOpenMasterLogFile.setEnabled(False)

        self.MATRIX_FILES = {}
        with os.scandir(self.root) as it1:
            for idx, EB_SBE_NET in enumerate(folder for folder in it1 if folder.is_dir()):

                self.progress.setLabelText('Searching directory:\n{0}'.format(EB_SBE_NET.path))
                self.progress.setValue(idx)
                if self.progress.wasCanceled():
                    return

                self.MATRIX_FILES[EB_SBE_NET.name] = {'Layout': {}, 'Faults': {}}
                with os.scandir(EB_SBE_NET) as it2:
                    for FILENAME in (file for file in it2 if file.is_file() and file.stat()[-4]):
                        FILESIZE = FILENAME.stat()[-4] / (1024 * 1024)
                        EXTENSION = FILENAME.name[FILENAME.name.rfind('.'):].lower()
                        try:
                            if EXTENSION == '.json':
                                with open(FILENAME.path, encoding='utf-8') as fid:
                                    self.MATRIX_FILES[EB_SBE_NET.name]['Layout'] = json.load(fid)
                            elif EXTENSION in ('.out', '.csv'):
                                eb_sbe_net, FEJL = FILENAME.name[:FILENAME.name.rfind('.')].split('-')
                                if EB_SBE_NET.name == eb_sbe_net:
                                    if FEJL not in self.MATRIX_FILES[EB_SBE_NET.name]['Faults']:
                                        if EXTENSION == '.out':
                                            self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL] = {'LogFile': {'Path': FILENAME.path, 'FileSize': FILESIZE, 'Content': None}, 'PlotFile': {'Path': None, 'FileSize': None, 'Content': {}}}
                                        else:
                                            self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL] = {'LogFile': {'Path': None, 'FileSize': None, 'Content': None}, 'PlotFile': {'Path': FILENAME.path, 'FileSize': FILESIZE, 'Content': {}}}
                                    else:
                                        if EXTENSION == '.out':
                                            self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL]['LogFile']['Path'] = FILENAME.path
                                            self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL]['LogFile']['FileSize'] = FILESIZE
                                        else:
                                            self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL]['PlotFile']['Path'] = FILENAME.path
                                            self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL]['PlotFile']['FileSize'] = FILESIZE
                        except Exception:
                            pass

                for FEJL in (key for key, value in self.MATRIX_FILES[EB_SBE_NET.name]['Faults'].items() if value['LogFile']['Path'] is None):
                    FILENAME = ''.join([self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL]['PlotFile']['Path'].rsplit('.', 1)[0], '.out'])
                    with open(FILENAME, 'w') as fid:
                        fid.write('This is dummy LogFile created by DSAlight_v{0} at {1}\n\n'
                                  'This will enable showing the plots inside DSAlight, but all results are to be considered as invalid !!!'
                                  .format(self.__version__, time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())))
                    self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL]['LogFile']['Path'] = FILENAME
                    self.MATRIX_FILES[EB_SBE_NET.name]['Faults'][FEJL]['LogFile']['FileSize'] = os.stat(FILENAME).st_size / (1024 * 1024)

                if not self.MATRIX_FILES[EB_SBE_NET.name]['Faults']:
                    del self.MATRIX_FILES[EB_SBE_NET.name]

        self.progress.setValue(100)
        LogFiles = [FEJL['LogFile']['Path'] for EB_SBE_NET in self.MATRIX_FILES for FEJL in self.MATRIX_FILES[EB_SBE_NET]['Faults'].values()]

        self.LogFileNumber = len(LogFiles)
        self.mainW.LogFile_num.setText(str(self.LogFileNumber))

        if not self.LogFileNumber:
            QtWidgets.QMessageBox.critical(self.mainW, self.mainW.windowTitle(), 'Selected folder "{0}" does not contains any log files...'.format(self.root))
            return

        self.progress.setMaximum(self.LogFileNumber)
        self.MATRIX_RESULTS = {}
        fProcessedLogFilesSize = 0

        for idx, LogFile in enumerate(LogFiles, start=1):
            EB_SBE_NET, FEJL = os.path.split(LogFile)[1][:-4].split('-')

            if EB_SBE_NET not in self.MATRIX_RESULTS:
                self.MATRIX_RESULTS[EB_SBE_NET] = {}
            if FEJL not in self.MATRIX_RESULTS[EB_SBE_NET]:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL] = {}
            if 'Boundary' not in self.MATRIX_RESULTS[EB_SBE_NET][FEJL]:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['Boundary'] = {}

            self.progress.setLabelText('Scanning LogFile: {0} \\ {1} ({2:.2f} [MB] \\ {3:.2f} [MB])\n{4}'.format(idx, self.LogFileNumber,
                                       self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['FileSize'], fProcessedLogFilesSize, LogFile))
            self.progress.setValue(idx)
            if self.progress.wasCanceled():
                return

            if self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['FileSize'] <= self.LogFileSizeLimit:
                fProcessedLogFilesSize += self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['FileSize']

                if idx==1:
                    with open(LogFile, 'rb') as fid:
                        self.LogFileEncoding = chardet.detect(fid.read())['encoding']

                with open(LogFile, buffering=1 , encoding=self.LogFileEncoding) as fid:
                    raw_file = fid.read()

                if raw_file[0] == '[':
                    raw_file_slim = '\n'.join(line[line.find('DIgSI/')+6:] for line in raw_file.split('\n'))
                else:
                    raw_file_slim = raw_file.replace('DIgSI/', '')
                raw_file_slim_split = raw_file_slim.split('\n')
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Number_of_lines'] = len(raw_file_slim_split)
            else:
                raw_file = raw_file_slim = raw_file_slim_split = 'LogFile is too big to be opened by DSAlight'
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Number_of_lines'] = 'N/A'

            if sys.getsizeof(raw_file) / (1024 * 1024) <= self.BufferSizeLimit:
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content'] = raw_file
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content_slim'] = raw_file_slim
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content_slim_split'] = raw_file_slim_split

            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SuccessfullyExecuted'] = 'successfully executed' in raw_file
            try:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationStopTime'] = float(raw_file_slim[:raw_file_slim.find('Event: Stop Simulation')].split('Time:')[-1].split('[s]')[0].replace(',', '.'))
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExpectedSimulationStop'] = float(raw_file_slim[raw_file_slim.find('Expected stop:  Run Simulation has been:')+40:].split('[s]')[0].replace(',', '.'))
            except Exception:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationFinishTime'] = 0
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationStopTime'] = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExpectedSimulationStop'] = 'N/A'
            else:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationFinishTime'] = (self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExpectedSimulationStop'] - self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationStopTime']) / self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExpectedSimulationStop'] * 100

            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PoleSlip'] = raw_file_slim.count('Generator out of step (pole slip)')
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PowerLimitExceeded'] = sum(raw_file_slim.count(element) for element in ('Power Limit exceeded', 'power limit exceeded'))
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['AVC'] = raw_file_slim.count('AVC triggered:')
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['RPC'] = raw_file_slim.count('RPC triggered:')
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MIO'] = raw_file_slim.count('MIO triggered:')
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SIPS'] = raw_file_slim.count('SIPS triggered:')
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NotCorrectlyInitialised'] = sum(raw_file_slim.count(element) for element in ('is not correctly initialised',
                                                                                                                                'is initialised to a non-negligible value',
                                                                                                                                'limited state initialisation expression evaluates to a value outside of its limits',
                                                                                                                                'Governor lower limit exceeded',
                                                                                                                                'Governor upper limit exceeded'))

            try:
                FaultResistance = float(raw_file_slim[raw_file_slim.find('with Fault Impedance Rf =')+25:].split('Ohm')[0].strip().replace(',', '.'))
                FaultReactance = float(raw_file_slim[raw_file_slim.find('Ohm Xf =')+8:].split('Ohm')[0].strip().replace(',', '.'))
            except Exception:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['FaultImpedance'] = None
            else:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['FaultImpedance'] = complex(FaultResistance, FaultReactance)

            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['UserDefined'] = raw_file.count(self.mainW.leUserDefined.text())

            Unit_P = 99999
            Boundary_DEC = 0
            Boundary_EXC = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['CommutationFailure'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['BlockingMode'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NormalOperation'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['InnerLoops'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['OuterLoops'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['EventLoops'] = 0
            if 'Simulation Scan active: 0' in raw_file_slim:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'] = -1
            else:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntOver'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntUnder'] = 0
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExecutionDate'] = 'N/A'
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationDuration'] = 'N/A'
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationEvents'] = []
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['StartShortCircuit'] = None
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ClearShortCircuit'] = None
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MaxVoltage'] = raw_file_slim.count('maximum voltage limit violated')
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MinVoltage'] = raw_file_slim.count('minimum voltage limit violated')
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['IntegrationStep'] = 'N/A'

            iTrigger_Shunt_High = 0
            iTrigger_Shunt_Low = 0

            for idx, line in enumerate(raw_file_slim_split):

                if 'Simulation started:' in line:
                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExecutionDate'] = line[line.find('Simulation started:')+19:].lstrip()
                elif 'Integration step size:' in line:
                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['IntegrationStep'] = line[line.find('Integration step size:')+24:].replace(',', '.')
                elif re.search(r'Duration \[H{1,2}:MM:SS\]:',line):
                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationDuration'] = re.search(r'Duration \[H{1,2}:MM:SS\]:\s*(.*)', line).group(1)
                    if self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationDuration'] != 'N/A':
                        hours, minutes, seconds = map(int, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationDuration'].split(':'))
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationDuration_hist'] = hours * 60 + minutes + seconds / 60
                elif self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'] > -1 and 'Simulation Scan' in line and \
                    'Removed from sets used for' not in line and 'Simulation Scan active:' not in line and \
                        'fault detected in network' not in raw_file_slim_split[idx+1]:
                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'] += 1
                elif 'ShuntRelay' in line:
                    if 'U>' in line:
                        iTrigger_Shunt_High = idx
                    else:
                        iTrigger_Shunt_Low = idx
                elif 'message: Protection:' in line:
                    if idx - iTrigger_Shunt_High == 1:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntOver'] += 1
                    elif idx - iTrigger_Shunt_Low == 1:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntUnder'] += 1
                elif 'FRT limit violated' in line:
                    if '>=' in line:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MaxVoltage'] += 1
                    else:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MinVoltage'] += 1
                elif 'Reserve threshold has been set by:' in line:
                    Unit_Name = line[line.find('set by:')+7:line.find('with P =')].strip()
                    Unit_P = float(line[line.find('with P =')+8:line.find('[MW]')].strip().replace(',', '.'))
                elif '                Boundary: ' in line:
                    Boundary_Name = line[line.find('Boundary:')+9:line.find('P [MW] =')].strip()
                    Boundary_P = line[line.find('P [MW] =')+8:line.find(';')].replace(',', '.').split('\\')
                    Boundary_Q = line[line.find('Q [MVAr] =')+10:].replace(',', '.').split('\\')
                    if 'Decentral produktion' in Boundary_Name or 'Landvind Gammel' in Boundary_Name:
                        Boundary_DEC += abs(float(Boundary_P[1]) - float(Boundary_P[0]))
                    elif 'Udveksling til' in Boundary_Name:
                        Boundary_EXC += abs(float(Boundary_P[1]) - float(Boundary_P[0]))

                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['Boundary'][Boundary_Name] = {
                                            'Pstart': float(Boundary_P[0]),
                                            'Pstop': float(Boundary_P[1]),
                                            'Qstart': float(Boundary_Q[0]),
                                            'Qstop': float(Boundary_Q[1]),
                                            'Unit_Name': Unit_Name,
                                            'Unit_P': Unit_P}
                elif '[s]    Event: ' in line:
                    if 'Event: Start Simulation' not in line:                    
                        Event_Time = float(line[line.find('Time: ')+6:].replace(',', '.').split('[s')[0])
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationEvents'].append(Event_Time)
                elif 'Short-circuit' in line and self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['StartShortCircuit'] is None:
                        if 'with Fault Impedance' in '\t'.join([line, raw_file_slim_split[idx+1]]):
                            timestamp_line = raw_file_slim_split[idx - 1]
                            if timestamp_line.find('(t=') > 0:
                                if timestamp_line.find(' ms)') > 0:
                                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['StartShortCircuit'] = float(timestamp_line[timestamp_line.find('(t=')+3:timestamp_line.find(' ms)')].replace(':', '.')) / 1000
                                else:
                                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['StartShortCircuit'] = float(timestamp_line[timestamp_line.find('(t=')+3:timestamp_line.find('  s)')].replace(':', '.'))
                                    
    
                elif 'Clear Short-Circuit' in line and self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ClearShortCircuit'] is None:
                        timestamp_line = raw_file_slim_split[idx - 1]                        
                        if timestamp_line.find('(t=') > 0:
                                if timestamp_line.find(' ms)') > 0:
                                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ClearShortCircuit'] = float(timestamp_line[timestamp_line.find('(t=')+3:timestamp_line.find(' ms)')].replace(':', '.')) / 1000
                                else:
                                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ClearShortCircuit'] = float(timestamp_line[timestamp_line.find('(t=')+3:timestamp_line.find('  s)')].replace(':', '.'))
                        
                elif self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ClearShortCircuit'] is not None:
                    if 'Commutation failure' in line:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['CommutationFailure'] += 1
                    elif 'Changed to blocking mode' in line:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['BlockingMode'] += 1
                    elif 'Back to normal operation' in line:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NormalOperation'] += 1
                    elif 'Maximum number of inner-loop iterations' in line:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['InnerLoops'] += 1
                    elif 'Maximum number of outer-loop iterations' in line:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['OuterLoops'] += 1
                    elif 'Maximum number of event loops' in line:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['EventLoops'] += 1

            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['CommutationFailure'] //= 2
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['BlockingMode'] //= 2
            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NormalOperation'] //= 2

            self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NumericalIssues'] = sum(raw_file_slim.count(element) for element in ('System-Matrix Inversion failed', 'quation system could not be solved', 'Math. error in DSL')) + \
                sum(self.MATRIX_RESULTS[EB_SBE_NET][FEJL][element] // 1000 for element in ('OuterLoops', 'EventLoops', 'BlockingMode', 'NormalOperation')) + \
                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['InnerLoops'] // 10

            if sum(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'] > 0 or self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationFinishTime'] > 5 or
                   self.MATRIX_RESULTS[EB_SBE_NET][FEJL][element] for element in ('PoleSlip', 'NumericalIssues')) > 0 or \
               Boundary_DEC > Unit_P or Boundary_EXC > Unit_P or not self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SuccessfullyExecuted']:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['TrafficLight'] = 1
            elif sum(self.MATRIX_RESULTS[EB_SBE_NET][FEJL][element] for element in ('ShuntOver', 'ShuntUnder', 'AVC', 'RPC', 'MIO', 'PowerLimitExceeded')) > 0 or \
                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['CommutationFailure'] > 0:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['TrafficLight'] = -1
            else:
                self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['TrafficLight'] = 0

        TrafficLight = 0
        SimulationFinishTime = 0
        SimulationScan = 0
        SimulationDuration = 0
        PoleSlip = 0
        MaxVoltage = 0
        MinVoltage = 0
        CommutationFailure = 0
        ShuntOver = 0
        ShuntUnder = 0
        PowerLimitExceeded = 0
        AVC = 0
        RPC = 0
        MIO = 0
        SIPS = 0
        NotCorrectlyInitialised = 0
        FaultImpedance = 0
        IntegrationStep = 0
        NumericalIssues = 0
        Boundary = 0
        Boundary_list = []
        UserDefined = 0
        RED = 0
        ORANGE = 0
        GREEN = 0

        for EB_SBE_NET in self.MATRIX_RESULTS:
            for FEJL in self.MATRIX_RESULTS[EB_SBE_NET].values():
                if FEJL['TrafficLight'] != 0:
                    TrafficLight += 1
                    if FEJL['TrafficLight'] > 0:
                        RED += 1
                    else:
                        ORANGE += 1
                else:
                    GREEN += 1
                if FEJL['SimulationScan'] != 0:
                    SimulationScan += 1
                if FEJL['SimulationFinishTime'] > 5:
                    SimulationFinishTime += 1
                if FEJL['SimulationDuration'] != 'N/A':
                    SimulationDuration += 1
                if FEJL['PoleSlip'] > 0:
                    PoleSlip += 1
                if FEJL['MaxVoltage'] > 0:
                    MaxVoltage += 1
                if FEJL['MinVoltage'] > 0:
                    MinVoltage += 1
                if FEJL['CommutationFailure'] > 0:
                    CommutationFailure += 1
                if FEJL['ShuntOver'] > 0:
                    ShuntOver += 1
                if FEJL['ShuntUnder'] > 0:
                    ShuntUnder += 1
                if FEJL['PowerLimitExceeded'] > 0:
                    PowerLimitExceeded += 1
                if FEJL['AVC'] > 0:
                    AVC += 1
                if FEJL['RPC'] > 0:
                    RPC += 1
                if FEJL['MIO'] > 0:
                    MIO += 1
                if FEJL['SIPS'] > 0:
                    SIPS += 1
                if FEJL['NotCorrectlyInitialised'] > 0:
                    NotCorrectlyInitialised += 1
                if FEJL['FaultImpedance'] is not None and abs(FEJL['FaultImpedance']) > 0:
                    FaultImpedance += 1
                if FEJL['IntegrationStep'] != 'Fixed - 1.000[ms]':
                    IntegrationStep += 1
                if FEJL['NumericalIssues'] > 0:
                    NumericalIssues += 1
                if FEJL['Boundary']:
                    Boundary_list.extend([*FEJL['Boundary']])
                    for value in FEJL['Boundary'].values():
                        if abs(value['Pstop'] - value['Pstart']) >= value['Unit_P']:
                            Boundary += 1
                if FEJL['UserDefined'] > 0:
                    UserDefined += 1

        Boundary_list = [*set(Boundary_list)]
        Boundary_list.sort()

        self.mainW.laCriticalRed.setText('{0:>5.1f}% ({1} of {2})'.format(RED/self.LogFileNumber*100, RED, self.LogFileNumber))
        self.mainW.laCriticalOrange.setText('{0:>5.1f}% ({1} of {2})'.format(ORANGE/self.LogFileNumber*100, ORANGE, self.LogFileNumber))
        self.mainW.laCriticalGreen.setText('{0:>5.1f}% ({1} of {2})'.format(GREEN/self.LogFileNumber*100, GREEN, self.LogFileNumber))

        self.adjustOverviewButton(self.mainW.rb_SimulationFinishTime, SimulationFinishTime, 'Simulation finish time', 'red')
        self.adjustOverviewButton(self.mainW.rb_SimulationScan, SimulationScan, 'Simulation scan', 'red')
        self.adjustOverviewButton(self.mainW.rb_SimulationDuration, SimulationDuration, 'Simulation duration', 'black')
        self.adjustOverviewButton(self.mainW.rb_PoleSlip, PoleSlip, 'Pole slip', 'red')
        self.adjustOverviewButton(self.mainW.rb_MaxVoltage, MaxVoltage, 'Max voltage', 'red')
        self.adjustOverviewButton(self.mainW.rb_MinVoltage, MinVoltage, 'Min voltage', 'red')
        self.adjustOverviewButton(self.mainW.rb_CommutationFailure, CommutationFailure, 'Commutation failure', 'orange')
        self.adjustOverviewButton(self.mainW.rb_ShuntOver, ShuntOver, 'Shunt relay U>', 'orange')
        self.adjustOverviewButton(self.mainW.rb_ShuntUnder, ShuntUnder, 'Shunt relay U<', 'orange')
        self.adjustOverviewButton(self.mainW.rb_PowerLimitExceeded, PowerLimitExceeded, 'Power limit exceeded', 'orange')
        self.adjustOverviewButton(self.mainW.rb_AVC_RPC_MIO, AVC + RPC + MIO, 'AVC / RPC / MIO', 'orange')
        self.adjustOverviewButton(self.mainW.rb_SIPS, SIPS, 'SIPS', 'orange')
        self.adjustOverviewButton(self.mainW.rb_NotCorrectlyInitialised, NotCorrectlyInitialised, 'Not correctly initialised', 'black')
        self.adjustOverviewButton(self.mainW.rb_FaultImpedance, FaultImpedance, 'Fault impedance', 'black')
        self.adjustOverviewButton(self.mainW.rb_IntegrationStep, IntegrationStep, 'Integration step', 'black')
        self.adjustOverviewButton(self.mainW.rb_NumericalIssues, NumericalIssues, 'Numerical issues', 'red')
        self.adjustOverviewButton(self.mainW.rb_UserDefined, UserDefined, 'User defined', 'black')

        self.mainW.cbBoundary.clear()
        if Boundary:
            self.mainW.rb_BoundaryMW.setText('Boundary ∆P [MW] ({0} of {1})'.format(Boundary, self.LogFileNumber * len(Boundary_list)))
            self.mainW.rb_BoundaryMW.setStyleSheet('color: red')
        else:
            self.mainW.rb_BoundaryMW.setText('Boundary ∆P [MW]')
            self.mainW.rb_BoundaryMW.setStyleSheet(None)

        if Boundary_list:
            for element in (self.mainW.rb_BoundaryMW, self.mainW.rb_BoundaryMVAr, self.mainW.cbBoundary):
                element.setEnabled(True)

            idx_h = 0
            idx_l = 0
            for boundary in Boundary_list:
                trigger_h = False
                trigger_l = False
                for EB_SBE_NET in self.MATRIX_RESULTS:
                    for FEJL in self.MATRIX_RESULTS[EB_SBE_NET].values():
                        if boundary in FEJL['Boundary']:
                            if abs(FEJL['Boundary'][boundary]['Pstop'] - FEJL['Boundary'][boundary]['Pstart']) >= FEJL['Boundary'][boundary]['Unit_P']:
                                trigger_h = True
                            elif 'HVDC' in boundary and abs(FEJL['Boundary'][boundary]['Pstart']) > 1 and \
                                    abs(FEJL['Boundary'][boundary]['Pstop'] - FEJL['Boundary'][boundary]['Pstart']) / abs(FEJL['Boundary'][boundary]['Pstart']) * 100 >= 1:
                                trigger_l = True
                if trigger_h:
                    self.mainW.cbBoundary.insertItem(idx_h, self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_MessageBoxCritical')), boundary)
                    idx_h += 1
                elif trigger_l:
                    self.mainW.cbBoundary.insertItem(idx_h + idx_l, self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_MessageBoxWarning')), boundary)
                    idx_l += 1
                else:
                    self.mainW.cbBoundary.addItem(boundary)
            self.mainW.cbBoundary.setCurrentIndex(0)
        else:
            for element in (self.mainW.rb_BoundaryMW, self.mainW.rb_BoundaryMVAr, self.mainW.cbBoundary):
                element.setEnabled(False)
            self.mainW.cbBoundary.addItem('N/A')

        self.mainW.rb_TrafficLight.setText('Traffic Light ({0} of {1})'.format(TrafficLight, self.LogFileNumber))
        self.mainW.rb_TrafficLight.setChecked(True)
        self.fillInOverview()

        self.screenPlotFiles()

        self.BypassDate = True
        indexNew = [*self.MATRIX_FILES]
        indexNew.sort()
        for element in (self.mainW.cbDate, self.mainW.cbDateMaster, self.mainW.cbDateSlave):
            element.clear()
            element.addItems(indexNew)
            element.setCurrentIndex(0)
        self.BypassDate = False
        self.updateCbDate()
        self.updateCbDateMaster()
        self.increaseIndex(self.mainW.cbFaultSlave, 1)

        for element in (self.mainW.groupBox_1, self.mainW.groupBox_2,
                        self.mainW.tableWidget, self.mainW.Details,
                        self.mainW.Compare, self.mainW.btnSaveOverview, self.mainW.cbSaveActualView):
            element.setEnabled(True)

    def screenPlotFiles(self):
        PlotFiles = [(EB_SBE_NET, FEJL, self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Path'])
                     for EB_SBE_NET in self.MATRIX_FILES for FEJL in self.MATRIX_FILES[EB_SBE_NET]['Faults']
                     if self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Path'] is not None]

        PlotFileNumber = len(PlotFiles)

        self.progress.setMaximum(PlotFileNumber)
        fProcessedPlotFilesSize = 0
        for idx_plot, (EB_SBE_NET, FEJL, PlotFile) in enumerate(PlotFiles, start=1):

            if fProcessedPlotFilesSize < 1024:
                aProcessedPlotFilesSize = '{0:.2f} [MB]'.format(fProcessedPlotFilesSize)
            else:
                aProcessedPlotFilesSize = '{0:.2f} [GB]'.format(fProcessedPlotFilesSize/1024)

            self.progress.setLabelText('Scanning PlotFile: {0} \\ {1} ({2:.2f} [MB] \\ {3})\n{4}'.format(idx_plot, PlotFileNumber,
                                       self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['FileSize'], aProcessedPlotFilesSize, PlotFile))

            self.progress.setValue(idx_plot)
            if self.progress.wasCanceled():
                return

            fProcessedPlotFilesSize += self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['FileSize']

            if idx_plot==1:
                with open(PlotFile,'rb') as fid:
                    self.PlotFileEncoding = chardet.detect(b''.join([fid.readline() for _ in range(5)]))['encoding']

            with open(PlotFile) as fid:
                LINE = [fid.readline() for idx in range(5)]

            temp = []
            for idx, line in enumerate(LINE):
                line_split = line.replace('"', '').split(',')
                try:
                    float(line_split[0])
                except Exception:
                    temp.append(line_split)
                else:
                    break
                    
            try:
                headers_raw = [(el1, el2) for el1, el2 in zip(temp[-2], temp[-1])]
            
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['skiprows'] = idx
                seen = set()
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['names'] = [
                    ('duplicate_{0}'.format(idx), element) if element in seen else seen.add(element) or element
                    for idx, element in enumerate(headers_raw, start=1)
                ]
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['dtype'] = {element: float for element in self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['names']}
            except Exception:
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Path'] = None
            
    def fillInOverview(self):
        EB_SBE_NET = [*self.MATRIX_FILES]
        EB_SBE_NET.sort()

        FEJL = [*set([value for values in self.MATRIX_FILES.values() for value in values['Faults']])]
        FEJL.sort()

        if self.mainW.cbResultsTransposition.isChecked():
            EB_SBE_NET, FEJL = FEJL, EB_SBE_NET
        self.mainW.tableWidget.clear()
        self.mainW.tableWidget.setColumnCount(len(EB_SBE_NET))
        self.mainW.tableWidget.setRowCount(len(FEJL))
        self.mainW.tableWidget.setHorizontalHeaderLabels(EB_SBE_NET)
        self.mainW.tableWidget.setVerticalHeaderLabels(FEJL)
        self.evaluateLogFiles()

    def evaluateLogFiles(self):
        self.mainW.tableWidget.clearSelection()

        BOUNDARY = self.mainW.rb_BoundaryMW.isChecked() or self.mainW.rb_BoundaryMVAr.isChecked()
        self.mainW.cbBoundary.setEnabled(BOUNDARY)
        self.mainW.cbHighlightNumericalIssues.setEnabled(self.mainW.rb_TrafficLight.isChecked())

        for column in range(self.mainW.tableWidget.columnCount()):
            bHide = True
            if self.mainW.cbResultsTransposition.isChecked():
                FEJL = self.mainW.tableWidget.horizontalHeaderItem(column).text()
            else:
                EB_SBE_NET = self.mainW.tableWidget.horizontalHeaderItem(column).text()
                if EB_SBE_NET not in self.MATRIX_RESULTS:
                    return

            for row in range(self.mainW.tableWidget.rowCount()):
                if self.mainW.cbResultsTransposition.isChecked():
                    EB_SBE_NET = self.mainW.tableWidget.verticalHeaderItem(row).text()
                    if EB_SBE_NET not in self.MATRIX_RESULTS:
                        return
                else:
                    FEJL = self.mainW.tableWidget.verticalHeaderItem(row).text()

                self.mainW.tableWidget.takeItem(row, column)

                if FEJL in self.MATRIX_RESULTS[EB_SBE_NET]:

                    if self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['FileSize'] > self.LogFileSizeLimit:
                        self.mainW.tableWidget.setItem(row, column, QtWidgets.QTableWidgetItem())
                        self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(150, 150, 150, 255))
                        self.mainW.tableWidget.item(row, column).setToolTip('LogFile is too big to be opened by DSAlight')
                        continue

                    if self.mainW.rb_TrafficLight.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['TrafficLight']
                    elif self.mainW.rb_SimulationScan.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan']
                    elif self.mainW.rb_SimulationFinishTime.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationFinishTime']
                    elif self.mainW.rb_SimulationDuration.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationDuration']
                    elif self.mainW.rb_PoleSlip.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PoleSlip']
                    elif self.mainW.rb_MaxVoltage.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MaxVoltage']
                    elif self.mainW.rb_MinVoltage.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MinVoltage']
                    elif self.mainW.rb_CommutationFailure.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['CommutationFailure']
                    elif self.mainW.rb_ShuntOver.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntOver']
                    elif self.mainW.rb_ShuntUnder.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntUnder']
                    elif self.mainW.rb_PowerLimitExceeded.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PowerLimitExceeded']
                    elif self.mainW.rb_AVC_RPC_MIO.isChecked():
                        VALUE = sum(self.MATRIX_RESULTS[EB_SBE_NET][FEJL][element] for element in ('AVC', 'RPC', 'MIO'))
                    elif self.mainW.rb_SIPS.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SIPS']
                    elif self.mainW.rb_NotCorrectlyInitialised.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NotCorrectlyInitialised']
                    elif self.mainW.rb_FaultImpedance.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['FaultImpedance']
                    elif self.mainW.rb_IntegrationStep.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['IntegrationStep']
                    elif self.mainW.rb_NumericalIssues.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NumericalIssues']
                    elif self.mainW.rb_UserDefined.isChecked():
                        VALUE = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['UserDefined']

                    if not BOUNDARY:
                        self.mainW.tableWidget.setItem(row, column, QtWidgets.QTableWidgetItem())
                        if self.mainW.rb_FaultImpedance.isChecked():
                            if VALUE is None:
                                self.mainW.tableWidget.takeItem(row, column)
                            elif abs(VALUE) > 0:
                                self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 170, 0, 255))
                                self.mainW.tableWidget.item(row, column).setText('{0:.3f} + j {1:.3f} [Ohm]'.format(VALUE.real, VALUE.imag))
                            else:
                                self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(0, 255, 0, 255))
                        elif self.mainW.rb_SimulationScan.isChecked() and VALUE == -1:
                            self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 170, 0, 255))
                            self.mainW.tableWidget.item(row, column).setText('Deactivated')
                        elif (self.mainW.rb_SimulationFinishTime.isChecked() and self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationStopTime'] == 'N/A') or \
                             (self.mainW.rb_SimulationDuration.isChecked() and VALUE == 'N/A'):
                            self.mainW.tableWidget.takeItem(row, column)
                        elif self.mainW.rb_SimulationDuration.isChecked():
                            self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(0, 255, 0, 255))
                            self.mainW.tableWidget.item(row, column).setText('{0}'.format(VALUE))
                        elif self.mainW.rb_UserDefined.isChecked() and VALUE > 0:
                            self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 170, 0, 255))
                            self.mainW.tableWidget.item(row, column).setText('{0}'.format(VALUE))
                        elif self.mainW.rb_TrafficLight.isChecked() and not self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SuccessfullyExecuted']:
                            self.mainW.tableWidget.takeItem(row, column)
                        elif self.mainW.rb_IntegrationStep.isChecked():
                            self.mainW.tableWidget.item(row, column).setText('{0}'.format(VALUE))
                            if VALUE == 'Fixed - 1.000[ms]':
                                self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(0, 255, 0, 255))
                            else:
                                self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 170, 0, 255))
                        elif (self.mainW.rb_CommutationFailure.isChecked() and VALUE > 0) or \
                                (self.mainW.rb_SimulationFinishTime.isChecked() and VALUE > 5) or \
                                (not self.mainW.rb_CommutationFailure.isChecked() and not self.mainW.rb_SimulationFinishTime.isChecked()
                                 and VALUE > 0):
                            self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 0, 0, 255))
                            if self.mainW.rb_SimulationFinishTime.isChecked():
                                self.mainW.tableWidget.item(row, column).setText('{0:.1f} [s]'.format(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationStopTime']))
                            elif not self.mainW.rb_TrafficLight.isChecked():
                                self.mainW.tableWidget.item(row, column).setText('{0}'.format(VALUE))
                        elif VALUE < 0:
                            self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 170, 0, 255))
                        else:
                            self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(0, 255, 0, 255))
                    else:
                        if self.mainW.cbBoundary.currentText() in self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['Boundary']:
                            self.mainW.tableWidget.setItem(row, column, QtWidgets.QTableWidgetItem())
                            boundary = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['Boundary'][self.mainW.cbBoundary.currentText()]
                            if self.mainW.rb_BoundaryMW.isChecked():
                                delta = boundary['Pstop'] - boundary['Pstart']
                            else:
                                delta = boundary['Qstop'] - boundary['Qstart']
                            self.mainW.tableWidget.item(row, column).setText('{0:.1f}'.format(delta))
                            if self.mainW.rb_BoundaryMW.isChecked() and abs(delta) >= boundary['Unit_P']:
                                self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 0, 0, 255))
                            elif 'HVDC' in self.mainW.cbBoundary.currentText() and abs(boundary['Pstart']) > 1 and \
                                    abs(boundary['Pstop'] - boundary['Pstart']) / abs(boundary['Pstart']) * 100 >= 1:
                                self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(255, 170, 0, 255))
                            else:
                                self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(0, 255, 0, 255))

                    if not self.mainW.tableWidget.item(row, column):
                        self.mainW.tableWidget.setItem(row, column, QtWidgets.QTableWidgetItem())
                        self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(150, 150, 150, 255))

                    if self.mainW.rb_TrafficLight.isChecked() and self.mainW.cbHighlightNumericalIssues.isChecked():
                        if not self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SuccessfullyExecuted']:
                            self.mainW.tableWidget.item(row, column).setText('Not successfully executed')
                        elif self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NumericalIssues']:
                            self.mainW.tableWidget.item(row, column).setText('Numerical issues')

                    self.mainW.tableWidget.item(row, column).setToolTip('\n'.join(['{0}: {1}'.format(
                            key, self.MATRIX_RESULTS[EB_SBE_NET][FEJL][value]) for key, value in zip(
                                    ('Simulation finish time', 'Simulation Scan', 'Simulation duration',
                                     'Pole slip', 'Max voltage', 'Min voltage', 'Commutation failure',
                                     'Shunt relay U>', 'Shunt relay U<', 'Power limit exceeded',
                                     'AVC', 'RPC', 'MIO', 'SIPS', 'Not correctly initialised',
                                     'Fault impedance', 'Integration Step', 'Numerical issues', 'User defined'),
                                    ('SimulationStopTime', 'SimulationScan', 'SimulationDuration',
                                     'PoleSlip', 'MaxVoltage', 'MinVoltage', 'CommutationFailure',
                                     'ShuntOver', 'ShuntUnder', 'PowerLimitExceeded',
                                     'AVC', 'RPC', 'MIO', 'SIPS', 'NotCorrectlyInitialised',
                                     'FaultImpedance', 'IntegrationStep', 'NumericalIssues', 'UserDefined'))]))
                else:
                    self.mainW.tableWidget.setItem(row, column, QtWidgets.QTableWidgetItem())
                    self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(150, 150, 150, 255))
                    self.mainW.tableWidget.item(row, column).setToolTip('LogFile not found')

                if not self.mainW.tableWidget.item(row, column):
                    self.mainW.tableWidget.setItem(row, column, QtWidgets.QTableWidgetItem())
                    self.mainW.tableWidget.item(row, column).setBackground(QtGui.QColor(150, 150, 150, 255))
                elif self.mainW.tableWidget.item(row, column).text():
                    self.mainW.tableWidget.item(row, column).setTextAlignment(QtCore.Qt.AlignCenter)
                    if self.mainW.tableWidget.item(row, column).background() != QtGui.QColor(0, 255, 0, 255):
                        self.mainW.tableWidget.item(row, column).setForeground(QtGui.QColor('white'))
                        self.mainW.tableWidget.item(row, column).setFont(self.font)

                if self.mainW.tableWidget.item(row, column).background() != QtGui.QColor(0, 255, 0, 255):
                    bHide = False

            if self.mainW.cBHideStableCases.isChecked() and bHide:
                self.mainW.tableWidget.hideColumn(column)
            else:
                self.mainW.tableWidget.showColumn(column)

        for row in range(self.mainW.tableWidget.rowCount()):
            bHide = True
            for column in range(self.mainW.tableWidget.columnCount()):
                if self.mainW.tableWidget.item(row, column).background() != QtGui.QColor(0, 255, 0, 255):
                    bHide = False
                    break
            if self.mainW.cBHideStableCases.isChecked() and bHide:
                self.mainW.tableWidget.hideRow(row)
            else:
                self.mainW.tableWidget.showRow(row)

        self.mainW.tableWidget.resizeColumnsToContents()
        self.mainW.tableWidget.resizeRowsToContents()

    def detailedView(self, row, column):
        EB_SBE_NET = self.mainW.tableWidget.horizontalHeaderItem(column).text()
        FEJL = self.mainW.tableWidget.verticalHeaderItem(row).text()
        if self.mainW.cbResultsTransposition.isChecked():
            EB_SBE_NET, FEJL = FEJL, EB_SBE_NET
        if EB_SBE_NET in self.MATRIX_FILES and FEJL in self.MATRIX_FILES[EB_SBE_NET]['Faults']:
            if self.mainW.cbDate.currentText() == EB_SBE_NET:
                self.mainW.cbFault.setCurrentText(FEJL)
            else:
                self.BypassFault = True
                self.mainW.cbFault.clear()
                self.mainW.cbFault.addItems(sorted(self.MATRIX_FILES[EB_SBE_NET]['Faults']))
                self.mainW.cbFault.setCurrentText(FEJL)
                self.mainW.cbDate.setCurrentText(EB_SBE_NET)
            self.mainW.tabWidget.setCurrentIndex(1)

    def searchInLogFile(self, iterator=None):
        if not self.mainW.cbLockToFirstLine.isChecked() and iterator is not None:
            if iterator == 'redLightButton':
                iterator = ('Simulation Scan', 'maximum voltage limit violated',
                            'minimum voltage limit violated', 'FRT limit',
                            'Generator out of step (pole slip)',
                            'System-Matrix Inversion failed', 'quation system could not be solved', 'Math. error in DSL')
            elif iterator == 'orangeLightButton':
                iterator = ('Commutation failure', 'ShuntRelay')
            elif iterator == 'NotCorrectlyInitialised':
                iterator = ('is not correctly initialised',
                            'is initialised to a non-negligible value',
                            'limited state initialisation expression evaluates to a value outside of its limits',
                            'Governor lower limit exceeded',
                            'Governor upper limit exceeded')

            Cursor = self.mainW.logViewer.textCursor()
            for idx, line in enumerate(self.raw_file_slim_split[Cursor.blockNumber()+1:], start=1):
                for element in iterator:
                    if element in line or ('.*' in element and re.search(re.compile(element), line)):
                        Cursor.movePosition(QtGui.QTextCursor.NextBlock, 0, idx)
                        Cursor.select(QtGui.QTextCursor.LineUnderCursor)
                        self.mainW.logViewer.setTextCursor(Cursor)
                        self.mainW.logViewer.setFocus()
                        return
        self.mainW.logViewer.moveCursor(QtGui.QTextCursor.Start, QtGui.QTextCursor.MoveAnchor)

    def searchUserDefined(self):
        UserDefined = 0
        for EB_SBE_NET in self.MATRIX_RESULTS:
            for FEJL in self.MATRIX_RESULTS[EB_SBE_NET]:
                if self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content'] is None:
                    with open(self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Path'], buffering=1, encoding='cp1252') as fid:
                        self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['UserDefined'] = fid.read().count(self.mainW.leUserDefined.text())
                else:
                    self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['UserDefined'] = self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content'].count(self.mainW.leUserDefined.text())
                if self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['UserDefined'] > 0:
                    UserDefined += 1

        self.adjustOverviewButton(self.mainW.rb_UserDefined, UserDefined, 'User defined', 'black')

        if UserDefined > 0:
            self.mainW.rb_UserDefined.click()
        else:
            self.mainW.rb_TrafficLight.click()

    def updateCbDate(self):
        if not self.BypassDate:
            self.mainW.lbDate.setText('Point in time: {0} of {1}'.format(self.mainW.cbDate.currentIndex()+1, len(self.MATRIX_FILES)))
            indexOld = self.mainW.cbFault.currentText()
            self.BypassFault = True
            self.mainW.cbFault.clear()
            self.mainW.cbFault.addItems(sorted(self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Faults']))
            if indexOld in self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Faults']:
                self.mainW.cbFault.setCurrentText(indexOld)
            else:
                self.mainW.cbFault.setCurrentIndex(0)
            self.BypassFault = False
            self.updateCbFault()

    def updateCbFault(self):
        if not self.BypassFault:
            self.updateLog()
            self.mainW.lbFault.setText('Fault location: {0} of {1}'.format(self.mainW.cbFault.currentIndex()+1, len(self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Faults'])))
            indexOld = self.mainW.cbPlot.currentText()
            if indexOld != 'N/A' or self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Layout']:
                self.BypassPlot = True
                self.mainW.cbPlot.clear()
                if not self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Layout']:
                    self.mainW.cbPlot.addItem('N/A')
                else:
                    self.mainW.cbPlot.addItems(sorted(self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Layout']))
                    if indexOld in self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Layout']:
                        self.mainW.cbPlot.setCurrentText(indexOld)
                    else:
                        self.mainW.cbPlot.setCurrentIndex(0)
                self.BypassPlot = False
                self.updateCbPlot()

    def updateCbPlot(self):
        if not self.BypassPlot:
            if self.mainW.cbPlot.currentText() == 'N/A':
                self.mainW.lbPlot.setText('Plot: N/A')
                self.mainW.btnPrevPlot.setEnabled(False)
                self.mainW.btnNextPlot.setEnabled(False)
            else:
                self.mainW.lbPlot.setText('Plot: {0} of {1}'.format(self.mainW.cbPlot.currentIndex()+1, len(self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Layout'])))
                self.mainW.btnPrevPlot.setEnabled(True)
                self.mainW.btnNextPlot.setEnabled(True)

            self.PlotFile = self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Faults'][self.mainW.cbFault.currentText()]['PlotFile']['Path']
            if self.PlotFile is not None and self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Layout'][self.mainW.cbPlot.currentText()] is not None:
                self.updatePlot(self.fig, self.mainW.cbDate.currentText(), self.mainW.cbFault.currentText(), self.mainW.cbPlot.currentText())
                for element in (self.mainW.widget_matplot, self.mainW.btnExportPlots, self.mainW.cb_GraphicsDisplayResolution, self.mainW.cbPlotExtension,
                                self.mainW.cbAllTimes, self.mainW.cbAllLocations, self.mainW.cbAllPlots,
                                self.mainW.cbLimitX, self.mainW.cbFreezeYDetails, self.mainW.cbRemoveLegend):
                    element.setEnabled(True)
                self.mainW.btnShowPlot.setEnabled(False)
                xlim = self.mainW.cbLimitX.isChecked()
                self.mainW.dSPXmin.setEnabled(xlim)
                self.mainW.dSPXmax.setEnabled(xlim)
            else:
                self.drawEmptyMatplot(self.fig)
                for element in (self.mainW.widget_matplot, self.mainW.btnShowPlot, self.mainW.btnExportPlots, self.mainW.cb_GraphicsDisplayResolution, self.mainW.cbPlotExtension,
                                self.mainW.cbAllTimes, self.mainW.cbAllLocations, self.mainW.cbAllPlots,
                                self.mainW.cbLimitX, self.mainW.dSPXmin, self.mainW.dSPXmax, self.mainW.cbFreezeYDetails, self.mainW.cbRemoveLegend):
                    element.setEnabled(False)

    def updatePlot(self, fig, EB_SBE_NET, FEJL, PLOT):
        CSV_HEADER, CSV_DATA = self.readPlotData(EB_SBE_NET, FEJL, PLOT)

        XLIM = [CSV_DATA.iloc[0, 0], CSV_DATA.iloc[-1, 0]]

        VP_mode, VP_data = self.checkVoltageProilfe(EB_SBE_NET, FEJL, PLOT, XLIM)

        if fig == self.fig:
            self.mainW.dSPXmin.setMinimum(XLIM[0])
            self.mainW.dSPXmin.setMaximum(XLIM[1] - 0.1)
            self.mainW.dSPXmax.setMinimum(XLIM[0] + 0.1)
            self.mainW.dSPXmax.setMaximum(XLIM[1])

        if self.mainW.cbLimitX.isChecked():
            XLIM = [self.mainW.dSPXmin.value(), self.mainW.dSPXmax.value()]

        ROW, COL, ORDER = self.arrangeSubplots(len(self.MATRIX_FILES[EB_SBE_NET]['Layout'][PLOT]))

        XLABEL = self.xLabel(CSV_HEADER[0][1])

        for ax in fig.get_axes():
            fig.delaxes(ax)

        axes = fig.subplots(nrows=ROW, ncols=COL, sharex=True, subplot_kw=dict(xlim=XLIM))
        if not isinstance(axes, np.ndarray):
            axes = np.array(axes)

        for subplot, ax in zip(sorted(self.MATRIX_FILES[EB_SBE_NET]['Layout'][PLOT].items()), (axes.flat[idx] for idx in ORDER)):
            ax.set_title(subplot[0])
            ax.title.set_visible(False)
            ax.grid(True)
            ax.xaxis.set_tick_params(labelbottom=True)
            ax.set_xlabel(XLABEL, horizontalalignment='right', x=1.0)
            legend = []
            for lValues in subplot[1][0]:
                if len(lValues) == 3:
                    aElement, aVariable, iColor = lValues
                    bNorm = False
                else:
                    aElement, aVariable, iColor, bNorm, fNomVal = lValues
                for data_index, header in enumerate(CSV_HEADER):
                    if (aElement in header[0] and aVariable in header[1]) or \
                            (header[1] in ('b:dtgrd', 'b:ierrgrd', 'b:warnA', 'b:warnB', 'b:warnC') and aVariable == header[1]):
                        if bNorm:
                            ax.plot(CSV_DATA.iloc[:, 0], CSV_DATA.iloc[:, data_index]/fNomVal, self.MCOLORS.get(iColor, iColor if isinstance(iColor, str) else ''))
                            legend.append(' '.join([*header, '(base: {0:.3f})'.format(fNomVal)]).replace('$', ''))
                        else:
                            ax.plot(CSV_DATA.iloc[:, 0], CSV_DATA.iloc[:, data_index], self.MCOLORS.get(iColor, iColor if isinstance(iColor, str) else ''))
                            legend.append(' '.join(header).replace('$', ''))
                        break

            if legend:
                self.drawConstant(ax, subplot[1][1], XLIM)
                self.createVoltageProfile(VP_mode, VP_data, EB_SBE_NET, PLOT, ax, legend)

                if fig == self.fig or not self.mainW.cbRemoveLegend.isChecked():
                    leg = ax.legend(legend, loc='upper left', bbox_to_anchor=(0.0, -0.1), frameon=0)
                    leg.set_draggable(True)
                    leg.set_visible(not self.mainW.cbRemoveLegend.isChecked())

        if self.mainW.cbFreezeYDetails.isChecked():
            if len(ORDER) == len(self.YLIM[fig]):
                for idx, ax in enumerate(fig.get_axes()):
                    ax.set_ylim(self.YLIM[fig][idx])
            else:
                self.mainW.cbFreezeYDetails.setChecked(False)

        if fig == self.fig:
            fig.canvas.draw()

    def updateLog(self):
        for element in (self.mainW.btnSimulationDuration,
                        self.mainW.btnSimulationStopTime,
                        self.mainW.btnExpectedSimulationStop,
                        self.mainW.btnTripOfCHP,
                        self.mainW.btnTripOfWindOld,
                        self.mainW.btnACExchange,
                        self.mainW.btnPoleSlip,
                        self.mainW.btnMaxVoltage,
                        self.mainW.btnMinVoltage,
                        self.mainW.btnSimulationScan,
                        self.mainW.btnShuntOver,
                        self.mainW.btnShuntUnder,
                        self.mainW.btnCommutationFailure,
                        self.mainW.btnBlockingMode,
                        self.mainW.btnNormalOperation,
                        self.mainW.btnAVC,
                        self.mainW.btnRPC,
                        self.mainW.btnMIO,
                        self.mainW.btnPowerLimitExceeded,
                        self.mainW.btnSIPS,
                        self.mainW.btnInnerLoops,
                        self.mainW.btnOuterLoops,
                        self.mainW.btnEventLoops,
                        self.mainW.btnNotCorrectlyInitialised,
                        self.mainW.btnNumericalIssues):
            element.setStyleSheet('font: 8pt; color: black')

        for element in (self.mainW.RMS_SimulationDuration,
                        self.mainW.RMS_SimulationStopTime,
                        self.mainW.RMS_ExpectedSimulationStop,
                        self.mainW.RMS_TripOfCHP,
                        self.mainW.RMS_TripOfWindOld,
                        self.mainW.RMS_ACExchange,
                        self.mainW.RMS_PoleSlip,
                        self.mainW.RMS_MaxVoltage,
                        self.mainW.RMS_MinVoltage,
                        self.mainW.RMS_SimulationScan,
                        self.mainW.RMS_ShuntOver,
                        self.mainW.RMS_ShuntUnder,
                        self.mainW.RMS_CommutationFailure,
                        self.mainW.RMS_BlockingMode,
                        self.mainW.RMS_NormalOperation,
                        self.mainW.RMS_AVC,
                        self.mainW.RMS_RPC,
                        self.mainW.RMS_MIO,
                        self.mainW.RMS_PowerLimitExceeded,
                        self.mainW.RMS_SIPS,
                        self.mainW.RMS_InnerLoops,
                        self.mainW.RMS_OuterLoops,
                        self.mainW.RMS_EventLoops,
                        self.mainW.RMS_NotCorrectlyInitialised,
                        self.mainW.RMS_NumericalIssues,
                        self.mainW.RMS_ExecutionDate):
            element.setText('N/A')
            element.setStyleSheet('font: 8pt; color: black')

        tl_style = 'QPushButton {background-color: rgb(200, 200, 200); border-radius:15px}'
        for element in (self.mainW.btnRedLight, self.mainW.btnOrangeLight, self.mainW.btnGreenLight):
            element.setStyleSheet(tl_style)

        EB_SBE_NET = self.mainW.cbDate.currentText()
        FEJL = self.mainW.cbFault.currentText()

        self.LogFile = self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Path']

        for element in (self.mainW.btnOpenLog,
                        self.mainW.btnShowLog,
                        self.mainW.btnLogToClip,
                        self.mainW.cbTimeStamp,
                        self.mainW.cbLineWrap,
                        self.mainW.cbLockToFirstLine,
                        self.mainW.btnGreenLight,
                        self.mainW.btnOrangeLight,
                        self.mainW.btnRedLight):
            element.setEnabled(self.LogFile is not None)

        if self.LogFile is not None:

            if self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content'] is not None:
                if self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['FileSize'] <= self.LogFileSizeLimit:
                    self.mainW.laLogFileSize.setStyleSheet('font: 8pt')
                else:
                    self.mainW.laLogFileSize.setStyleSheet('font: bold 8pt; color: red')
                self.raw_file = self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content']
                self.raw_file_slim = self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content_slim']
                self.raw_file_slim_split = self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Content_slim_split']
            else:
                self.mainW.laLogFileSize.setStyleSheet('font: bold 8pt')
                with open(self.LogFile, buffering=1, encoding='cp1252') as fid:
                    self.raw_file = fid.read()
                if self.raw_file[0] == '[':
                    self.raw_file_slim = '\n'.join(line[line.find('DIgSI/')+6:] for line in self.raw_file.split('\n'))
                else:
                    self.raw_file_slim = self.raw_file.replace('DIgSI/', '')
                self.raw_file_slim_split = self.raw_file_slim.split('\n')

            self.mainW.cbTimeStamp.setEnabled(self.raw_file[0] == '[')
            self.showTimestamp()

            self.mainW.laLogFileSize.setText('Logfile size: {0:5.2f} [MB]'.format(self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['FileSize']))
            self.mainW.laLineNumber.setText('Line number: {0}'.format(self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['LogFile']['Number_of_lines']))

            self.mainW.RMS_CommutationFailure.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['CommutationFailure']))
            self.mainW.RMS_BlockingMode.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['BlockingMode']))
            self.mainW.RMS_NormalOperation.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NormalOperation']))
            self.mainW.RMS_PoleSlip.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PoleSlip']))
            self.mainW.RMS_SimulationScan.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan']))
            self.mainW.RMS_MaxVoltage.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MaxVoltage']))
            self.mainW.RMS_MinVoltage.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MinVoltage']))
            self.mainW.RMS_ShuntOver.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntOver']))
            self.mainW.RMS_ShuntUnder.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntUnder']))
            self.mainW.RMS_InnerLoops.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['InnerLoops']))
            self.mainW.RMS_OuterLoops.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['OuterLoops']))
            self.mainW.RMS_EventLoops.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['EventLoops']))
            self.mainW.RMS_NumericalIssues.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NumericalIssues']))
            self.mainW.RMS_SimulationDuration.setText(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationDuration'])
            self.mainW.RMS_SimulationStopTime.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationStopTime']))
            self.mainW.RMS_ExpectedSimulationStop.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExpectedSimulationStop']))
            self.mainW.RMS_ExecutionDate.setText(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExecutionDate'])
            self.mainW.RMS_NotCorrectlyInitialised.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NotCorrectlyInitialised']))
            self.mainW.RMS_AVC.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['AVC']))
            self.mainW.RMS_RPC.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['RPC']))
            self.mainW.RMS_MIO.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MIO']))
            self.mainW.RMS_SIPS.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SIPS']))
            self.mainW.RMS_PowerLimitExceeded.setText(str(self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PowerLimitExceeded']))

            if self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['TrafficLight'] == 1:
                self.mainW.btnRedLight.setStyleSheet('QPushButton {background-color: red; border-radius:15px}')
                self.searchInLogFile('redLightButton')
            elif self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['TrafficLight'] == -1:
                self.mainW.btnOrangeLight.setStyleSheet('QPushButton {background-color: orange; border-radius:15px}')
                self.searchInLogFile('orangeLightButton')
            else:
                self.mainW.btnGreenLight.setStyleSheet('QPushButton {background-color: green; border-radius:15px}')

            if self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'] < 0:
                self.adjustDetailsButton(self.mainW.btnSimulationScan, self.mainW.RMS_SimulationScan, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'], -2, 'orange')
            else:
                self.adjustDetailsButton(self.mainW.btnSimulationScan, self.mainW.RMS_SimulationScan, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationScan'], 0, 'red')
            self.adjustDetailsButton(self.mainW.btnSimulationStopTime, self.mainW.RMS_SimulationStopTime, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationFinishTime'], 5, 'red')
            self.adjustDetailsButton(self.mainW.btnPoleSlip, self.mainW.RMS_PoleSlip, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PoleSlip'], 0, 'red')
            self.adjustDetailsButton(self.mainW.btnCommutationFailure, self.mainW.RMS_CommutationFailure, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['CommutationFailure'], 0, 'orange')
            self.adjustDetailsButton(self.mainW.btnBlockingMode, self.mainW.RMS_BlockingMode, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['BlockingMode'], 1000, 'red')
            self.adjustDetailsButton(self.mainW.btnNormalOperation, self.mainW.RMS_NormalOperation, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NormalOperation'], 1000, 'red')
            self.adjustDetailsButton(self.mainW.btnMaxVoltage, self.mainW.RMS_MaxVoltage, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MaxVoltage'], 0, 'red')
            self.adjustDetailsButton(self.mainW.btnMinVoltage, self.mainW.RMS_MinVoltage, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MinVoltage'], 0, 'red')
            self.adjustDetailsButton(self.mainW.btnShuntOver, self.mainW.RMS_ShuntOver, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntOver'], 0, 'orange')
            self.adjustDetailsButton(self.mainW.btnShuntUnder, self.mainW.RMS_ShuntUnder, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ShuntUnder'], 0, 'orange')
            self.adjustDetailsButton(self.mainW.btnInnerLoops, self.mainW.RMS_InnerLoops, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['InnerLoops'], 10, 'red')
            self.adjustDetailsButton(self.mainW.btnOuterLoops, self.mainW.RMS_OuterLoops, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['OuterLoops'], 1000, 'red')
            self.adjustDetailsButton(self.mainW.btnEventLoops, self.mainW.RMS_EventLoops, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['EventLoops'], 1000, 'red')
            self.adjustDetailsButton(self.mainW.btnNumericalIssues, self.mainW.RMS_NumericalIssues, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NumericalIssues'], 0, 'red')
            self.adjustDetailsButton(self.mainW.btnNotCorrectlyInitialised, self.mainW.RMS_NotCorrectlyInitialised, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['NotCorrectlyInitialised'], 0, None)
            self.adjustDetailsButton(self.mainW.btnAVC, self.mainW.RMS_AVC, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['AVC'], 0, 'orange')
            self.adjustDetailsButton(self.mainW.btnRPC, self.mainW.RMS_RPC, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['RPC'], 0, 'orange')
            self.adjustDetailsButton(self.mainW.btnMIO, self.mainW.RMS_MIO, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['MIO'], 0, 'orange')
            self.adjustDetailsButton(self.mainW.btnSIPS, self.mainW.RMS_SIPS, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SIPS'], 0, 'orange')
            self.adjustDetailsButton(self.mainW.btnPowerLimitExceeded, self.mainW.RMS_PowerLimitExceeded, self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['PowerLimitExceeded'], 0, 'orange')

            Unit_P = 0
            Boundary_EXC = 0
            Boundary_CHP = 0
            Boundary_WIND = 0

            for key, value in self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['Boundary'].items():
                if 'Udveksling til' in key:
                    Boundary_EXC = value['Pstop'] - value['Pstart']
                    Unit_P = value['Unit_P']
                    self.mainW.RMS_ACExchange.setText('{0:.2f}'.format(Boundary_EXC))
                    self.adjustDetailsButton(self.mainW.btnACExchange, self.mainW.RMS_ACExchange, abs(Boundary_EXC), Unit_P, 'red')
                elif 'Decentral produktion' in key:
                    Boundary_CHP = value['Pstop'] - value['Pstart']
                    self.mainW.RMS_TripOfCHP.setText('{0:.2f}'.format(Boundary_CHP))
                elif 'Landvind Gammel' in key:
                    Boundary_WIND = value['Pstop'] - value['Pstart']
                    self.mainW.RMS_TripOfWindOld.setText('{0:.2f}'.format(Boundary_WIND))

            if abs(Boundary_CHP) > Unit_P or abs(Boundary_CHP + Boundary_WIND) > Unit_P:
                self.mainW.btnTripOfCHP.setStyleSheet('font: bold 8pt; color: red')
                self.mainW.RMS_TripOfCHP.setStyleSheet('font: bold 8pt; color: red')
            if abs(Boundary_WIND) > Unit_P or abs(Boundary_CHP + Boundary_WIND) > Unit_P:
                self.mainW.btnTripOfWindOld.setStyleSheet('font: bold 8pt; color: red')
                self.mainW.RMS_TripOfWindOld.setStyleSheet('font: bold 8pt; color: red')
        else:
            self.mainW.logViewer.setPlainText('LogFile does not exists...')
            self.mainW.laLineNumber.setText('Line number: N/A')
            self.mainW.laLogFileSize.setText('Logfile size: N/A')

    def adjustOverviewButton(self, radio_button, detection, text, color):
        if detection > 0:
            radio_button.setText('{0} ({1} of {2})'.format(text, detection, self.LogFileNumber))
            radio_button.setStyleSheet('color: {0}'.format(color))
            radio_button.setEnabled(True)
        else:
            radio_button.setText(text)
            radio_button.setStyleSheet(None)
            radio_button.setEnabled(False)

    def adjustDetailsButton(self, push_button, line_edit, detection, criterion, color):
        if detection > criterion:
            if color is None:
                color = 'black'
            push_button.setStyleSheet('font: bold 8pt; color: {0}'.format(color))
            line_edit.setStyleSheet('font: bold 8pt; color: {0}'.format(color))

    def showTimestamp(self):
        if self.mainW.cbTimeStamp.isEnabled() and self.mainW.cbTimeStamp.isChecked():
            self.mainW.logViewer.setPlainText(self.raw_file)
        else:
            self.mainW.logViewer.setPlainText(self.raw_file_slim)

    def increaseIndex(self, combo_box, number):
        index = combo_box.currentIndex()
        if 0 <= index + number < combo_box.count():
            combo_box.setCurrentIndex(index + number)

    def updateCbDateMaster(self):
        if not self.BypassDate:
            self.mainW.lbDateMaster.setText('Point in time: {0} of {1}'.format(self.mainW.cbDateMaster.currentIndex()+1, len(self.MATRIX_FILES)))
            indexOld = self.mainW.cbFaultMaster.currentText()
            self.BypassFault = True
            self.mainW.cbFaultMaster.clear()
            self.mainW.cbFaultMaster.addItems(sorted(self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Faults']))
            if indexOld in self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Faults']:
                self.mainW.cbFaultMaster.setCurrentText(indexOld)
            else:
                self.mainW.cbFaultMaster.setCurrentIndex(0)
            self.BypassFault = False
            self.updateCbFaultMaster()

    def updateCbFaultMaster(self):
        if not self.BypassFault:
            self.mainW.lbFaultMaster.setText('Fault location: {0} of {1}'.format(self.mainW.cbFaultMaster.currentIndex()+1, len(self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Faults'])))
            indexOld = self.mainW.cbPlotMaster.currentText()
            self.BypassPlot = False
            if indexOld != 'N/A' or self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout']:
                self.BypassPlot = True
                self.mainW.cbPlotMaster.clear()
                if not self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout']:
                    self.mainW.cbPlotMaster.addItem('N/A')
                else:
                    self.mainW.cbPlotMaster.addItems(sorted(self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout']))
                    if indexOld in self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout']:
                        self.mainW.cbPlotMaster.setCurrentText(indexOld)
                    else:
                        self.mainW.cbPlotMaster.setCurrentIndex(0)
                self.BypassPlot = False
            self.updateCbPlotMaster()

    def updateCbPlotMaster(self):
        if not self.BypassPlot:
            if self.mainW.cbPlotMaster.currentText() == 'N/A':
                self.mainW.lbPlotMaster.setText('Plot: N/A')
            else:
                self.mainW.lbPlotMaster.setText('Plot: {0} of {1}'.format(self.mainW.cbPlotMaster.currentIndex()+1, len(self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout'])))
            self.updateCbDateSlave()

    def updateCbDateSlave(self):
        if not self.BypassDate:
            self.mainW.lbDateSlave.setText('Point in time: {0} of {1}'.format(self.mainW.cbDateSlave.currentIndex()+1, len(self.MATRIX_FILES)))
            indexOld = self.mainW.cbFaultSlave.currentText()
            if self.mainW.cbSynchronizeDate.isChecked():
                self.BypassDate = True
                self.mainW.cbDateSlave.setCurrentIndex(self.mainW.cbDateMaster.currentIndex())
                self.BypassDate = False
                indexNew = [*self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Faults']]
                for element in (self.mainW.cbDateSlave, self.mainW.btnPrevDateSlave, self.mainW.btnNextDateSlave):
                    element.setEnabled(False)
            else:
                indexNew = [*self.MATRIX_FILES[self.mainW.cbDateSlave.currentText()]['Faults']]
                for element in (self.mainW.cbDateSlave, self.mainW.btnPrevDateSlave, self.mainW.btnNextDateSlave):
                    element.setEnabled(True)

            self.BypassFault = True
            self.mainW.cbFaultSlave.clear()
            indexNew.sort()
            self.mainW.cbFaultSlave.addItems(indexNew)
            if indexOld in indexNew:
                self.mainW.cbFaultSlave.setCurrentText(indexOld)
            else:
                self.mainW.cbFaultSlave.setCurrentIndex(0)
            self.BypassFault = False
            self.updateCbFaultSlave()

    def updateCbFaultSlave(self):
        if not self.BypassFault:
            self.mainW.lbFaultSlave.setText('Fault location: {0} of {1}'.format(self.mainW.cbFaultSlave.currentIndex()+1, len(self.MATRIX_FILES[self.mainW.cbDateSlave.currentText()]['Faults'])))
            if self.mainW.cbSynchronizeFault.isChecked():
                indexOld = self.mainW.cbFaultMaster.currentText()
                if indexOld in self.MATRIX_FILES[self.mainW.cbDateSlave.currentText()]['Faults']:
                    self.mainW.cbFaultSlave.setCurrentText(indexOld)
                else:
                    self.mainW.cbFaultSlave.clear()
                    self.mainW.cbFaultSlave.addItem('N/A')
                    self.mainW.lbFaultSlave.setText('Fault location: N/A')
                for element in (self.mainW.cbFaultSlave, self.mainW.btnPrevFaultSlave, self.mainW.btnNextFaultSlave):
                    element.setEnabled(False)
            else:
                for element in (self.mainW.cbFaultSlave, self.mainW.btnPrevFaultSlave, self.mainW.btnNextFaultSlave):
                    element.setEnabled(True)

            if self.mainW.cbPlotMaster.currentText() != 'N/A' and self.mainW.cbFaultSlave.currentText() != 'N/A' and \
                    self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Faults'][self.mainW.cbFaultMaster.currentText()]['PlotFile']['Path'] is not None and \
                    self.MATRIX_FILES[self.mainW.cbDateSlave.currentText()]['Faults'][self.mainW.cbFaultSlave.currentText()]['PlotFile']['Path'] is not None and \
                    self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout'].get(self.mainW.cbPlotMaster.currentText()) is not None:
                self.updateComparePlot()
                for element in (self.mainW.frame_matplotlib, self.mainW.cbLimitXCompare, self.mainW.cbFreezeYCompare, self.mainW.cbRemoveLegendCompare):
                    element.setEnabled(True)
                xlim = self.mainW.cbLimitXCompare.isChecked()
                self.mainW.dSPXminCompare.setEnabled(xlim)
                self.mainW.dSPXmaxCompare.setEnabled(xlim)
            else:
                self.drawEmptyMatplot(self.figCompare)
                self.mainW.frame_matplotlib.setEnabled(False)
                for element in (self.mainW.cbLimitXCompare, self.mainW.dSPXminCompare, self.mainW.dSPXmaxCompare, self.mainW.cbFreezeYCompare, self.mainW.cbRemoveLegendCompare):
                    element.setEnabled(False)

    def updateComparePlot(self):

        CSV_HEADER, CSV_DATA = self.readPlotData(self.mainW.cbDateMaster.currentText(), self.mainW.cbFaultMaster.currentText(), self.mainW.cbPlotMaster.currentText())
        CSV_HEADER_SLAVE, CSV_DATA_SLAVE = self.readPlotData(self.mainW.cbDateSlave.currentText(), self.mainW.cbFaultSlave.currentText(), self.mainW.cbPlotMaster.currentText())

        XLIM = [CSV_DATA.iloc[0, 0], CSV_DATA.iloc[-1, 0]]
        EB_SBE_NET = self.mainW.cbDate.currentText()
        PLOT = self.mainW.cbPlotMaster.currentText()

        VP_mode, VP_data = self.checkVoltageProilfe(self.mainW.cbDateMaster.currentText(), self.mainW.cbFaultMaster.currentText(), self.mainW.cbPlotMaster.currentText(), XLIM)

        self.mainW.dSPXminCompare.setMinimum(XLIM[0])
        self.mainW.dSPXminCompare.setMaximum(XLIM[1] - 0.1)
        self.mainW.dSPXmaxCompare.setMinimum(XLIM[0] + 0.1)
        self.mainW.dSPXmaxCompare.setMaximum(XLIM[1])

        if self.mainW.cbLimitXCompare.isChecked():
            XLIM = [self.mainW.dSPXminCompare.value(), self.mainW.dSPXmaxCompare.value()]

        ROW, COL, ORDER = self.arrangeSubplots(len(self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout'][self.mainW.cbPlotMaster.currentText()]))

        XLABEL = self.xLabel(CSV_HEADER[0][1])

        for ax in self.figCompare.get_axes():
            self.figCompare.delaxes(ax)

        axes = self.figCompare.subplots(nrows=ROW, ncols=COL, sharex=True, subplot_kw=dict(xlim=XLIM))
        if not isinstance(axes, np.ndarray):
            axes = np.array(axes)

        for subplot, ax in zip(sorted(self.MATRIX_FILES[self.mainW.cbDateMaster.currentText()]['Layout'][self.mainW.cbPlotMaster.currentText()].items()), (axes.flat[idx] for idx in ORDER)):
            ax.set_title(subplot[0])
            ax.title.set_visible(False)
            ax.grid(True)
            ax.xaxis.set_tick_params(labelbottom=True)
            ax.set_xlabel(XLABEL, horizontalalignment='right', x=1.0)
            legend = []
            for lValues in subplot[1][0]:
                if len(lValues) == 3:
                    aElement, aVariable, iColor = lValues
                    bNorm = False
                else:
                    aElement, aVariable, iColor, bNorm, fNomVal = lValues
                for data_index, header in enumerate(CSV_HEADER):
                    if (aElement in header[0] and aVariable in header[1]) or \
                            (header[1] in ('b:dtgrd', 'b:ierrgrd', 'b:warnA', 'b:warnB', 'b:warnC') and aVariable == header[1]):
                        if bNorm:
                            ax.plot(CSV_DATA.iloc[:, 0], CSV_DATA.iloc[:, data_index]/fNomVal, self.MCOLORS.get(iColor, iColor if isinstance(iColor, str) else ''))
                            legend.append(' '.join([*header, '(base: {0:.3f})'.format(fNomVal)]).replace('$', ''))
                        else:
                            ax.plot(CSV_DATA.iloc[:, 0], CSV_DATA.iloc[:, data_index], self.MCOLORS.get(iColor, iColor if isinstance(iColor, str) else ''))
                            legend.append(' '.join(header).replace('$', ''))
                        break

                for data_index, header in enumerate(CSV_HEADER_SLAVE):
                    if (aElement in header[0] and aVariable in header[1]) or \
                            (header[1] in ('b:dtgrd', 'b:ierrgrd', 'b:warnA', 'b:warnB', 'b:warnC') and aVariable == header[1]):
                        if bNorm:
                            ax.plot(CSV_DATA_SLAVE.iloc[:, 0], CSV_DATA_SLAVE.iloc[:, data_index]/fNomVal, self.MCOLORS.get(iColor, iColor if isinstance(iColor, str) else ''), linestyle=self.mainW.cbLineStyle.currentText())
                            legend.append(' '.join([*header, '(base: {0:.3f})'.format(fNomVal)]).replace('$', ''))
                        else:
                            ax.plot(CSV_DATA_SLAVE.iloc[:, 0], CSV_DATA_SLAVE.iloc[:, data_index], self.MCOLORS.get(iColor, iColor if isinstance(iColor, str) else ''), linestyle=self.mainW.cbLineStyle.currentText())
                            legend.append(' '.join(header).replace('$', ''))
                        break

            if legend:
                self.drawConstant(ax, subplot[1][1], XLIM)
                self.createVoltageProfile(VP_mode, VP_data, EB_SBE_NET, PLOT, ax, legend)

                leg = ax.legend(legend, loc='upper left', bbox_to_anchor=(0.0, -0.1), frameon=0)
                leg.set_draggable(True)
                leg.set_visible(not self.mainW.cbRemoveLegendCompare.isChecked())

        if self.mainW.cbFreezeYCompare.isChecked():
            if len(ORDER) == len(self.YLIM[self.figCompare]):
                for idx, ax in enumerate(self.figCompare.get_axes()):
                    ax.set_ylim(self.YLIM[self.figCompare][idx])
            else:
                self.mainW.cbFreezeYCompare.setChecked(False)
        self.figCompare.canvas.draw()

    def arrangeSubplots(self, subplots):
        if subplots <= 3:
            ROW = subplots
            COL = 1
            ORDER = range(subplots)
        else:
            ROW = 2
            subplots += subplots % 2
            COL = subplots // 2
            ORDER = np.arange(subplots, dtype=np.int8).reshape(2, COL).T.flat
        return [ROW, COL, ORDER]

    def readPlotData(self, EB_SBE_NET, FEJL, PLOT):
        if self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Content'].get(PLOT) is None:
            columns = [idx for subplot in self.MATRIX_FILES[EB_SBE_NET]['Layout'][PLOT].items()
                       for lValues in subplot[1][0]
                       for idx, header in enumerate(self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['names'])
                       if (lValues[0] in header[0] and lValues[1] in header[1]) or (header[1] in ('b:dtgrd', 'b:ierrgrd', 'b:warnA', 'b:warnB', 'b:warnC') and header[1] == lValues[1])]
            with open(self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Path']) as fid:
                df = pd.read_csv(fid,
                                skiprows=self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['skiprows'],
                                names=self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['names'],
                                dtype=self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['dtype'],
                                usecols=[0] + columns,
                                memory_map=True,
                                encoding=self.PlotFileEncoding)
            if sys.getsizeof(df) / (1024 * 1024) <= self.BufferSizeLimit:
                self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Content'][PLOT] = df
            return [df.columns.values.tolist(), df]
        else:
            return [self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Content'][PLOT].columns.values.tolist(),
                    self.MATRIX_FILES[EB_SBE_NET]['Faults'][FEJL]['PlotFile']['Content'][PLOT]]

    def drawConstant(self, ax, subplot, XLIM):
        for aLabel, aAxis, fValue, iColor in subplot:
            if aAxis == 'Y':
                ax.axhline(fValue, linestyle='--', color='{0}'.format(self.MCOLORS.get(iColor, (iColor if isinstance(iColor, str) else 'navy'))))
                if '<' in aLabel[0]:
                    vertical_alignment = 'top'
                else:
                    vertical_alignment = 'bottom'
                an = ax.annotate(aLabel[0], xy=(XLIM[1], fValue), horizontalalignment='right', verticalalignment=vertical_alignment)
            else:
                ax.axvline(fValue, linestyle='--', color='{0}'.format(self.MCOLORS.get(iColor, (iColor if isinstance(iColor, str) else 'navy'))))
                an = ax.annotate(aLabel[0], xy=(fValue, ax.get_ylim()[1]), horizontalalignment='left', annotation_clip=False)
            an.draggable(True)

    def drawEmptyMatplot(self, fig):
        for ax in fig.get_axes():
            fig.delaxes(ax)
        ax = fig.add_subplot(111, title='No data available')
        ax.grid(True)
        fig.canvas.draw()

    def checkVoltageProilfe(self, EB_SBE_NET, FEJL, PLOT, XLIM):
        if 'voltages' in PLOT.lower():
            x_start = XLIM[0]
            x_stop = XLIM[1]
            if 'flatrun' in FEJL.lower() or self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['SimulationEvents'][0] == x_stop:
                VP_mode = 'FlatRun'
                sc_start = sc_stop = 0
            else:
                VP_mode = 'VoltageEnvolope'
                sc_start = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['StartShortCircuit']
                sc_stop = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ClearShortCircuit']
                if sc_start is None:
                    sc_start = 0.2
                if sc_stop is None:
                    sc_stop = sc_start + 0.1
                    if self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExpectedSimulationStop'] != 'N/A':
                        x_stop = self.MATRIX_RESULTS[EB_SBE_NET][FEJL]['ExpectedSimulationStop']
                    else:
                        x_stop = 12
                else:
                    if x_stop < sc_stop + 10:
                        x_stop = 12
            return VP_mode, [x_start, x_stop, sc_start, sc_stop]
        else:
            return None, []

    def createVoltageProfile(self, VP_mode, VP_data, EB_SBE_NET, PLOT, ax, legend):
        if VP_mode is not None and not [1 for element in legend if 'm:phiurel' in element]:
            for el1, el2 in [(el1, el2) for el1, el2 in zip(legend, ax.lines) if 'HV Boundary' in el1 or 'LV Boundary' in el1]:
                legend.remove(el1)
                ax.lines.remove(el2)

            x_start, x_stop, sc_start, sc_stop = VP_data

            if VP_mode == 'FlatRun':
                HV_X = LV_X = [x_start, x_stop]
                HV_Y = [1.05, 1.05]
                LV_Y = [0.9, 0.9]
                legend.append('FlatRun - Upper voltage limit')
                legend.append('FlatRun - Lower voltage limit')
            else:
                HV_X = [x_start, *[sc_stop + element for element in (0, 0, 0.1, 0.1, 5.1, 5.1, 10.1, 10.1)], x_stop]
                HV_Y = [1.05, 1.05, 1.3, 1.3, 1.2, 1.2, 1.07, 1.07, 1.05, 1.05]
                if any('Transmission Node - LV Boundary' in el for el in self.MATRIX_FILES[EB_SBE_NET]['Layout'][PLOT][ax.get_title()][0]):
                    LV_X = [x_start, sc_start, sc_start, sc_stop, sc_stop, sc_stop + 1, sc_stop + 10, sc_stop + 10, x_stop]
                    LV_Y = [0.9, 0.9, 0, 0, 0.25, 0.75, 0.75, 0.9, 0.9]
                    legend.append('Transmission Node - Upper voltage limit')
                    legend.append('Transmission Node - Lower voltage limit')
                else:
                    LV_X = [x_start, sc_start, sc_start, sc_stop, sc_stop, sc_stop + 0.6, sc_stop + 1.4, sc_stop + 4.9, sc_stop + 4.9, x_stop]
                    LV_Y = [0.9, 0.9, 0, 0, 0.6, 0.6, 0.7, 0.7, 0.9, 0.9]
                    legend.append('PV Node - Upper voltage limit')
                    legend.append('PV Node - Lower voltage limit')

            ax.plot(HV_X, HV_Y, LV_X, LV_Y, color='black')

    def xLabel(self, header):
        return 'Time [{0}]'.format(header.split('b:tnow in ')[1]) if 'b:tnow in ' in header else ''

    def freezeYAxis(self, fig, redraw=False):
        if self.sender().isChecked():
            self.YLIM[fig] = [ax.get_ylim() for ax in fig.get_axes()]
        else:
            for ax in fig.get_axes():
                if not redraw:
                    YLIM = ax.get_ylim()
                ax.autoscale(axis='y')
                if not redraw and YLIM != ax.get_ylim():
                    redraw = True
            if redraw:
                fig.canvas.draw()

    def limitXAxis(self, fig, checkbox, vmin, vmax, redraw=False):
        if checkbox.isChecked():
            XMAX = vmax.value()
            if XMAX <= vmin.value():
                vmin.setValue(XMAX - 1)
            XMIN = vmin.value()
        else:
            XMIN = vmin.minimum()
            XMAX = vmax.maximum()

        for ax in fig.get_axes():
            if (XMIN, XMAX) != ax.get_xlim():
                ax.set_xlim(XMIN, XMAX)
                redraw = True

        vmin.setEnabled(checkbox.isChecked())
        vmax.setEnabled(checkbox.isChecked())

        if redraw:
            fig.canvas.draw()

    def showLegend(self, fig):
        for legend in (ax.get_legend() for ax in fig.get_axes() if ax.get_legend()):
            legend.set_visible(not legend.get_visible())
        fig.canvas.draw()

    def saveOverview(self):
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self.mainW, 'Save overview to Excel file', self.root, 'Excel file (*.xlsx)')
        if fileName:
            radioButtonDefault = [element for element in self.overviewButtons if element.isChecked()]
            if self.mainW.cbSaveActualView.isChecked():
                overviewButtons = radioButtonDefault
            else:
                overviewButtons = [element for element in self.overviewButtons if element.isEnabled()]

            with xlsxwriter.Workbook(fileName) as workbook:
                header_format = workbook.add_format({'bg_color': 'blue', 'border': 1, 'border_color': 'white', 'font_color': 'white', 'bold': True})
                cell_green_format = workbook.add_format({'align': 'center', 'bg_color': '#00FF00', 'border': 1, 'border_color': 'white'})
                cell_red_format = workbook.add_format({'align': 'center', 'bg_color': '#FF0000', 'border': 1, 'border_color': 'white', 'font_color': 'white', 'bold': True})
                cell_orange_format = workbook.add_format({'align': 'center', 'bg_color': '#FFAA00', 'border': 1, 'border_color': 'white', 'font_color': 'white', 'bold': True})
                cell_gray_format = workbook.add_format({'align': 'center', 'bg_color': '#969696', 'border': 1, 'border_color': 'white', 'font_color': 'white', 'bold': True})
                cell_green_invis_format = workbook.add_format({'align': 'center', 'bg_color': '#00FF00', 'border': 1, 'border_color': 'white', 'font_color': '#00FF00', 'bold': True})
                cell_red_invis_format = workbook.add_format({'align': 'center', 'bg_color': '#FF0000', 'border': 1, 'border_color': 'white', 'font_color': '#FF0000', 'bold': True})
                cell_orange_invis_format = workbook.add_format({'align': 'center', 'bg_color': '#FFAA00', 'border': 1, 'border_color': 'white', 'font_color': '#FFAA00', 'bold': True})
                cell_gray_invis_format = workbook.add_format({'align': 'center', 'bg_color': '#969696', 'border': 1, 'border_color': 'white', 'font_color': '#969696', 'bold': True})

                for element in overviewButtons:
                    bBoundary = 'Boundary' in element.text()
                    element.click()
                    worksheet = workbook.add_worksheet(element.text()[:30].replace('[', '(').replace(']', ')').replace('/', '-'))
                    if not bBoundary:
                        worksheet.autofilter(0, 0, 0, self.mainW.tableWidget.columnCount())
                        worksheet.freeze_panes(1, 1)
                        worksheet.write_row(0, 1, [self.mainW.tableWidget.horizontalHeaderItem(idx).text()
                                            for idx in range(self.mainW.tableWidget.columnCount())], header_format)
                        for column in range(self.mainW.tableWidget.columnCount()):
                            for row in range(self.mainW.tableWidget.rowCount()):
                                if column == 0:
                                    worksheet.write(row+1, 0, self.mainW.tableWidget.verticalHeaderItem(row).text(), header_format)
                                cell = self.mainW.tableWidget.item(row, column)
                                if cell.background() == QtGui.QColor(255, 0, 0, 255):
                                    if cell.text() == '':
                                        cell_traffic_light = "red"
                                        cell_format = cell_red_invis_format
                                    else:
                                        cell_format = cell_red_format
                                elif cell.background() == QtGui.QColor(255, 170, 0, 255):
                                    if cell.text() == '':
                                        cell_traffic_light = "orange"
                                        cell_format = cell_orange_invis_format
                                    else:
                                        cell_format = cell_orange_format
                                elif cell.background() == QtGui.QColor(0, 255, 0, 255):
                                    if cell.text() == '':
                                        cell_traffic_light = "green"
                                        cell_format = cell_green_invis_format
                                    else:
                                        cell_format = cell_green_format
                                else:
                                    if cell.text() == '':
                                        cell_traffic_light = "gray"
                                        cell_format = cell_gray_invis_format
                                    else:
                                        cell_format = cell_gray_format
                                try:
                                    worksheet.write_number(row+1, column+1, float(cell.text()), cell_format)
                                except Exception:
                                    if cell.text() == '':
                                        worksheet.write(row+1, column+1, cell_traffic_light, cell_format)
                                    else:
                                        worksheet.write(row+1, column+1, cell.text(), cell_format)
                    else:
                        row_index = 0
                        indexOld = self.mainW.cbBoundary.currentText()
                        for boundary in range(self.mainW.cbBoundary.count()):
                            self.mainW.cbBoundary.setCurrentIndex(boundary)
                            worksheet.write(row_index, 0, self.mainW.cbBoundary.currentText(), header_format)
                            worksheet.write_row(row_index+1, 1, [self.mainW.tableWidget.horizontalHeaderItem(idx).text()
                                                                 for idx in range(self.mainW.tableWidget.columnCount())], header_format)
                            for column in range(self.mainW.tableWidget.columnCount()):
                                for row in range(self.mainW.tableWidget.rowCount()):
                                    if column == 0:
                                        worksheet.write(row_index+2+row, 0, self.mainW.tableWidget.verticalHeaderItem(row).text(), header_format)
                                    cell = self.mainW.tableWidget.item(row, column)
                                    if cell.background() == QtGui.QColor(255, 0, 0, 255):
                                        cell_format = cell_red_format
                                    elif cell.background() == QtGui.QColor(255, 170, 0, 255):
                                        cell_format = cell_orange_format
                                    elif cell.background() == QtGui.QColor(0, 255, 0, 255):
                                        cell_format = cell_green_format
                                    else:
                                        cell_format = cell_gray_format
                                    try:
                                        worksheet.write_number(row_index+2+row, column+1, float(cell.text()), cell_format)
                                    except Exception:
                                        worksheet.write(row_index+2+row, column+1, cell.text(), cell_format)
                            row_index += row + 4
                        self.mainW.cbBoundary.setCurrentText(indexOld)
                if not self.mainW.cbSaveActualView.isChecked():
                    radioButtonDefault[0].click()

    def exportPlots(self):
        ExportFolder = QtWidgets.QFileDialog.getExistingDirectory(self.mainW, 'Choose Directory', self.root)
        if os.path.isdir(ExportFolder):
            self.ExportFolder = os.path.abspath(ExportFolder)
            EXTENSION = self.mainW.cbPlotExtension.currentText().split('*.')[1]
            PlotFileList = []
            if self.mainW.cbAllTimes.isChecked():
                EB_SBE_NET = [*self.MATRIX_FILES]
            else:
                EB_SBE_NET = [self.mainW.cbDate.currentText()]
            for eb_sbe_net in EB_SBE_NET:
                if self.mainW.cbAllLocations.isChecked():
                    FEJL = [*self.MATRIX_FILES[eb_sbe_net]['Faults']]
                else:
                    FEJL = [self.mainW.cbFault.currentText()]
                for fejl in FEJL:
                    if self.mainW.cbAllPlots.isChecked():
                        PLOT = [*self.MATRIX_FILES[eb_sbe_net]['Layout']]
                    else:
                        PLOT = [self.mainW.cbPlot.currentText()]
                    for plot in PLOT:
                        PlotFileList.append((eb_sbe_net, fejl, plot,
                                            os.path.join(self.ExportFolder, '{0}-{1}-{2}.{3}'.format(eb_sbe_net, fejl, plot, EXTENSION))))
            PlotFileNumber = len(PlotFileList)

            DPI = 100
            RESOLUTION = [element/DPI for element in self.GraphicsDisplayResolutions[self.mainW.cb_GraphicsDisplayResolution.currentText().split('(')[0].rstrip()]]

            figEmpty = plt.figure(figsize=RESOLUTION, dpi=DPI, tight_layout=True)
            fig = plt.figure(figsize=RESOLUTION, dpi=DPI, tight_layout=True)

            self.drawEmptyMatplot(figEmpty)

            self.progress.setMaximum(PlotFileNumber)

            for idx, (EB_SBE_NET, FEJL, PLOT, filename) in enumerate(PlotFileList, start=1):
                self.progress.setLabelText('Exporting: {0} \\ {1}\n{2}'.format(idx, PlotFileNumber, filename))
                self.progress.setValue(idx)
                if self.progress.wasCanceled():
                    return
                if self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Faults'][self.mainW.cbFault.currentText()]['PlotFile']['Path'] is not None and \
                   self.MATRIX_FILES[self.mainW.cbDate.currentText()]['Layout'][self.mainW.cbPlot.currentText()] is not None:
                    self.updatePlot(fig, EB_SBE_NET, FEJL, PLOT)
                    fig.savefig(filename)
                else:
                    figEmpty.savefig(filename)
            self.mainW.btnShowPlot.setEnabled(True)
            plt.close(fig)
            plt.close(figEmpty)

    def computationStats(self):
        red = [FEJL['SimulationDuration_hist'] for EB_SBE_NET in self.MATRIX_RESULTS
               for FEJL in self.MATRIX_RESULTS[EB_SBE_NET].values()
               if 'SimulationDuration_hist' in FEJL and FEJL['TrafficLight'] == 1]

        orange = [FEJL['SimulationDuration_hist'] for EB_SBE_NET in self.MATRIX_RESULTS
                  for FEJL in self.MATRIX_RESULTS[EB_SBE_NET].values()
                  if 'SimulationDuration_hist' in FEJL and FEJL['TrafficLight'] == -1]

        green = [FEJL['SimulationDuration_hist'] for EB_SBE_NET in self.MATRIX_RESULTS
                 for FEJL in self.MATRIX_RESULTS[EB_SBE_NET].values()
                 if 'SimulationDuration_hist' in FEJL and FEJL['TrafficLight'] == 0]

        n_bins = sum(len(element) for element in (red, orange, green))

        fig = plt.figure(self.__name__, tight_layout=True)

        plt.hist([red, orange, green], color=['red', 'orange', 'green'], bins=round(n_bins/2), stacked=True)

        plt.xlabel('Computation time [minutes]')
        plt.ylabel('Number of simulations')
        plt.title('Histogram of computation time for {0} simulations'.format(n_bins))
        plt.xlim([0, max(red + orange + green)+10])
        plt.grid(True)
        fig.show()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    RMSReport(None)
    sys.exit(app.exec_())
