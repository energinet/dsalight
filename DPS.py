# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-3.0-only

import os
import csv
import glob
import time
import zipfile
import subprocess
import xml.etree.ElementTree as ET

from PyQt5 import QtWidgets, QtCore


class DPS():
    def __init__(self, app, Da2Prog, DPS_input_file, mapping_file, output_folder, output_window, progress_bar):
        super().__init__()

        self.app = app
        self.Da2Prog = Da2Prog
        self.DPS_input_file = os.path.abspath(DPS_input_file)
        self.mapping_file = mapping_file
        self.output_folder = output_folder
        self.progress_bar = progress_bar
        self.output_window = output_window
        self.oStudyCase = self.app.GetActiveStudyCase()
        self.CFGTP, self.GekkoXMLfile = self.DPS()

    def updateGUI(self, message, progress):
        if message is not None:
            item = QtWidgets.QListWidgetItem(message)
            if 'ERROR' in message:
                item.setForeground(QtCore.Qt.red)
            elif 'WARN' in message:
                item.setForeground(QtCore.Qt.darkYellow)
            self.output_window.addItem(item)
            self.output_window.scrollToBottom()
        if progress is not None:
            self.progress_bar.setValue(progress)
        QtWidgets.QApplication.processEvents()

    def importDPSmapping(self, DPSmappingPath, iErr=False):
        dictDPSmap = {}
        try:
            with open(DPSmappingPath, encoding='utf-8-sig') as csvfile:
                DPSmappingCont = csv.reader(csvfile, delimiter=';')
                for idx, row in enumerate(DPSmappingCont, start=1):
                    if idx >= 2:
                        try:
                            TimeSeriesName = row[0]
                            Column = int(float(row[1]))
                            PfForeignKey = row[2]
                            PfParam = row[4]
                            PfScaling = row[5]
                            if PfForeignKey == '':
                                self.updateGUI('WARN: No foreign key defined in line {0} of file {1}'.format(idx, DPSmappingPath), None)
                            dictFK = {idx: [TimeSeriesName, Column, PfParam, PfScaling, PfForeignKey]}
                            dictDPSmap.update(dictFK)
                        except Exception:
                            self.updateGUI('WARN: Failed to read line {0} of file {1}'.format(idx, DPSmappingPath), None)
        except IOError:
            iErr = True
            self.updateGUI('ERROR: Could not read file: {0}'.format(DPSmappingPath), 100)
        return [dictDPSmap, iErr]

    def deleteExistingChars(self):
        for oChaTime in self.app.GetProjectFolder('chars').GetContents('DPS_*.ChaTime', 1):
            for oChaRef in oChaTime.GetReferences('*.ChaRef'):
                if oChaRef.Delete():
                    self.updateGUI('WARN: Reference "{0}" of characteristic "{1}" could not be deleted!'.format(oChaRef.GetAttribute('e:loc_name'), oChaTime.GetAttribute('e:loc_name')), None)
            if oChaTime.Delete():
                self.updateGUI('WARN: Characteristic "{0}" could not be deleted!'.format(oChaTime.GetAttribute('e:loc_name')), None)
        self.oStudyCase.Deactivate()
        self.oStudyCase.Activate()

    def createCharAndRef(self, dictDPSmap, CharFilePath):
        oChar = self.app.GetProjectFolder('chars')
        self.updateGUI('Importing DPS data into PowerFactory', 0)
        totalNumber = len(dictDPSmap)
        for idx, line in enumerate(dictDPSmap, start=1):
            ForeignKey = dictDPSmap[line][4]
            self.updateGUI(None, idx/totalNumber*100)
            try:
                oElm = self.app.SearchObjectByForeignKey(ForeignKey)
            except Exception:
                oElm = self.app.SearchObjectByForeignKey(ForeignKey.encode('latin-1').decode('utf-8'))
            if oElm is not None:
                TimeSeries = dictDPSmap[line][0].replace('Consumption', 'Cons').replace(' (outserv)', '')
                CharName = 'DPS_{0}_{1}_{2}'.format(TimeSeries, dictDPSmap[line][2], dictDPSmap[line][3])
                FoundChar = oChar.GetContents('{0}.ChaTime'.format(CharName))
                if not FoundChar:
                    DPSChar = oChar.CreateObject('ChaTime', CharName)
                else:
                    DPSChar = FoundChar[0]
                usageStr = dictDPSmap[line][3]
                if usageStr == 'perunit':
                    usage = 1
                elif usageStr == 'absolute':
                    usage = 2
                elif usageStr == 'percent':
                    usage = 0
                else:
                    usage = 1
                    self.updateGUI('WARN: Unknown "usage" for characteristic (defined in line "{0}" of DSA mapping file)'.format(line), None)
                DPSChar.SetAttribute('e:source', 1)
                DPSChar.SetAttribute('e:iopt_stamp', 1)
                DPSChar.SetAttribute('e:timeformat', 'YYYY-MM-DD hh:mm')
                DPSChar.SetAttribute('e:f_name', CharFilePath)
                DPSChar.SetAttribute('e:iopt_file', 0)
                DPSChar.SetAttribute('e:iopt_sep', 0)
                DPSChar.SetAttribute('e:col_Sep', ';')
                DPSChar.SetAttribute('e:dec_Sep', '.')
                DPSChar.SetAttribute('e:timecol', 1)
                DPSChar.SetAttribute('e:datacol', dictDPSmap[line][1])
                DPSChar.SetAttribute('e:usage', usage)
                DPSChar.SetAttribute('e:approx', 0)
                if oElm.HasAttribute(dictDPSmap[line][2]):
                    oChaRef = oElm.CreateObject('ChaRef', dictDPSmap[line][2])
                    if oChaRef is not None:
                        oChaRef.SetAttribute('e:typ_id', DPSChar)
                    else:
                        self.updateGUI('WARN: Creation of *.ChaRef object for element "{0}" failed!'.format(oElm.GetAttribute('e:loc_name')), None)
                else:
                    self.updateGUI('WARN: Element "{0}" has not attribute "{1}"'.format(oElm.GetAttribute('e:loc_name'), dictDPSmap[line][2]), None)
            else:
                self.updateGUI('WARN: No element with foreign key "{0}" has been found.'.format(ForeignKey), None)

    def readCFGTPFile(self, CFGTP=None, GekkoXMLfile=None):
        currPath = os.path.dirname(self.DPS_input_file)
        try:
            SCADASimReq_File = glob.glob(os.path.join(currPath, 'SCADA SimReq*.zip'))[0]
        except Exception:
            self.updateGUI('ERROR: No file name inclunding "{0}" found in this path "{1}"'.format(SCADASimReq_File, currPath), 100)
        else:
            with zipfile.ZipFile(SCADASimReq_File) as zf:
                CFGTP = zf.read('CFGTP.csv').decode().splitlines()[-1]
                GekkoXMLfile = zf.extract('GekkoOutage.xml', self.output_folder)
        return [CFGTP, GekkoXMLfile]

    def runDa2Prog(self, iErr=False):
        netsysraport = os.path.join(self.output_folder, 'DPS_Netsysrapport.txt')
        oComDpl = self.app.GetProjectFolder('script').GetContents('A04 Generelle værktøjer')[0].GetContents('Netsysrapport*.ComDpl')[0]
        oComDpl.SetInputParameterString('aFile', netsysraport)
        oComDpl.SetInputParameterInt('iMode', 1)
        self.updateGUI('Saving Netsysrapport to: {0}'.format(netsysraport), 15)
        if oComDpl.Execute() == 0:
            self.updateGUI('Calling Da2Prog in order to import DPS data', 30)
            arg = '"{0}" dps "{1}" "{2}" "{3}" "{4}"'.format(self.Da2Prog, netsysraport, self.mapping_file, self.DPS_input_file, self.output_folder)
            CREATE_NO_WINDOW = 0x08000000
            try:
                subprocess.check_output(arg, creationflags=CREATE_NO_WINDOW)
            except subprocess.CalledProcessError as e:
                if e.returncode != 0:
                    iErr = True
                    self.updateGUI('ERROR: Da2Prog failed! Please check the Da2Prog log file located: {0}'.format(self.output_folder), 100)
        else:
            iErr = True
            self.updateGUI('ERROR: Netsysrapport failed!', 100)
        return iErr

    def DPS(self, CFGTP=None, GekkoXMLfile=None):
        ns = {'Unit': 'http://Energinet.Integration.Common.Schemas.InhouseXml.ScadaSimulationRequest'}
        if not ET.parse(self.DPS_input_file).getroot().findall('Unit:Unit', ns):
            self.updateGUI('ERROR: Invalid DPS input file - no instances of class "Unit" is found!', 100)
        else:
            DPSmappingPath = os.path.join(self.output_folder, 'DPSmap.csv')
            CharFilePath = os.path.join(self.output_folder, 'DPSvec.csv')
            if not self.runDa2Prog():
                dictDPSmap, iErr = self.importDPSmapping(DPSmappingPath)
                try:
                    os.remove(DPSmappingPath)
                except OSError:
                    pass
                if not iErr:
                    CFGTP, GekkoXMLfile = self.readCFGTPFile()
                    if any(CFGTP):
                        SCtime = int(time.mktime(time.strptime(CFGTP.split(',')[1], '%d-%m-%Y %H:%M:%S')))
                        oIntScheme = self.app.GetProjectFolder('scheme').CreateObject('IntScheme', 'DPS_Variation')
                        oIntScheme.NewStage('DPS_Stage', SCtime-60, 1)
                        self.app.SetWriteCacheEnabled(1)
                        self.deleteExistingChars()
                        self.createCharAndRef(dictDPSmap, CharFilePath)
                        self.app.WriteChangesToDb()
                        self.app.SetWriteCacheEnabled(0)
        return [CFGTP, GekkoXMLfile]
