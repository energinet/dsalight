# -*- coding: utf-8 -*-
"""
Updated on Fri May  3 14:47:58 2016

@author: kok
"""
import os
import sys
import time as timemodul
try:
    os.environ["PATH"] = r'C:\\Program Files\\DIgSILENT\\PowerFactory 2017 SP6\\Python\\3.5\\'+ os.environ["PATH"]
    sys.path.append(r"C:\\Program Files\\DIgSILENT\\PowerFactory 2017 SP6\\Python\\3.5\\")
except:
    os.environ["PATH"] = r'C:\\Program Files\\DIgSILENT\\PowerFactory 2017 SP6\\Python\\3.4\\'+ os.environ["PATH"]
    sys.path.append(r"C:\\Program Files\\DIgSILENT\\PowerFactory 2017 SP6\\Python\\3.4\\")

class SetupBalances():
    '''
    author: KOK
    Description:
    Script can be used to import and setup balances in the ELvis model.
    The script can import many n-years and for both grids in one executation

        | Input parameters | ex.              | info
        |    grids         | str(1,2)         | 1 for DK1, 2 for DK2 or 1,2 for both grids
        |    years         | str(2018, 2020)  | YYYY for single year, or YYYY, YYYY for multiple years
        |    type          | int(i)           | 0 = Sifre, 1 = Planlægningsbalancer
        |    PathFolder    | str(path)        | Path to the Folder where Sifrebalances are located!
        |    printinfo     | int(i)           | print detailed info = 1 else 0 for nothing

    Main() function directs the flow in the script:
        1. Input for PowerFactory
            Netplan-> NetplanBalances_OpenExcelSheet()
            Sifre 	-> SifreBalances_GetDataFromPathFolder()
        2. Setup Library and Characteristics
            All type of Balances -> SetupLibraryAndCharacteristics()
        3. Find Objects for Characteristics
            Netplan-> NetplanBalances_FindObjectsAndScale()
            Sifre	-> SifreBalances_FindObjects()
        4. Setup of variations
            All type of balance -> SetupVariationAndExpansionStage()
        In end for all types of balances -> CleanUpStandardBrugerExpansionStage()

             ! SIFRE REQUIREMENTS !
         Sifrebalances NEED to be named correctly, with indicators ("Sifre", "Grid", and "YEAR").
         Example:   Sifre_DK1_2018.csv
    '''
    def __init__(self, *arg):
        Main = SetupBalances.Main(*arg)
        super(Main, self).__init__(None)

### MAIN
    def Main(*arg):
        '''
        author: KOK
        Description:
        Only purpose of main is to translate the PowerFactory-Input from the user.
        '''
        ts = timemodul.time()
        app = arg[0]
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError
        Str_grids, Str_year, FolderName, PathFolder, printinfo, p_balancer = '', '', '', '', '', ''
        if len(arg) > 1:
            prtp('SetupBalances - EngineMode')
            Str_grids, Str_year, PathFolder, FolderName, printinfo, Mode = '', '', '', 1, 0, 'EngineMode'
        else:
            prtp('SetupBalances - ELvis')
            Script = app.GetCurrentScript()
            Str_grids, Str_year, FolderName, PathFolder, printinfo, p_balancer, Mode = Script.grids, Script.years, Script.type, Script.PathFolder, Script.printinfo, [None], 'PF'

        if FolderName == 1 and PathFolder == '': # FolderName = 1 = Planlægningsbalancer. Path to Excel-sheet will be found.
            p_balancer = SetupBalances.NetplanBalances_GetActiveSheet(app) # returns PathFolder, FileName, Year, Grid, StartAtTime
            PathFolder, FullPathToFile = p_balancer[0], p_balancer[1]
        if Str_grids == '' and Str_year == '':
            Str_grids, Str_year = str(p_balancer[3]), str(p_balancer[2])
        if FolderName  != 1 and not ':' in PathFolder:
            prte('Error! You are trying to import Sifre/Bid Balances without any path! Please configure path to Sifre/Bid files.\nRemember files should be named "Sifre_DKX_YYYY.csv"')
            exit()
        years, grids, StringToList = [], [], [Str_grids, Str_year]
        try:
            for k in range(len(StringToList)):
                if ',' in StringToList[k]:
                    [grids.append(int(drop)) for drop in StringToList[k].split(',') if k == 0]
                    [years.append(int(drop)) for drop in StringToList[k].split(',') if k == 1]
                else:
                    if k == 0: grids = [int(Str_grids)]
                    if k == 1: years = [int(Str_year)]
        except:
            prte('Error X83S2. Input parameters!')
            exit()
        [prti(x) for x in ['\nInput parameters\n-------------------', 'years: %s' %years,'grids: %s' %grids, 'type: %s' %FolderName, 'PathFolder: %s' %PathFolder, 'end\n-------------------'] if printinfo is 1]

        if [oCase.Activate() for oCase in app.GetProjectFolder('study').GetContents('*.IntCase') if oCase.loc_name == 'DK Fokuseret reference'] == []:
            prte('Error KJ39. "DK Fokuseret Reference" was not found. Program exiting.')
            exit()
            
# STEP 1--------------------------------------------------------------------------------------------------                   
        for grid in grids:
            for year in years:
                time = app.GetActiveStudyCase().GetContents('*.SetTime')[0]
                time.cDate = 19700101
                prtp('\n1. Input for PowerFactory\n-------------------')
                timestep1 = timemodul.time()
                if FolderName != 1: # Sifre/BID etc.
                    data = SetupBalances.SifreBalances_GetDataFromPathFolder(app, PathFolder, str(year), grid, FolderName, printinfo)
                else: # Planlægningsbalancer
                    app.SetWriteCacheEnabled(1)
                    data = SetupBalances.NetplanBalances_OpenExcelSheet(app, PathFolder, FullPathToFile, year, 'Scenarier_'+str(year), printinfo, grid)
                    app.WriteChangesToDb()
                    app.SetWriteCacheEnabled(0)
                m, s = divmod((timemodul.time()-timestep1), 60)
                h, m = divmod(m, 60)
                prtp('Timer for step 1: %d:%02d:%02d '% (h, m, s))
                if printinfo==1: prtp('End -------------------\n')
                
                # --------------
                global Debugger
                Debugger = 0 
                if Debugger == 1: 
                    app.PrintPlain('# --------------------------------------------------------------------------------------------------')                    
                    app.PrintPlain('Output from STEP 1: NetplanBalances_OpenExcelSheet')
                    app.PrintPlain('Length: %d' %len(data))
                    for i in range(len(data)): 
                        app.PrintPlain('.....nr: %d' %i)                        
                        if type(data[i]) == str:
                            app.PrintPlain(data[i])
                        else: 
                            for k in data[i]: 
                                app.PrintPlain('Length: %d, %s'%(len(k),k))
                    for i in range(len(data[2][0])):
                        app.PrintPlain('')
                        app.PrintPlain(data[2][0][i])
                        app.PrintPlain(data[2][1][i])
                        app.PrintPlain(data[2][2][i])
                        app.PrintPlain(data[2][3][i])
                        app.PrintPlain(data[2][4][i])
                    app.PrintPlain('# --------------------------------------------------------------------------------------------------')      
# STEP 2--------------------------------------------------------------------------------------------------                   

                prtp('\n2. Setup Library and Characteristics\n-------------------')
                timestep2 = timemodul.time()
                if not p_balancer[len(p_balancer)-1] == None:
                    GetSpec = SetupBalances.NetplanBalances_SpecificStartHour(app, p_balancer[len(p_balancer)-1], data)
                    SpecificHourStart, AmountOfBalance, data[2][2] = GetSpec[1], GetSpec[2], GetSpec[0]
                else:
                    SpecificHourStart, AmountOfBalance = 1, len(data[2][2][0])
                VecData = SetupBalances.SetupLibraryAndCharacteristics(app, FolderName, str(grid), str(year), data, printinfo, SpecificHourStart, AmountOfBalance, Mode)
                m, s = divmod((timemodul.time()-timestep2), 60)
                h, m = divmod(m, 60)
                prtp('Timer for step 2: %d:%02d:%02d '% (h, m, s))
                if printinfo==1: prtp('End -------------------\n')
                # --------------
                if Debugger == 1: 
                    app.PrintPlain('# --------------------------------------------------------------------------------------------------')                                                                        
                    app.PrintPlain('Output from STEP 2: SetupLibraryAndCharacteristics ("VecData")')
                    app.PrintPlain('Length: %d' %len(VecData))
                    for i in range(len(VecData)): 
                        app.PrintPlain('.....nr: %d' %i)                        
                        if type(VecData[i]) == str or type(VecData) == object:
                            app.PrintPlain(VecData[i])
                        else: 
                            for k in range(len(VecData[i])):                                   
                                app.PrintPlain(VecData[i][k])

#                for i in range(len(VecData[0][0])):
#                    app.PrintPlain('')
#                    app.PrintPlain(VecData[0][0][i])
#                    app.PrintPlain(VecData[0][1][i])
#                    app.PrintPlain(VecData[1][0][i])
#                    app.PrintPlain(VecData[1][1][i])
#                                
#                    app.PrintPlain('# --------------------------------------------------------------------------------------------------')     
                                                                                             
# STEP 3--------------------------------------------------------------------------------------------------   


                prtp('\n3. Find Objects for Characteristics\n-------------------')
                SetupBalances.doTime(app, year, 1, 1, '0', '0', '0') # VIGTIGT
                timestep3 = timemodul.time()
                if FolderName != 1: # Sifre/BID etc.
                    Data2 = SetupBalances.SifreBalances_FindObjects(app, year, VecData, grid, printinfo, FolderName)
                elif FolderName == 1: # Planlægningsbalancer
                    app.SetWriteCacheEnabled(1)
                    Data2 = SetupBalances.NetplanBalances_FindObjectsAndScale(app, year, VecData, grid, printinfo, FolderName, SpecificHourStart, AmountOfBalance)
                    app.WriteChangesToDb()
                    app.SetWriteCacheEnabled(0)
                m, s = divmod((timemodul.time()-timestep3), 60)
                h, m = divmod(m, 60)
                prtp('Timer for step 3: %d:%02d:%02d '% (h, m, s))
                if printinfo==1: prtp('End -------------------\n')
                # --------------
                if Debugger == 1: 
                    app.PrintPlain('# --------------------------------------------------------------------------------------------------')                                                                        
                    app.PrintPlain('Output from STEP 3: NetplanBalances_FindObjectsAndScale')
                    app.PrintPlain('Length: %d' %len(Data2))
                    for i in range(len(VecData)): 
                        app.PrintPlain('.....nr: %d' %i)                        
                        if type(Data2[i]) == str or type(Data2) == object:
                            app.PrintPlain(Data2[i])
                        else: 
                            for k in range(len(Data2[i])):                                   
                                app.PrintPlain(Data2[i][k])
                    app.PrintPlain('# --------------------------------------------------------------------------------------------------')     
                                                                                                
# STEP 4-------------------------------------------------------------------------------------------------- 


                prtp('\n4. Setup of variations\n-------------------')
                timestep4 = timemodul.time()
                SetupBalances.SetupVariationAndExpansionStage(app, grid, year, FolderName, data, Data2, printinfo)
                if printinfo==1: prtp('End -------------------\n')
                m, s = divmod((timemodul.time()-timestep4), 60)
                h, m = divmod(m, 60)
                prtp('Timer for step 4: %d:%02d:%02d '% (h, m, s))

                timeClearBruger = timemodul.time()
                SetupBalances.CleanUpStandardBrugerExpansionStage(app, year, printinfo)
                m, s = divmod((timemodul.time()-timeClearBruger), 60)
                h, m = divmod(m, 60)
                prtp('Timer for step clearing bruger variation: %d:%02d:%02d '% (h, m, s))
                if printinfo==1: prtp('End -------------------\n')
        prtp('\nProcess complete...')
        m, s = divmod((timemodul.time()-ts), 60)
        h, m = divmod(m, 60)
        prtp('Total time: %d:%02d:%02d '% (h, m, s))
        if printinfo==1: prtp('End -------------------\n')
        return
# -------------------------------------------------------------------------------------------------- 

### NETPLANLÆGNINGSBALANCER (Only used for PB)
    def NetplanBalances_SpecificStartHour(app, SpecificHourStart, idata):
        '''
        author: KOK
        Description:
        The function re-arranges the balance vector, so that it is possible to read Planlægningsbalances which
        start at a specific hour on the year. E.g. not at YYYY-01-01-00:00:00 Fx. which are in June
        This helps with analysis when projects (variations are active) are due to in the middel of the year f.x.
        '''
        SetupBalances.doTime(app, SpecificHourStart[0], SpecificHourStart[1], SpecificHourStart[2], SpecificHourStart[3], '0', '0')
        time = app.GetActiveStudyCase().GetContents('*.SetTime')[0]
        hourStart = time.hourofyear

        data = idata[2][2]
        ReArranged, dum = [], [0.0001 for x in range(0, 8760)]
        for i in range(len(data)):
            ndata = data[i]
            TotalAmountOfBalance = len(data[i])
            dum = [0.0001 for x in range(0, 8760)]
#            app.PrintPlain('\n%s at i %s Vektor before %s: ' %(idata[2][0][i], i, dum[2889:2905]))
            for k in range(hourStart, hourStart+len(ndata)):
                dum[k-1] = ndata[k-(hourStart)]
#            app.PrintPlain('%s at i %s Vektor after %s: ' %(idata[2][0][i], i, dum[2889:2905]))
            ReArranged.append(dum)
#        app.PrintPlain('%s %s' %(len(ReArranged), len(data)))
        SetupBalances.doTime(app, 1970, 1, 1, '0', '0', '0')
        return ReArranged, hourStart, TotalAmountOfBalance

    def NetplanBalances_FindObjectsAndScale(app, year, VecData, grid, printinfo, studytype, SpecificHourStart, AmountOfBalance):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        RelevantObj = SetupBalances.FindRelevantObjects(app, year, grid, studytype)

        DK_Objs, DKgrid, Nabo_Objs = RelevantObj[0], RelevantObj[1], RelevantObj[2]
        Vector, OOSVector, Objekt, used, NotActiveObjs, DK_Production_MW, DK_Consumption_MW, HVDC_MW = [], [], [], [], [], [], [], []
        usedHVDCsum = []
        for k in range(len(VecData[0][0])):
            # [VecData, ObjID,], [VecData_OSS, ObjID_OSS]
            ObjectName, ObjectId, ObjectData, ObjectOOS = VecData[0][0][k].loc_name, VecData[0][1][k], VecData[0][0][k], VecData[1][0][k]
           # app.PrintPlain(VecData[0][0][k])

            MWvec = ObjectData.vector
            if len(ObjectData.vector) < SpecificHourStart+AmountOfBalance:            
                while not len(MWvec) == SpecificHourStart+AmountOfBalance:
                    MWvec = MWvec + [0.0001]
            
            if 'id' in ObjectId:
                if printinfo == 1: prtp('\n---------------------------\nCharacteristic id/attribute: %s \nCharacteristic %s \nPlanlægnings data %s \nFollowing objects will be used for the p.u. characteristic: %s' %(ObjectId, ObjectData, ObjectData.vector,ObjectData))
                objs=[]
                try:
                    if 'id' in ObjectId and not '|' in ObjectId:
                        obj = app.SearchObjectByForeignKey(ObjectId[3:])
                        if not obj == None:
                            if not any(obj.loc_name == s for s in used) and any(SetupBalances.getNet(obj) in s for s in DKgrid):
                                if printinfo == 1: prtp('%s \nPlanlægnings data %s' %(obj, ObjectData.vector))
                                used.append(obj.loc_name)
                                MW = SetupBalances.NetplanBalances_MakeScale(app, [obj], ObjectData, DK_Objs, SpecificHourStart, AmountOfBalance)
                                objs=[obj]
                                if printinfo == 1: prtp('Characteristic p.u %s' %ObjectData.vector)
                                unitClass = obj.GetUserAttribute('UnitClass')
                                Classnam = obj.GetClassName()
                                if Classnam == 'ElmLod':
                                    DK_Consumption_MW.append(MWvec)            # # # # # LOAD
                                elif unitClass[0] == 'Exchange HVDC':
                                    HVDC_MW.append(MWvec)
                                else:
                                    DK_Production_MW.append(MWvec)             # # # # # PROD

                            else: 
                                app.PrintError('Please check foreign key %s since it might be used more than once' %ObjectId)
                        else:
                            prtw('Warning XXH32. Object not found! ID is incorrect or object is not active! %s %s' %(ObjectName, ObjectId))
                    if 'id' in ObjectId and '|' in ObjectId:
                        keep, dum, objid = [], [], ObjectId[3:]
                        for k in range(len(objid)):
                            dum.append(objid[k])
                            if objid[k] == '|':
                                keep.append(''.join(dum[:len(dum)-1]))
                                dum=[]
                            if k == len(objid)-1:
                                keep.append(''.join(dum))
                                dum=[]
                        
                        for objekt in keep:
                            obj = app.SearchObjectByForeignKey(objekt)
                            if not obj == None:
                                if (not any(obj.loc_name == s for s in used) and any(SetupBalances.getNet(obj) in s for s in DKgrid)) or 'dc_' == obj.loc_name[:3]:
                                    if printinfo == 1: prtp(obj)
                                    used.append(obj.loc_name)
                                    used.append(ObjectName)
                                    objs.append(obj)
                                else: 
                                    app.PrintError('Please check foreign key %s since it might be used more than once' %ObjectId)                                    
                            else:
                                prtw('Warning xJs19: Object not found! ID is incorrect or object is not active! %s %s' %(ObjectName, ObjectId))
                        if printinfo == 1: prtp('Planlægnings data  %s' %ObjectData.vector)
                        MW = SetupBalances.NetplanBalances_MakeScale(app, objs, ObjectData, DK_Objs, SpecificHourStart, AmountOfBalance)
                        unitClass = obj.GetUserAttribute('UnitClass')
                        Classnam = obj.GetClassName()
                        if Classnam == 'ElmLod':
                            DK_Consumption_MW.append(MWvec)            # # # # # LOAD
                        elif unitClass[0] == 'Exchange HVDC' and not obj in usedHVDCsum:
                            HVDC_MW.append(MWvec)
                            usedHVDCsum.append(obj)
                        elif (unitClass[0] != 'Exchange HVDC') and (Classnam != 'ElmLod'):
                            DK_Production_MW.append(MWvec)             # # # # # PROD
                                    
                        if printinfo == 1: prtp('Characteristic p.u %s' %ObjectData.vector)
                    if not objs == []:
                        Objekt.append(objs)
                        Vector.append(ObjectData)
                        OOSVector.append(ObjectOOS)
                except:
                    prtw('Warning hd012: Scenarios_Scale: Check Foreign Key for: %s %s.\nVector will not be assigned to any object.\nID could be incorrect or object is not active!'  %(ObjectName, ObjectId))
                    NotActiveObjs.append(ObjectData)
        KeepObjsForHVDC_Q=[]
               
        for k in range(len(VecData[0][0])):
            ObjectName, ObjectId, ObjectData, ObjectOOS = VecData[0][0][k].loc_name, VecData[0][1][k], VecData[0][0][k], VecData[1][0][k]
            dumval = [0.0001 for value in ObjectData.vector if 0 < value < 0.0001]
            objs, objsUse = [], []
            if not len(dumval) == len(ObjectData.vector) and not 'id' in ObjectId:
                if printinfo == 1 and not 'Sverige' in ObjectData.loc_name and not 'Tyskland' in ObjectData.loc_name: prtp('\n---------------------------\nCharacteristic id/attribute: %s \nCharacteristic %s \nPlanlægnings data %s \nInstalled MW from following objects will be used to p.u. scale: %s' %(ObjectId, ObjectData, ObjectData.vector,ObjectData))
                # all classes except HVDC!
                if ('attribute' in ObjectId) and not ('HVDC' in ObjectId) and not ('Tyskland' in ObjectName) and not ('Sverige' in ObjectName):
                    for ka in range(len(DK_Objs)):
                        attri, uclass = DK_Objs[ka].GetUserAttribute('UnitClass'), DK_Objs[ka].GetClassName()
                    # Attribute groups: Load Normal, Wind coast, Wind Offshore, Wind Onshore.
                        if not attri == 1:
                            if (attri[0] in ObjectId) and not ('HVDC' in attri[0]) and not any(DK_Objs[ka].loc_name in s for s in used):
                                chk=0
                                chk1=0
                                if ObjectName == 'Landvind GL':
                                    chk1=1
                                    if uclass == 'ElmAsm':
                                        chk=1
                                elif ObjectName == 'Landvind NY':
                                    chk1=1
                                    if uclass == 'ElmGenstat':
                                        chk=1
                                else:
                                    chk=0
                                    chk1=0
                                if chk == chk1:
                                    if printinfo == 1: prtp(DK_Objs[ka])
                                    used.append(DK_Objs[ka].loc_name)
                                    used.append(ObjectName)
                                    objs.append(DK_Objs[ka])
                                    objsUse.append(DK_Objs[ka])
                if ('Tyskland' in ObjectName or 'Sverige' in ObjectName):
                        # Start Nabo Part
                        for kNABO in range(len(Nabo_Objs)):
                            uclass = Nabo_Objs[kNABO].GetClassName()
                            if 'Production' in ObjectName and any(uclass == x for x in ['ElmGenstat', 'ElmAsm', 'ElmSym']):
                                used.append(Nabo_Objs[kNABO].loc_name)
                                used.append(ObjectName)
                                objs.append(Nabo_Objs[kNABO])
                                objsUse.append(Nabo_Objs[kNABO])
                            elif 'Consumption' in ObjectName and uclass == 'ElmLod':
                                used.append(Nabo_Objs[kNABO].loc_name)
                                used.append(ObjectName)
                                objs.append(Nabo_Objs[kNABO])
                                objsUse.append(Nabo_Objs[kNABO])

                if ('attribute' in ObjectId and 'HVDC' in ObjectId):
                    for ka in range(len(DK_Objs)):
                        attri = DK_Objs[ka].GetUserAttribute('UnitClass')
                        uclass = DK_Objs[ka].GetClassName()
                        if not attri == 1:
                            if (attri[0] in ObjectId and 'HVDC' in attri[0]):
                                dc = [['SK', 'SK1', 'SK2', 'SK3', 'SK4'], ['CO1', 'Cobra'], ['KS', 'KS1', 'KS2', 'KontiSkan'], ['VL', 'VL1', 'Viking', 'Viking Link'],
                                  ['SB', 'SB1', 'STB1', 'Storebaelt'], ['KO', 'KO1', 'Kontek'], ['KF', 'KF1', 'Kriegers Flak']]
                                for k2 in range(len(dc)):
                                    if any(ObjectName in x for x in dc[k2]):
                                        for dcName in dc[k2]:
                                            objname = DK_Objs[ka].loc_name
                                            if dcName == objname[3:6] and not any(DK_Objs[ka].loc_name in s for s in used):
                                                MW = DK_Objs[ka].pgini_a
                                                if not MW < 0:
                                                    if printinfo == 1: prtp(DK_Objs[ka])
                                                    objs.append(DK_Objs[ka])
                                                    objsUse.append(DK_Objs[ka])
                                                    used.append(objname)
                                                else:
                                                    objsUse.append(DK_Objs[ka])
                                                    used.append(objname)
                                                KeepObjsForHVDC_Q.append(DK_Objs[ka])

            if not objs == [] or (('_Q' in ObjectName) and not any(ObjectName in x for x in used)):
                if printinfo == 1 and not ('Sverige' in ObjectData.loc_name) and not ('Tyskland' in ObjectData.loc_name): prtp('Planlægnings data  %s' %ObjectData.vector)
                if '_Q' in ObjectName[len(ObjectName)-2:]:
                    HVDC_Q=[abs(value) for value in VecData[0][0][k-1].vector]
                    VecData[0][0][k].vector = HVDC_Q
                    objsUse = KeepObjsForHVDC_Q
                    KeepObjsForHVDC_Q=[]
                elif not ('Sverige' in ObjectData.loc_name) and not ('Tyskland' in ObjectData.loc_name): # and not ('_Q' in ObjectData.loc_name):
                    MW = SetupBalances.NetplanBalances_MakeScale(app, objs, ObjectData, DK_Objs, SpecificHourStart, AmountOfBalance)
                    if not objs[0].GetClassName() == 'ElmLod' and not ('HVDC' in objs[0].GetUserAttribute('UnitClass')[0]):    
                        DK_Production_MW.append(MWvec)                 # # # # # PROD
                    elif objs[0].GetClassName() == 'ElmLod':
                        DK_Consumption_MW.append(MWvec)                # # # # # LOAD

                if printinfo==1 and not ('Sverige' in ObjectData.loc_name) and not ('Tyskland' in ObjectData.loc_name): prtp('Characteristic p.u %s\n---------------------------' %ObjectData.vector)
                used.append(ObjectName)
                Objekt.append(objsUse)
                Vector.append(ObjectData)
                OOSVector.append(ObjectOOS)

      
        # Make Balance for Neighbouring Grid; Sum the Total Production and Consumption over DK! For each balance!
        # Production DK balancer
        Production_DK_sum_balances = [0] # 0 at time 00:00:00             
        for j in range(1,len(DK_Production_MW[0])):
            Prod_bal = sum([x[j] for x in DK_Production_MW])
            Production_DK_sum_balances.append(Prod_bal)
            
        # HVDC DK balancer          
        HVDC_sum_balances = [0] # 0 at time 00:00:00     
        try: 
            for j in range(1,len(HVDC_MW[0])):                              ### 
                HVDC_bal = sum([x[j] for x in HVDC_MW])                     ###
                HVDC_sum_balances.append(HVDC_bal)
        except: pass
        # Forbrug DK balancer           
        Consumption_DK_sum_balances = [0]
        for j in range(1, len(DK_Consumption_MW[0])):
            Cons_bal = sum([x[j] for x in DK_Consumption_MW])
            Consumption_DK_sum_balances.append(Cons_bal)
                       
        # Make p.u. values for Neighbouring Grid wrt. installed prod/con in grid.
        Actual_Nabo_Prod, Actual_Nabo_Consumption = 0, 0
        for kNABO in range(len(Nabo_Objs)):
            uclass = Nabo_Objs[kNABO].GetClassName()
            if not uclass == 'ElmLod':
                if Nabo_Objs[kNABO].pgini_a > 0.0001:
                    Actual_Nabo_Prod += Nabo_Objs[kNABO].pgini_a
            elif uclass == 'ElmLod':
                if Nabo_Objs[kNABO].plini_a > 0.0001:
                    Actual_Nabo_Consumption += Nabo_Objs[kNABO].plini_a
        
        Used_Nabo_Production, Used_Nabo_Consumption = [0.0001], [0.0001]
        for k in range(1, len(Consumption_DK_sum_balances)): # Make P.U scale for nabo
            Used_Nabo_Production.append((0.3 * Actual_Nabo_Consumption)/Actual_Nabo_Prod)
            Used_Nabo_Consumption.append((0.3 * Actual_Nabo_Consumption)/Actual_Nabo_Consumption)
            Debugger = 0
            if Debugger ==1:                                                   # Herna
                app.PrintPlain('* **** ****  **** Balance: %d ****  **** **** *' %k)
                app.PrintPlain('DK - Production             %s' %Production_DK_sum_balances[k])
                app.PrintPlain('DK - Consumption            %s' %Consumption_DK_sum_balances[k])
                app.PrintPlain('HVDC_sum                    %s' %HVDC_sum_balances[k])                
                app.PrintPlain('Nabo Grid - Actual Prod     %s' %Actual_Nabo_Prod)
                app.PrintPlain('Nabo Grid - Actual Consump. %s' %Actual_Nabo_Consumption)                  
                app.PrintPlain('Used_Nabo_Production        %s' %Used_Nabo_Production)
                app.PrintPlain('Used_Nabo_Consumption       %s\n' %Used_Nabo_Consumption)               

        # Fix the vectors for the neighbouring grid
        for k in range(len(VecData[0][0])):
            ObjectName, ObjectId, ObjectData, ObjectOOS = VecData[0][0][k].loc_name, VecData[0][1][k], VecData[0][0][k], VecData[1][0][k]
            if ('Sverige' in ObjectName) or ('Tyskland' in ObjectName):
                if ('Production' in ObjectName):
                    ObjectData.vector = Used_Nabo_Production
                else:
                    ObjectData.vector = Used_Nabo_Consumption
                dum=[]
                for ki in range(len(ObjectData.vector)):
                    if ObjectData.vector[ki] > 0.0001:
                       dum.append(0)
                    else:
                        dum.append(1)
                ObjectOOS.vector = dum
                if printinfo == 1: prtp('\n---------------------------\nNeighbour Characteristic with regard to DK: %s \nCharacteristic %s \np.u. values %s' %(ObjectId, ObjectData, ObjectData.vector))

        NotUsedDK = [xObj for xObj in DK_Objs if not any(xObj.loc_name == s for s in used)]
        NotUsedNeighbour = [xObj for xObj in Nabo_Objs if not any(xObj.loc_name == s for s in used)]
        ObjNotUsed= NotUsedDK + NotUsedNeighbour

        Objekt.append(ObjNotUsed)
        Vector.append(VecData[2][0])
        OOSVector.append(VecData[2][1])
        if printinfo == 1:
            [prtp(xstr) for xstr in ['\nDK Grid: Following will be assigned out-of-service\n---------------------------']+NotUsedDK+['\nNeighbouring Grid: Following will be assigned out-of-service\n---------------------------']+NotUsedNeighbour+['\nFollowing objects and their relevant characteristics:']]
            for g in range(len(Objekt)):
                prtp('---------------------------\n %s \nCharacteristics : %s and %s ' %(Objekt[g], Vector[g], OOSVector[g]))
        return Objekt, Vector, OOSVector

    def NetplanBalances_MakeScale(app, Object, ObjectVec, DK_Objs, SpecificHourStart, AmountOfBalance):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError
               
        if len(ObjectVec.vector) < SpecificHourStart+AmountOfBalance:
            dummy = ObjectVec.vector
            while not len(dummy) == SpecificHourStart+AmountOfBalance:
                dummy = dummy + [0.0001]
            ObjectVec.vector = dummy
        MW, pu = 0, [0.0001 for x in range(SpecificHourStart)]
        for obj in Object:
            objClass, objAttribute = obj.GetClassName(), obj.GetUserAttribute('UnitClass')
            if objClass == 'ElmLod':
                if obj.IsHidden() : 
                    MW = MW + 0                
                elif obj.plini_a > 0.0001:
                    MW = MW + obj.plini_a
                else:
                    MW = MW + 0.0001
            else:
                if obj.IsHidden() : 
                    MW = MW + 0                
                elif (obj.pgini_a > 0.0001) or (obj.pgini_a < 0):
                    if not any('Exchange HVDC' == x for x in objAttribute):
                        MW = MW + abs(obj.pgini_a)
                    else:
                        if not obj.pgini_a < 0:
                            MW = MW + obj.pgini_a
                else:
                    MW = MW + 0.0001

        for val in range(SpecificHourStart, SpecificHourStart+AmountOfBalance):
            value = ObjectVec.vector[val]
            if value < 0 and objClass == 'ElmLod':
                value = value * (-1)
            # Start: Fortegn på HVDC
            if not '_Q' == ObjectVec.loc_name[len(ObjectVec.loc_name)-2:]:
                if any('Exchange HVDC' == x for x in objAttribute) and any('ENDKW' == SetupBalances.getNet(x) for x in DK_Objs[5:35]):
                    if value > 0:
                        value = abs(value)*(-1)
                    else:
                        value = abs(value)
                if any('Exchange HVDC' == x for x in objAttribute) and any('ENDKE' == SetupBalances.getNet(x) for x in DK_Objs[5:35]):
                    if not 'dc_SB' == Object[0].loc_name[:5] and not value == 0.0001 and not 'dc_KF' == Object[0].loc_name[:5] :
                        value = value * (-1)
                    elif 'dc_SB' == Object[0].loc_name[:5] or 'dc_KF' == Object[0].loc_name[:5]:
                        if value > 0:
                            value = abs(value)
                        else:
                            value = abs(value) * (-1)
            # Slut: Fortegn på HVDC
            if not value == 0 and not value == 0.0001:
                NewValue = (value/MW)
                if NewValue < 0.0001 and not NewValue < -0.0001:
                    pu.append(0.0001)
                else:
                    pu.append(NewValue)
            else:
                pu.append(0.0001)

        pu_end = [0.0001 for x in range(len(pu), 8760)]
        pu = pu+pu_end

        if '_Q' == ObjectVec.loc_name[len(ObjectVec.loc_name)-2:]:
            ab = [abs(val) for val in pu]
            ObjectVec.vector = ab
        else:
            ObjectVec.vector = pu

        return ObjectVec.vector

    def NetplanBalances_OpenExcelSheet(app, PathFolder, FileName, year, FolderName, printinfo, grid):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        app.SetUserBreakEnabled(0)
        import xlrd as xlrd
        if FileName == '':
            book = book = xlrd.open_workbook(PathFolder)
        else:
            book = xlrd.open_workbook(PathFolder+FileName)
        nsheets = book.sheet_names()
        try:
            for k in range(len(nsheets)):
                if nsheets[k] == 'ELvis '+str(year)+' DK'+str(grid):
                    use_sheet = book.sheet_by_index(k)
                    if printinfo == 1: prtp('\nBalances will be installed from: %s\n0. Finding start/end reading posistions for chosen year: %s \n-------------------' %(FileName, nsheets[k]))
            ncols = use_sheet.ncols
        except UnboundLocalError:
            prte('Error: NetplanBalances_OpenExcelSheet(): In the excel file: Year, Grid not found. Sheetname should be "ELvis YYYY DKX".')
            exit()

        objekt, idx_col, idx_row, mh = [], [], [], 0
        try:
            for k in range(ncols):
                col = use_sheet.col_values(k)
                for k1 in range(len(col)):
                    if col[k1] == str(col[k1]):
                        if 'attribute' in str(col[k1][:len('attribute')]) or 'id' in str(col[k1][:2]):
                            if printinfo == 1: prtp('Attribute or ID found in coloumn %s x row %s' %(k+1, k1+1))
                            objekt.append(col[k1])
                            idx_col.append(k)
                            idx_row.append(k1)
                            mh=1
                if mh==1:
                    break
            data=[PathFolder+FileName, FolderName]
        except:
            prte('Error: NetplanBalances_OpenExcelSheet(): Error finding attribute/id coloumn in Excel sheet!')
            exit()
        try:
            ObjId, ObjName, ObjValue =[], [], []
            for g in range(len(idx_row)):
                row = use_sheet.row_values(idx_row[g])
                rowidx = row[idx_col[g]:]
                ObjectId, ObjectName, ObjectValues = [], [], []
                for k in range(len(rowidx)):
                    if not rowidx[k] == '':
                        if str(rowidx[k]) == rowidx[k]:
                            if k == 0:
                                ObjectId.append(rowidx[k])
                            else:
                                ObjectName.append(rowidx[k+1])
                        elif float(rowidx[k]) == rowidx[k]:
                            ObjectValues.append(rowidx[k])
                sObjectValues=[0]
                counter=0
                for kval in range(len(ObjectValues)):
                    sObjectValues.append(float(ObjectValues[kval]))
                    if any(float(ObjectValues[kval]) == xv for xv in [0, 0.0]):
                        counter +=1
                if not counter == len(ObjectValues):
                    ObjId.append(ObjectId[0])
                    ObjName.append(ObjectName[0])
                    ObjValue.append(sObjectValues)
            data.append([ObjName, ObjId, ObjValue])
        except:
            prte('Error: NetplanBalances_OpenExcelSheet(): Error finding balance values in Excel sheet!')
            exit()
        if printinfo == 1:
            prtp('\nData: ')
            for k in range(len(data[2][0])):
                prtp('%s %s %s' %(data[2][0][k], data[2][1][k], data[2][2][k]))
            prtp('\n..Data gathering complete')
        app.SetUserBreakEnabled(1)
        return data

    def NetplanBalances_GetActiveSheet(app):
        '''
        author: KOK
        Description:
        '''
        import win32com.client
        import datetime

        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        xl = win32com.client.Dispatch("EXCEL.Application")
        ActiveWorkbook = xl.ActiveWorkbook
        ActiveWorkbookpath = xl.ActiveWorkbook.FullName
        if ActiveWorkbookpath[:len('http:')] == 'http:':
            ActiveWorkbookpath = ActiveWorkbookpath[len('http:'):]

        sFullPath = ActiveWorkbookpath.split('\\')
        PathFolder = []
        for s in sFullPath:
            if not s == sFullPath[len(sFullPath)-1]:
                PathFolder.append(s)
                PathFolder.append('\\')
        PathFolder = ''.join(PathFolder)
        FileName = sFullPath[len(sFullPath)-1]
        try:
            SheetName = xl.ActiveSheet.Name
            SheetName = SheetName.split(' ')
            Year = int(SheetName[1])
            Grid = int(SheetName[2][len(SheetName[2])-1])
        except:
            Year = ''
            Grid = ''
        try: # Check if Clock is set.
          t_act = str(datetime.datetime.strptime(str(xl.ActiveSheet.Cells(1,1).Value).split('+')[0], '%Y-%m-%d %H:%M:%S'))
          date=t_act.split(' ')[0]
          time=t_act.split(' ')[1]
          StartAtTime = [str(date.split('-')[0]), str(date.split('-')[1]), str(date.split('-')[2]), str(time.split(':')[0]), str(time.split(':')[1]), str(time.split(':')[2])]
          prtp('..Specific start hour')
        except:
          prtp('..Regular')
          StartAtTime = None

        return PathFolder, FileName, Year, Grid, StartAtTime

### SIFRE FUNCTIONS (Only used for Sifre/BID)
    def SifreBalances_FindObjects(app, year, VecData, grid, printinfo, studytype):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        RelevantObj = SetupBalances.FindRelevantObjects(app, year, grid, studytype)
        DK_Objs, DKgrid, Nabo_Objs = RelevantObj[0], RelevantObj[1], RelevantObj[2]
        Vector, OOSVector, Objekt, used, NotActiveObjs = [], [], [], [], []  
        
        for k in range(len(VecData[0][0])):
            # [VecData, ObjID,], [VecData_OSS, ObjID_OSS]
            ObjectName, ObjectId, ObjectData, ObjectOOS = VecData[0][0][k].loc_name, VecData[0][1][k], VecData[0][0][k], VecData[1][0][k]
            if any(x in ObjectName for x in ['Sverige','Tyskland']):
                Actual_Nabo_Prod, Actual_Nabo_Consumption = 0, 0
                oProd   = []
                oForbrug= []
                objs    = []
                for kNABO in range(len(Nabo_Objs)):
                    uclass = Nabo_Objs[kNABO].GetClassName()
                    if not uclass == 'ElmLod':
                        oProd.append(Nabo_Objs[kNABO])
                        used.append(Nabo_Objs[kNABO].loc_name)
                        if Nabo_Objs[kNABO].pgini_a > 0.0001:
                            Actual_Nabo_Prod += Nabo_Objs[kNABO].pgini_a
                            
                    elif uclass == 'ElmLod':
                        oForbrug.append(Nabo_Objs[kNABO])
                        used.append(Nabo_Objs[kNABO].loc_name)                        
                        if Nabo_Objs[kNABO].plini_a > 0.0001:
                            Actual_Nabo_Consumption += Nabo_Objs[kNABO].plini_a
                Used_Nabo_Production, Used_Nabo_Consumption = [], []           
                if 'Production' in ObjectName: 
                    for k in range(1, len(ObjectData.vector)): # Make P.U scale for nabo
                        Used_Nabo_Production.append(0.3 * Actual_Nabo_Consumption/Actual_Nabo_Prod)
                    ObjectData.vector = Used_Nabo_Production
                    objs = oProd
                elif 'Consumption' in ObjectName: 
                    for k in range(1, len(ObjectData.vector)): # Make P.U scale for nabo
                        Used_Nabo_Consumption.append(0.3 * Actual_Nabo_Consumption/Actual_Nabo_Consumption)
                    ObjectData.vector = Used_Nabo_Consumption
                    objs = oForbrug
                    
                ObjectOOS.vector = [1 if value == 0.0001 or (value < 0.0001 and not value < 0) else 0.0001 for value in ObjectData.vector]                    
            
            dumval = [0.0001 for value in ObjectData.vector if value == 0.0001 or (value < 0.0001 and not value < 0)]            

            if not len(dumval) == len(ObjectData.vector):
                if printinfo == 1: prtp('\n---------------------------\nCharacteristic id/attribute: %s\nCharacteristic %s \nInput data %s \nFollowing objects will be used for the p.u. characteristic %s' %(ObjectId, ObjectData, ObjectData.vector[:30],ObjectData))
                if not any(x in ObjectName for x in ['Tyskland','Sverige']):
                    objs=[]                    
                    if not '|' in ObjectId:
                        Objs_FK = [ObjectId]
                    else:
                        Objs_FK = ObjectId.split('|')
                    for FKs in Objs_FK:
                        try:
                            obj = app.SearchObjectByForeignKey(FKs)
                            if not 'dc_' == obj.loc_name[:3]:
                              if not any(obj.loc_name == s for s in used) and any(SetupBalances.getNet(obj) in s for s in DKgrid):
                                used.append(obj.loc_name)
                                objs.append(obj)
                                if printinfo == 1: prtp(obj)
                            else:
                              used.append(obj.loc_name)
                              objs.append(obj)
                        except AttributeError:
                            if not ('Tyskland' and 'Sverige') in ObjectName: prtw('Warning q2v33: SifreBalances_FindObjects(): ForeignKey invalid. Object will not be assigned to vector %s' %ObjectName)
                if not objs == []:
                    if printinfo==1: [prtp(xstr) for xstr in ['Characteristic %s data %s' %(ObjectData, ObjectData.vector[:30]), '---------------------------']]
                    used.append(ObjectName)
                    Objekt.append(objs)
                    Vector.append(ObjectData)
                    OOSVector.append(ObjectOOS)
                    objs=[]                    
                else:
                    if not 'Tyskland' in ObjectData.loc_name and not 'Sverige' in ObjectData.loc_name:
                        [prtw(xstr) for xstr in ['Warning p9JJ3: SifreBalances_FindObjects(): Unable to acces objects in current study year OR error in Mapningsregler. No object will be assigned to vector.', 'For the following object %s' %ObjectData]]
                    NotActiveObjs.append(ObjectData)

        NotUsedDK = [xObj for xObj in DK_Objs if not any(xObj.loc_name in s for s in used)]
        NotUsedNeighbour = [xObj for xObj in Nabo_Objs if not any(xObj.loc_name in s for s in used)]
        ObjNotUsed= NotUsedDK + NotUsedNeighbour

        Objekt.append(ObjNotUsed)
        Vector.append(VecData[2][0])
        OOSVector.append(VecData[2][1])

        if printinfo == 1:
            [prtp(xstr) for xstr in ['\nDK Grid: Following will be assigned out-of-service\n---------------------------']+NotUsedDK+['\nNeighbouring Grid: Following will be assigned out-of-service\n---------------------------']+NotUsedNeighbour+['\nFollowing objects and their relevant characteristics:']]
            for g in range(len(Objekt)):
                prtp('---------------------------\n %s \nCharacteristics : %s and %s ' %(Objekt[g], Vector[g], OOSVector[g]))
        return Objekt, Vector, OOSVector

    def SifreBalances_OpenCsvFile(app, PathFolder, FileName, printinfo):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        app.SetUserBreakEnabled(0)
        csv = open(PathFolder+FileName, 'r', encoding = 'utf-8-sig')
        data, dummy, k = [], [], 0
        for line in csv:
            newline = line.replace(',', '.')
            currentline = newline.split(";")
            if '\n' in currentline[len(currentline)-1]:
                currentline[len(currentline)-1] = currentline[len(currentline)-1][:len(currentline[len(currentline)-1])-1]
            currentline.remove(currentline[0])
            data.append(currentline)
            k = k + 1
            if k == 4:
                for g in range(len(currentline)):
                    dummy.append(['dummy'])
                break
        row_val=[]
        for line in csv:
            currentline = line.split(";")
            if '\n' in currentline[len(currentline)-1]:
                currentline[len(currentline)-1] = currentline[len(currentline)-1][:len(currentline[len(currentline)-1])-1]
            currentline.remove(currentline[0])
            row_val.append(currentline)

        for k in range(len(dummy)):
            for g in range(len(row_val)):
                dummy[k].append(float((row_val[g][k])))
        for g in range(len(dummy)):
            dummy[g].remove(dummy[g][0])
        data.append(dummy)

        app.SetUserBreakEnabled(1)
        data1= [data[0], data[1], data[4], data[2], data[3]]
        if printinfo == 1 and not len(data) < 1:
            prtp('\n\n1. %s file overview\n-------------------\n[ [Object names, ..], [Objects IDs, ..] ], [ [COL DATA], ... , ]\n\nObjects in File: %s\n%s \n\nForeignkeys or attributes \n%s\nEnd-------------------\n' %((FileName), FileName, data[0], data[1]))
        return data1

    def SifreBalances_GetDataFromPathFolder(app, PathFolder, year, grid, FolderName, printinfo):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        from os import listdir
        from os.path import isfile, join

        data, used =[], []
        if FolderName == 0:
            kFolderName='Sifre'
        elif FolderName == 3:
            kFolderName='BID'
        else:
            prte('Error0: _GetDataFromPathFolder(). Study type not valid!')
            exit()

        onlyfiles = [f for f in listdir(PathFolder) if isfile(join(PathFolder, f))]
        for kFile in range(len(onlyfiles)):
            FileName = onlyfiles[kFile]
            if kFolderName in FileName and 'DK'+str(grid) in FileName:
                if (kFolderName == 'Sifre' or kFolderName == 'BID') and year in FileName and not FileName in used and '.csv' in FileName and not 'OOS' in FileName and not "MappedBalanceReport" in FileName:
                    data.append(PathFolder+FileName)
                    if (kFolderName == 'Sifre'):
                        data.append('SIFRE_'+str(year))
                    else:
                        data.append('BID_'+str(year))
                    GetData = SetupBalances.SifreBalances_OpenCsvFile(app, PathFolder, FileName, printinfo)
                    used.append(FileName)
                    data.append(GetData)
            if len(data)-1 == kFile and data == []:
                prte('Error: _GetDataFromPathFolder(): File not found in Path.\nFilename should be named with Indicators: "Sifre", "year", and "grid". For example: Sifre_2017_DK1.csv\nOr Fx. BID_2018_DK1.csv\nPlease reconfigure')
                exit()
        return data

### COMMON FUNCTIONS (Used for all typebalances)
    def SetupLibraryAndCharacteristics(app, FolderName, grid, year, data, printinfo, SpecificHourStart, AmountOfBalance, Mode):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        if FolderName == 1: # Get relevant library folder. Planlægningsbalance-library
            Folder, attribute_data = app.SearchObjectByForeignKey('1-Elvis-Scenarier_DK'+str(grid)), data[2][1]
        elif FolderName == 0: # Sifre-library
            Folder, attribute_data = app.SearchObjectByForeignKey('1-Elvis-SIFRE_DK'+str(grid)), data[2][4]
        elif FolderName == 3: # BID-library
            Folder, attribute_data = app.SearchObjectByForeignKey('1-Elvis-BID_DK'+str(grid)), data[2][4]
        else:
            prte('Error: SetupLibraryAndCharacteristics(). Input error! grid or study-type not valid.')
            exit()
        if Folder == '' or Folder == None or Folder == []:
            prtw('Warning n2M1: SetupLibraryAndCharacteristics(). Library Main folder for study type not found. Foreign key of folder not valid.')
            exit()

        ## Define nabo grid
        if int(grid) == 1: strNaboGrid = 'Tyskland '
        if int(grid) == 2: strNaboGrid ='Sverige '
#        a = input('Continue. Ent')
        # check and delete if "old scenarios folder" exists
        [oldFold.Delete() for oldFold in Folder.GetContents() if oldFold.loc_name[:len('ParamCharScenario')] == 'ParamCharScenario' and oldFold.loc_name[len(oldFold.loc_name)-4:] == data[1][len(data[1])-4:]]
        ## Create new vectors. Delete old ones.
        VecData, ObjID, VecData_OSS, ObjID_OSS, Fm = [],[],[],[], 0
        for Fold in Folder.GetContents():
            if Fold.loc_name == data[1] and year in Fold.loc_name:
                if printinfo == 1: [prtp(xstr) for xstr in ['Folder exists, ... old data will be deleted', Fold, 'Creating new vectors...']]
                keepObjFold, Fm = Fold, 1
                [dat.Delete() for dat in Fold.GetContents() if len(Fold.GetContents()) >= 1] # Delete old vectors
                for g in range(len(data[2][0])): # Create new vectors for data
                    ObjectName, ObjectId, ObjectData = data[2][0][g], data[2][1][g], data[2][2][g]
                    VecData.append(SetupBalances.CreateCharacteristics(app, keepObjFold, ObjectName, ObjectData, printinfo, ))
                    ObjID.append(ObjectId)
                    if 'Exchange HVDC' in attribute_data[g] or 'dc_' in ObjectName: # Create Mvar vector for HVDC's
                        ObjectData = [abs(ObjectData[x]) for x in range(len(ObjectData))]                      
                        VecData.append(SetupBalances.CreateCharacteristics(app, Fold, ObjectName+'_Q', ObjectData, printinfo))
                        ObjID.append(ObjectId)
                # Create Production and Consumption vectors for neighbouring grids.
                VecData.append(SetupBalances.CreateCharacteristics(app, keepObjFold, strNaboGrid+' Production', [0 for x in range(len(data[2][2][1]))], printinfo)) # Create Nabo VEC Production
                ObjID.append(strNaboGrid+' Production')
                VecData.append(SetupBalances.CreateCharacteristics(app, keepObjFold, strNaboGrid+' Consumption', [0 for x in range(len(data[2][2][1]))], printinfo)) # Create Nabo VEC Consumption
                ObjID.append(strNaboGrid+' Consumption')
        ## Create New Library Folder, if the folder does not exists (Fm=0).
        if Fm == 0:
            keepObjFold = Folder.CreateObject('IntFolder', data[1])
            if printinfo == 1: [prtp(xstr) for xstr in ['\nFolder does not exist, creating new folder...', Fold, 'Creating new vectors...']]
            for g in range(len(data[2][0])):
                ObjectName, ObjectId, ObjectData = data[2][0][g], data[2][1][g], data[2][2][g]
                VecData.append(SetupBalances.CreateCharacteristics(app, keepObjFold, ObjectName, ObjectData, printinfo))
                ObjID.append(ObjectId)
                if 'Exchange HVDC' in attribute_data[g]:
                    VecData.append(SetupBalances.CreateCharacteristics(app, keepObjFold, ObjectName+'_Q', ObjectData, printinfo))
                    ObjID.append(ObjectId)
            VecData.append(SetupBalances.CreateCharacteristics(app, keepObjFold, strNaboGrid+' Production', [0 for x in range(len(data[2][2][1]))], printinfo)) # Create Neighbouring Vector Production
            ObjID.append(strNaboGrid+' Production')
            VecData.append(SetupBalances.CreateCharacteristics(app, keepObjFold, strNaboGrid+' Consumption', [0 for x in range(len(data[2][2][1]))], printinfo)) # Create Neighbouring Vector Consumption
            ObjID.append(strNaboGrid+' Consumption')
        ## Create OOS Vectors
        for k in range(len(VecData)):           # Create oos characteristics. 
            if not '_Q' == VecData[k].loc_name[len(VecData[k].loc_name)-2:]:
                OOSdata=[]
                for value in VecData[k].vector:
                    if 0 <= value and value <= 0.0001:
                        OOSdata.append(1)
                    else:
                        OOSdata.append(0)
                VecData_OSS.append(SetupBalances.CreateCharacteristics(app, keepObjFold, 'OOS_'+VecData[k].loc_name, OOSdata, printinfo))
                ObjID_OSS.append(ObjID[k])
            elif '_Q' in VecData[k].loc_name[len(VecData[k].loc_name)-4:]:
                VecData_OSS.append(VecData_OSS[k-1])
                ObjID_OSS.append(ObjID[k-1])
        ## Create Objects-NOT-IN-USE Vectors
        E_Frem = [0 for xf in VecData[0].vector]
        E_OOS = [1 for xf in E_Frem]
        NotUsedVec, OOSNotUsedVec = SetupBalances.CreateCharacteristics(app, keepObjFold, 'Enhed_OOS_i_beregning', E_Frem, printinfo), SetupBalances.CreateCharacteristics(app, keepObjFold, 'OOS_Enhed_OOS_i_beregning', E_OOS, printinfo)
        # Create IntMat for balances
        if FolderName == 1 and SpecificHourStart == 1 and not Mode == 'EngineMode': # Create Balance Overview af Planlægningsbalancerne
            SetupBalances.CreateIntMatForBalanceView(app, keepObjFold, VecData)
        return [VecData, ObjID,], [VecData_OSS, ObjID_OSS], [NotUsedVec, OOSNotUsedVec]

    def CreateIntMatForBalanceView(app, Folder, VecData):
        '''
        author: KOK
        Description: overblik over de installerede balancer i modellen
        eksl. sifre. Opsætning af matrix til et helt år tager for lang tid.
        '''
        prtp = app.PrintPlain
        Par = Folder.GetParent()
        intMat = Folder.CreateObject('IntMat', Par.loc_name[len(Par.loc_name)-3:]+' '+Folder.loc_name+' Balance View')
        intMat.for_name = 'IntMat_'+Par.loc_name[len(Par.loc_name)-3:]+' '+Folder.loc_name+' Balance View'
        vecs, cons, prods = [], [], []
        for Q in range(len(VecData)):
            if not '_Q' in VecData[Q].loc_name and not 'Tyskland' in VecData[Q].loc_name and not 'Sverige' in VecData[Q].loc_name:
                vecs.append(VecData[Q])
                if ('orbrug' in VecData[Q].loc_name) or ('l_' in VecData[Q].loc_name[:2]):
                    cons.append(VecData[Q])
                else:
                    prods.append(VecData[Q])

        con,prod,udveksling = [], [], []
        for balance in range(len(cons[0].vector)):
            bal=0
            for unit in range(len(cons)):
                bal += cons[unit].vector[balance]
            con.append(abs(bal)*(-1))

        for balance in range(len(prods[0].vector)):
            bal=0
            for unit in range(len(prods)):
                bal += prods[unit].vector[balance]
            prod.append(bal)
        for balance in range(len(con)):
            udveksling.append(sum([con[balance]+prod[balance]]))
        udveksling=udveksling[1:]

        for k in range(len(vecs[0].vector)):
            if vecs[0].vector[k] == 0:
                break

        intMat.Init(len(vecs)+1, k)
        intMat.SetRowLabel(len(vecs)+1, 'Udveksling')
        for h in range(len(udveksling)):
            intMat.Set(len(vecs)+1, h+1, float(udveksling[h]))
        used=[]

        for i in range(len(vecs)):
            sRow = vecs[i].loc_name
            intMat.SetRowLabel(i+1, sRow)
            if not any(vecs[i].loc_name[:len(vecs[i].loc_name)-2] == x for x in used):
                cCol=0
                for g in range(len(vecs[i].vector)):
                    if g == len(vecs[i].vector)-1:
                        break
                    cCol+=1
                    intMat.SetColumnLabel(g+1, str(cCol))
                    if (vecs[i].vector[g+1]) == 0.0001:
                        value = float(0)
                    else:
                        value = float(vecs[i].vector[g+1])
                    intMat.Set(i+1, g+1, value)
                    used.append(vecs[i].loc_name)
        intMat.Save()
        return

    def CreateCharacteristics(app, Fold, ObjectName, ObjectData, printinfo):
        '''
        author: KOK
        Description:
        '''
        prtp = app.PrintPlain
        if 0 in ObjectData or 0.0001 in ObjectData and not 'OOS_' in ObjectName:
          dum=[]
          for i, x in enumerate(ObjectData):
                if 0 <= x <= 0.0001:
                    dum.append(0.0001)
                else:
                    dum.append(x)
          ObjectData=dum

        NewVec = Fold.CreateObject('ChaTime', ObjectName)
        if 'OOS_' in ObjectName[:len('OOS_')+1]: NewVec.usage = 2
        else: NewVec.usage = 1
        NewVec.source = 0
        NewVec.repeat = 3
        NewVec.cunit = 0
        NewVec.approx = 0
        NewVec.vector = ObjectData
        if printinfo == 1: prtp(NewVec)

        return NewVec

    def SetupVariationAndExpansionStage(app, grid, year, FolderName, data, VecData, printinfo):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        if FolderName == 1:
            fk = '1-Elvis-Scenarier-Var-DK'+str(grid)
        elif FolderName == 0:
            if grid == 1:
                fk = '48739055d43b7710d6e7'
            elif grid == 2:
                fk = '4d3192b8bcef2028da41'
        elif FolderName == 3:
            fk = '1-Elvis-BID-Var-DK'+str(grid)
        else:
            prte('Error: SetupVariationAndExpansionStage(). Input for grid or study-type not valid.')
            exit()
        try:
            VarFolder = app.SearchObjectByForeignKey(fk)
            kVarFolder = VarFolder.GetContents()
            [vFold.Deactivate() for vFold in kVarFolder]
        except:
            prte('Error: SetupVariationAndExpansionStage(). Foreign key for folder not valid.')
            exit()

        used, mh = [], 0
        for VarFold in kVarFolder:
            if not VarFold.loc_name in used and VarFold.loc_name == data[1]:
                mh=1
                used.append(VarFold.loc_name)
                [sFold.Delete() for sFold in VarFold.GetContents() if len(VarFold.GetContents()) >= 1]
                VarFold.Activate()
                useTime = SetupBalances.doTime(app, year, '1', '1', '0', '0', '0')  # doTime(app, year, month, day, hour, minutes, seconds)
                VarFold.NewStage(data[1], useTime, 1)
                if printinfo == 1: [prtp(xstr) for xstr in ['VarationFolder exists, old data deleted...', VarFold, 'New expansion stage created']+VarFold.GetContents()+['End-------------------\n','4. Assigning vectors']]
                SetupBalances.AssignCharacterstics(app, VecData, printinfo, FolderName)
                if printinfo == 1: prtp('End-------------------')
                VarFold.Deactivate()

        if mh == 0 and not data[1] in used:
            Parent = app.SearchObjectByForeignKey(fk)
            NewFold = Parent.CreateObject('IntScheme', data[1])
            used.append(NewFold.loc_name)
            NewFold.Activate()
            useTime = SetupBalances.doTime(app, year, '1', '1', '0', '0', '0')
            NewFold.NewStage(data[1], useTime, 1)
            if printinfo == 1: [prtp(xstr) for xstr in ['VariationFolder does not exist, creating new VariationFolder...', NewFold, 'New expansion stage created']+NewFold.GetContents()+['End-------------------\n','4. Assigning vectors\n-------------------']]
            SetupBalances.AssignCharacterstics(app, VecData, printinfo, FolderName)
            if printinfo == 1: prtp('End-------------------')
            NewFold.Deactivate()
        return

    def AssignCharacterstics(app, VecData, printinfo, FolderName):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError
        for k in range(len(VecData[0])):            # Number of scales to be assigned. 
            for k1 in range(len(VecData[0][k])):    # Number of machines for each scale. 
                obj, objVector, objOOSVector = VecData[0][k][k1], VecData[1][k], VecData[2][k]
                comp = 0
                if printinfo == 1: [prtp(xstr) for xstr in ['\n-------------------', 'Object: %s' %obj]]
                try:
                    checkRef=[]
                    for chaRef in obj.GetContents('*.ChaRef'):
                        oChaVec = chaRef.typ_id
                        if not oChaVec == None:
                            checkRef.append(oChaVec.loc_name)
                            if printinfo == 1 and not 'FREM' in oChaVec.loc_name:
                                prtp('Vectors in base, before assigning: %s and %s' %(oChaVec, chaRef))

                    if not any(('OUT' or 'OOS') in x for x in checkRef) and not 'dc_' in obj.loc_name[:3]:
                        if printinfo == 1: prtp('outserv.ChaRef missing\nNew OUT_/OOS_ *.ChaRef will be created')
                        newChaRefOOS = obj.CreateObject('ChaRef', 'outserv')
                        ChaRefsOOS = obj.GetContents('*.ChaRef')
                        for chaRefOOS in ChaRefsOOS:
                            if chaRefOOS.loc_name == newChaRefOOS.loc_name:
                                chaRefOOS.typ_id = objOOSVector
                    checkRef=[]
                    for chaRef in obj.GetContents('*.ChaRef'):
                        oChaVec = chaRef.typ_id
                        if not oChaVec == None:
                            if not 'FREM' in oChaVec.loc_name:
                                if 'aram' and 'har' in oChaVec.loc_name:
                                    if 'dc_' in obj.loc_name[:3] and obj.GetClassName() == 'ElmGenstat':
                                        if '_P' in oChaVec.loc_name[len(oChaVec.loc_name)-2:] and not '_Q' in objVector.loc_name[len(objVector.loc_name)-2:]:
                                            chaRef.typ_id = objVector
                                        elif '_Q' in oChaVec.loc_name[len(oChaVec.loc_name)-2:] and '_Q' in objVector.loc_name[len(objVector.loc_name)-2:]:
                                            chaRef.typ_id = objVector
                                    else:
                                        chaRef.typ_id = objVector
                                    comp = 1
                                if 'OUT' in oChaVec.loc_name:
                                    if not '_Q' == objVector.loc_name[len(objVector.loc_name)-2:]:
                                        chaRef.typ_id = objOOSVector
                                    comp = 1
                    if printinfo == 1:
                        for chaRef in obj.GetContents('*.ChaRef'):
                            oChaVec = chaRef.typ_id
                            if not oChaVec == None:
                                if not 'FREM' in oChaVec.loc_name:
                                    prtp('Vectors in base, after assigning: %s and %s' %(oChaVec, chaRef))
                                    
                    checkRef=[]
                    for chaRef in obj.GetContents('*.ChaRef'):
                        oChaVec = chaRef.typ_id
                        if not oChaVec == None:
                            checkRef.append(oChaVec.loc_name)
                    if not any('FREM' in x for x in checkRef) or checkRef == [] and not 'dc_' in obj.loc_name[:3]:
                        prtw('FREM_* Chars are missing for %s. New *.ChaRefs will be created for balances' %obj)
                        if obj.GetClassName() == 'ElmLod':
                            pstr = 'plini'
                            qstr = 'qlini'
                        else:
                            pstr = 'pgini'
                            qstr = 'qgini'
                        newChaRefP = obj.CreateObject('ChaRef', pstr)
                        newChaRefQ = obj.CreateObject('ChaRef', qstr)
                        ChaRefs = obj.GetContents('*.ChaRef')
                        for ChaRef in ChaRefs:
                            if ChaRef.loc_name == newChaRefP.loc_name or ChaRef.loc_name == newChaRefQ.loc_name:
                                ChaRef.typ_id = objVector
                        comp = 1

                    if comp == 0 and not '_Q' in objVector.loc_name[len(objVector.loc_name)-2:]:
                        prte('Error: Scenarios_AssignVec(). Vectors not assign to any object (object is not active in calculation year): %s %s and %s' %(obj, objVector,objOOSVector))
                    elif printinfo == 1:
                        for chaRef in obj.GetContents('*.ChaRef'):
                            oChaVec = chaRef.typ_id
                            if not oChaVec == None:
                                if not 'FREM' in oChaVec.loc_name:
                                    prtp('Vectors will become, if variation/expansion stage in 3. is active: %s and %s' %(oChaVec,chaRef))
                except:
                    prtw('Warning 92X2. AssignCharacterstics Loop. Validate assigning!')
        return

    def FindRelevantObjects(app, year, grid, studytype):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        if [oCase.Activate() for oCase in app.GetProjectFolder('study').GetContents('*.IntCase') if oCase.loc_name == 'DK'+str(grid)+' Fokuseret reference'] == []:
            prte('Error KJ39. "DK%s Fokuseret Reference" was not found. Program will exit.' %(str(grid)))
            exit()

        SetupBalances.doTime(app, year, '1', '1', '0', '0', '0') #year, month, day, hour, minutes, seconds

        actObj = app.GetCalcRelevantObjects('*.ElmLod,*.ElmGenstat, *.ElmSym, *.ElmAsm', 1, 0, 0)
        attributes = ['Wind Coast', 'Wind On-shore', 'Wind Off-shore', 'Solar', 'Exchange HVDC', 'Production CHP',
                      'Production Power', 'Load Normal', 'Load Elkedel', 'Load Heat Pump', 'Load Datacenter', 'Load Banedanmark', 'Load Service', 'Load Letbane']
        DKgrid = ['ØSTKRAFT', 'dcVL', 'dcSK','dcSB','dcKS','dcKO','dcKF','dcCO','ENDKW','ENDKE','FLENSBORG', 'Baltic']
        Ngrid= ['TYSKLAND', 'SVERIGE', 'Tyskland', 'Sverige']
        DK_Objs, Nabo_Objs = [], []
        for k in range(len(actObj)):
            hid, out = actObj[k].IsHidden(), actObj[k].outserv
            if hid == 0 and out == 0:
              try:
                uclass = actObj[k].GetUserAttribute('UnitClass')
                if any(uclass[0] in x for x in attributes):
                    if (not (any(SetupBalances.getNet(actObj[k]) in x for x in Ngrid))):
                        DK_Objs.append(actObj[k])
                    elif  ('l_UWN_060' in actObj[k].loc_name):
                        DK_Objs.append(actObj[k])
                    if  (any(SetupBalances.getNet(actObj[k]) in x for x in Ngrid)):
                        if not ('l_UWN_060' in actObj[k].loc_name): # and ClassName == 'ElmLod':
                            Nabo_Objs.append(actObj[k])
              except TypeError:
                prtw('Warnig 0U1. ELvis attributes missing for object! %s .\nObject will not be assigned characterstic.\nFix attributes to object, if characterstic is supposed to be used.' %actObj[k])

        if [oCase.Activate() for oCase in app.GetProjectFolder('study').GetContents('*.IntCase') if oCase.loc_name == 'DK Fokuseret reference'] == []:
            prte('Error KJ39. "DK Fokuseret Reference" was not found. Program will exit.')
            exit()
        SetupBalances.doTime(app, year, '1', '1', '0', '0', '0')

        return DK_Objs, DKgrid, Nabo_Objs, Ngrid

    def doTime(app, year, month, day, hour, minutes, seconds):
        '''
        author: KOK
        Description:
        '''
        import datetime
        import time
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError
        if int(year) < 2036 and not int(year)==1970: # use PF-UTC funktion
            dt = datetime.datetime(int(year), int(month), int(day), int(hour), int(minutes))
            dotime= int(time.mktime(dt.timetuple()))
            PF_time = app.GetActiveStudyCase().GetContents('*.SetTime')[0]
            PF_time.SetTimeUTC(dotime)
        else:
            PF_time = app.GetActiveStudyCase().GetContents('*.SetTime')[0]
            if len(str(month)) == 1: month = '0'+str(month)
            if len(str(day)) == 1: day = '0'+str(day)
            PF_time.cDate = int(''.join(str(year)+str(month)+str(day)))
            PF_time.hourofyear = int(hour)
            PF_time = app.GetActiveStudyCase().GetContents('*.SetTime')[0]
        return app.GetActiveStudyCase().iStudyTime

    def getNet(obj):
        '''
        author: KOK
        Description:
        '''
        net = obj.GetParent()                   # GetParent of object
        while not net.GetClassName() =='ElmNet':    # GetParent until class i ElmNet.
            net = net.GetParent()
        else:
            net = net.loc_name                      # Get name of the grid
        return net

    def CleanUpStandardBrugerExpansionStage(app, year, printinfo):
        '''
        author: KOK
        Description:
        '''
        prtp, prti, prtw, prte = app.PrintPlain, app.PrintInfo, app.PrintWarn, app.PrintError

        Folder = app.SearchObjectByForeignKey('42f9a8affabdf95fc1a5')
        Folder = Folder.GetContents()
        Var=[]
        for Variation in Folder:
            if Variation.GetClassName() == 'IntScheme' and str(year) in Variation.loc_name[:4]:
                Variation.Deactivate()
                Var.append(Variation)
                VariationDat = Variation.GetContents()
                for Exspansion in VariationDat:
                  Changes = Exspansion.GetContents()
                  for al in Changes:
                    if 'FREM' in al.loc_name and al.GetClassName() == 'ChaVec':
                      al.Delete()
                break
        if not Var == []:
          Var[0].Activate()
        if printinfo == 1: prtp('Variation Standard Bruger cleared')
        return

if __name__ == '__main__':
  import powerfactory as pf
  app = pf.GetApplication()
  app.ClearOutputWindow()
  Main = SetupBalances.Main(app)